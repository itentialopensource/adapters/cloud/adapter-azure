/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-azure',
      type: 'Azure',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Azure = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Azure Adapter Test', () => {
  describe('Azure Class Tests', () => {
    const a = new Azure(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    /*
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    */

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const networkInterfacesResourceGroupName = 'fakedata';
    const networkInterfacesNetworkInterfaceName = 'fakedata';
    describe('#networkInterfacesListEffectiveNetworkSecurityGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesListEffectiveNetworkSecurityGroups(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesListEffectiveNetworkSecurityGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGetEffectiveRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesGetEffectiveRouteTable(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesGetEffectiveRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesList(networkInterfacesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkInterfacesNetworkInterfacesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#networkInterfacesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesCreateOrUpdate(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, networkInterfacesNetworkInterfacesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkInterfacesNetworkInterfacesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#networkInterfacesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesUpdateTags(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, networkInterfacesNetworkInterfacesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesGet(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceIPConfigurationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfaceIPConfigurationsList(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceIPConfigurationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkInterfacesIpConfigurationName = 'fakedata';
    describe('#networkInterfaceIPConfigurationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfaceIPConfigurationsGet(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, networkInterfacesIpConfigurationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceIPConfigurationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceLoadBalancersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfaceLoadBalancersList(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceLoadBalancersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfaceTapConfigurationsList(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceTapConfigurationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkInterfacesTapConfigurationName = 'fakedata';
    const networkInterfacesNetworkInterfaceTapConfigurationsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#networkInterfaceTapConfigurationsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfaceTapConfigurationsCreateOrUpdate(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, networkInterfacesTapConfigurationName, networkInterfacesNetworkInterfaceTapConfigurationsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceTapConfigurationsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfaceTapConfigurationsGet(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, networkInterfacesTapConfigurationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceTapConfigurationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkInterfacesVirtualMachineScaleSetName = 'fakedata';
    describe('#networkInterfacesListVirtualMachineScaleSetNetworkInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetNetworkInterfaces(networkInterfacesResourceGroupName, networkInterfacesVirtualMachineScaleSetName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesListVirtualMachineScaleSetNetworkInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkInterfacesVirtualmachineIndex = 'fakedata';
    describe('#networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces(networkInterfacesResourceGroupName, networkInterfacesVirtualMachineScaleSetName, networkInterfacesVirtualmachineIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGetVirtualMachineScaleSetNetworkInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetNetworkInterface(networkInterfacesResourceGroupName, networkInterfacesVirtualMachineScaleSetName, networkInterfacesVirtualmachineIndex, networkInterfacesNetworkInterfaceName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesGetVirtualMachineScaleSetNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListVirtualMachineScaleSetIpConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetIpConfigurations(networkInterfacesResourceGroupName, networkInterfacesVirtualMachineScaleSetName, networkInterfacesVirtualmachineIndex, networkInterfacesNetworkInterfaceName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesListVirtualMachineScaleSetIpConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGetVirtualMachineScaleSetIpConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration(networkInterfacesResourceGroupName, networkInterfacesVirtualMachineScaleSetName, networkInterfacesVirtualmachineIndex, networkInterfacesNetworkInterfaceName, networkInterfacesIpConfigurationName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesGetVirtualMachineScaleSetIpConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsResourceGroupName = 'fakedata';
    const subscriptionsVirtualNetworkName = 'fakedata';
    const subscriptionsSubnetName = 'fakedata';
    const subscriptionsSubnetsPrepareNetworkPoliciesBodyParam = {
      serviceName: 'string',
      networkIntentPolicyConfigurations: [
        {
          networkIntentPolicyName: 'string',
          sourceNetworkIntentPolicy: {
            id: 'string',
            name: 'string',
            type: 'string',
            location: 'string',
            tags: {}
          }
        }
      ]
    };
    describe('#subnetsPrepareNetworkPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.subnetsPrepareNetworkPolicies(subscriptionsResourceGroupName, subscriptionsVirtualNetworkName, subscriptionsSubnetName, subscriptionsSubnetsPrepareNetworkPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'subnetsPrepareNetworkPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsSubnetsUnprepareNetworkPoliciesBodyParam = {
      serviceName: 'string'
    };
    describe('#subnetsUnprepareNetworkPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.subnetsUnprepareNetworkPolicies(subscriptionsResourceGroupName, subscriptionsVirtualNetworkName, subscriptionsSubnetName, subscriptionsSubnetsUnprepareNetworkPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'subnetsUnprepareNetworkPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVirtualWANName = 'fakedata';
    const subscriptionsGeneratevirtualwanvpnserverconfigurationvpnprofileBodyParam = {
      vpnServerConfigurationResourceId: 'string',
      authenticationMethod: 'EAPMSCHAPv2'
    };
    describe('#generatevirtualwanvpnserverconfigurationvpnprofile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generatevirtualwanvpnserverconfigurationvpnprofile(subscriptionsResourceGroupName, subscriptionsVirtualWANName, subscriptionsGeneratevirtualwanvpnserverconfigurationvpnprofileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.profileUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'generatevirtualwanvpnserverconfigurationvpnprofile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVpnSitesConfigurationDownloadBodyParam = {
      outputBlobSasUrl: 'string'
    };
    describe('#vpnSitesConfigurationDownload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vpnSitesConfigurationDownload(subscriptionsResourceGroupName, subscriptionsVirtualWANName, subscriptionsVpnSitesConfigurationDownloadBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSitesConfigurationDownload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsAssociatedWithVirtualWanList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnServerConfigurationsAssociatedWithVirtualWanList(subscriptionsResourceGroupName, subscriptionsVirtualWANName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.vpnServerConfigurationResourceIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnServerConfigurationsAssociatedWithVirtualWanList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsLocation = 'fakedata';
    const subscriptionsDomainNameLabel = 'fakedata';
    describe('#checkDnsNameAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.checkDnsNameAvailability(subscriptionsLocation, subscriptionsDomainNameLabel, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.available);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'checkDnsNameAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableDelegationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availableDelegationsList(subscriptionsLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availableDelegationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availablePrivateEndpointTypesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availablePrivateEndpointTypesList(subscriptionsLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availablePrivateEndpointTypesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableServiceAliasesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availableServiceAliasesList(subscriptionsLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availableServiceAliasesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceTagsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceTagsList(subscriptionsLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.changeNumber);
                assert.equal('string', data.response.cloud);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'serviceTagsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableEndpointServicesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availableEndpointServicesList(subscriptionsLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availableEndpointServicesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'p2sVpnGatewaysList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualWansList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualWansList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnGatewaysList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnGatewaysList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnServerConfigurationsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnServerConfigurationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSitesList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSitesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVirtualMachineScaleSetName = 'fakedata';
    describe('#publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses(subscriptionsResourceGroupName, subscriptionsVirtualMachineScaleSetName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVirtualmachineIndex = 'fakedata';
    const subscriptionsNetworkInterfaceName = 'fakedata';
    const subscriptionsIpConfigurationName = 'fakedata';
    describe('#publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses(subscriptionsResourceGroupName, subscriptionsVirtualMachineScaleSetName, subscriptionsVirtualmachineIndex, subscriptionsNetworkInterfaceName, subscriptionsIpConfigurationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsPublicIpAddressName = 'fakedata';
    describe('#publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress(subscriptionsResourceGroupName, subscriptionsVirtualMachineScaleSetName, subscriptionsVirtualmachineIndex, subscriptionsNetworkInterfaceName, subscriptionsIpConfigurationName, subscriptionsPublicIpAddressName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableResourceGroupDelegationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availableResourceGroupDelegationsList(subscriptionsLocation, subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availableResourceGroupDelegationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availablePrivateEndpointTypesListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availablePrivateEndpointTypesListByResourceGroup(subscriptionsLocation, subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availablePrivateEndpointTypesListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableServiceAliasesListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availableServiceAliasesListByResourceGroup(subscriptionsResourceGroupName, subscriptionsLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'availableServiceAliasesListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysListByResourceGroup(subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'p2sVpnGatewaysListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsGatewayName = 'fakedata';
    const subscriptionsP2sVpnGatewaysCreateOrUpdateBodyParam = {
      location: 'string'
    };
    describe('#p2sVpnGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsP2sVpnGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'p2sVpnGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysGet(subscriptionsResourceGroupName, subscriptionsGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'p2sVpnGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubsListByResourceGroup(subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVirtualHubName = 'fakedata';
    const subscriptionsVirtualHubsCreateOrUpdateBodyParam = {
      location: 'string'
    };
    describe('#virtualHubsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubsCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsVirtualHubName, subscriptionsVirtualHubsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubsGet(subscriptionsResourceGroupName, subscriptionsVirtualHubName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hubVirtualNetworkConnectionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hubVirtualNetworkConnectionsList(subscriptionsResourceGroupName, subscriptionsVirtualHubName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'hubVirtualNetworkConnectionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsConnectionName = 'fakedata';
    describe('#hubVirtualNetworkConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hubVirtualNetworkConnectionsGet(subscriptionsResourceGroupName, subscriptionsVirtualHubName, subscriptionsConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'hubVirtualNetworkConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubRouteTableV2sList(subscriptionsResourceGroupName, subscriptionsVirtualHubName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubRouteTableV2sList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsRouteTableName = 'fakedata';
    const subscriptionsVirtualHubRouteTableV2sCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#virtualHubRouteTableV2sCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubRouteTableV2sCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsVirtualHubName, subscriptionsRouteTableName, subscriptionsVirtualHubRouteTableV2sCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubRouteTableV2sCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubRouteTableV2sGet(subscriptionsResourceGroupName, subscriptionsVirtualHubName, subscriptionsRouteTableName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubRouteTableV2sGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsIpAddress = 'fakedata';
    describe('#virtualNetworksCheckIPAddressAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksCheckIPAddressAvailability(subscriptionsResourceGroupName, subscriptionsVirtualNetworkName, subscriptionsIpAddress, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.available);
                assert.equal(true, Array.isArray(data.response.availableIPAddresses));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualNetworksCheckIPAddressAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resourceNavigationLinksList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resourceNavigationLinksList(subscriptionsResourceGroupName, subscriptionsVirtualNetworkName, subscriptionsSubnetName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'resourceNavigationLinksList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceAssociationLinksList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceAssociationLinksList(subscriptionsResourceGroupName, subscriptionsVirtualNetworkName, subscriptionsSubnetName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'serviceAssociationLinksList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksListUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksListUsage(subscriptionsResourceGroupName, subscriptionsVirtualNetworkName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualNetworksListUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualWansListByResourceGroup(subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualWansListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVirtualWansCreateOrUpdateBodyParam = {
      location: 'string'
    };
    describe('#virtualWansCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualWansCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsVirtualWANName, subscriptionsVirtualWansCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualWansCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualWansGet(subscriptionsResourceGroupName, subscriptionsVirtualWANName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualWansGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#supportedSecurityProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.supportedSecurityProviders(subscriptionsResourceGroupName, subscriptionsVirtualWANName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.supportedProviders));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'supportedSecurityProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnGatewaysListByResourceGroup(subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnGatewaysListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVpnGatewaysCreateOrUpdateBodyParam = {
      location: 'string'
    };
    describe('#vpnGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnGatewaysCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsVpnGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnGatewaysGet(subscriptionsResourceGroupName, subscriptionsGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsListByVpnGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnConnectionsListByVpnGateway(subscriptionsResourceGroupName, subscriptionsGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnConnectionsListByVpnGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVpnConnectionsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#vpnConnectionsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnConnectionsCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsConnectionName, subscriptionsVpnConnectionsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnConnectionsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnConnectionsGet(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnLinkConnectionsListByVpnConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnLinkConnectionsListByVpnConnection(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnLinkConnectionsListByVpnConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsLinkConnectionName = 'fakedata';
    describe('#vpnSiteLinkConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSiteLinkConnectionsGet(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsConnectionName, subscriptionsLinkConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSiteLinkConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnServerConfigurationsListByResourceGroup(subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnServerConfigurationsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVpnServerConfigurationName = 'fakedata';
    const subscriptionsVpnServerConfigurationsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#vpnServerConfigurationsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnServerConfigurationsCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsVpnServerConfigurationName, subscriptionsVpnServerConfigurationsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnServerConfigurationsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnServerConfigurationsGet(subscriptionsResourceGroupName, subscriptionsVpnServerConfigurationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnServerConfigurationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSitesListByResourceGroup(subscriptionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSitesListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVpnSiteName = 'fakedata';
    const subscriptionsVpnSitesCreateOrUpdateBodyParam = {
      location: 'string'
    };
    describe('#vpnSitesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSitesCreateOrUpdate(subscriptionsResourceGroupName, subscriptionsVpnSiteName, subscriptionsVpnSitesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSitesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSitesGet(subscriptionsResourceGroupName, subscriptionsVpnSiteName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSitesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSiteLinksListByVpnSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSiteLinksListByVpnSite(subscriptionsResourceGroupName, subscriptionsVpnSiteName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSiteLinksListByVpnSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriptionsVpnSiteLinkName = 'fakedata';
    describe('#vpnSiteLinksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSiteLinksGet(subscriptionsResourceGroupName, subscriptionsVpnSiteName, subscriptionsVpnSiteLinkName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSiteLinksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualWANsResourceGroupName = 'fakedata';
    const virtualWANsVirtualHubName = 'fakedata';
    const virtualWANsVirtualHubsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#virtualHubsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualHubsUpdateTags(virtualWANsResourceGroupName, virtualWANsVirtualHubName, virtualWANsVirtualHubsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualWANs', 'virtualHubsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualWANsVirtualWANName = 'fakedata';
    const virtualWANsVirtualWansUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#virtualWansUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualWansUpdateTags(virtualWANsResourceGroupName, virtualWANsVirtualWANName, virtualWANsVirtualWansUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualWANs', 'virtualWansUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vpnSitesResourceGroupName = 'fakedata';
    const vpnSitesVpnSiteName = 'fakedata';
    const vpnSitesVpnSitesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#vpnSitesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnSitesUpdateTags(vpnSitesResourceGroupName, vpnSitesVpnSiteName, vpnSitesVpnSitesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VpnSites', 'vpnSitesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vpnServerConfigurationsResourceGroupName = 'fakedata';
    const vpnServerConfigurationsVpnServerConfigurationName = 'fakedata';
    const vpnServerConfigurationsVpnServerConfigurationsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#vpnServerConfigurationsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnServerConfigurationsUpdateTags(vpnServerConfigurationsResourceGroupName, vpnServerConfigurationsVpnServerConfigurationName, vpnServerConfigurationsVpnServerConfigurationsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VpnServerConfigurations', 'vpnServerConfigurationsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vpnGatewaysResourceGroupName = 'fakedata';
    let vpnGatewaysGatewayName = 'fakedata';
    describe('#vpnGatewaysReset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnGatewaysReset(vpnGatewaysResourceGroupName, vpnGatewaysGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              vpnGatewaysGatewayName = data.response.name;
              saveMockData('VpnGateways', 'vpnGatewaysReset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vpnGatewaysVpnGatewaysUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#vpnGatewaysUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vpnGatewaysUpdateTags(vpnGatewaysResourceGroupName, vpnGatewaysGatewayName, vpnGatewaysVpnGatewaysUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VpnGateways', 'vpnGatewaysUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const p2SVpnGatewaysResourceGroupName = 'fakedata';
    const p2SVpnGatewaysGatewayName = 'fakedata';
    const p2SVpnGatewaysP2sVpnGatewaysGenerateVpnProfileBodyParam = {
      authenticationMethod: 'EAPMSCHAPv2'
    };
    describe('#p2sVpnGatewaysGenerateVpnProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysGenerateVpnProfile(p2SVpnGatewaysResourceGroupName, p2SVpnGatewaysGatewayName, p2SVpnGatewaysP2sVpnGatewaysGenerateVpnProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.profileUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('P2SVpnGateways', 'p2sVpnGatewaysGenerateVpnProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysGetP2sVpnConnectionHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealth(p2SVpnGatewaysResourceGroupName, p2SVpnGatewaysGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('P2SVpnGateways', 'p2sVpnGatewaysGetP2sVpnConnectionHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const p2SVpnGatewaysP2sVpnGatewaysGetP2sVpnConnectionHealthDetailedBodyParam = {
      vpnUserNamesFilter: [
        'string'
      ],
      outputBlobSasUrl: 'string'
    };
    describe('#p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed(p2SVpnGatewaysResourceGroupName, p2SVpnGatewaysGatewayName, p2SVpnGatewaysP2sVpnGatewaysGetP2sVpnConnectionHealthDetailedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.sasUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('P2SVpnGateways', 'p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const p2SVpnGatewaysP2sVpnGatewayName = 'fakedata';
    const p2SVpnGatewaysP2sVpnGatewaysDisconnectP2sVpnConnectionsBodyParam = {
      vpnConnectionIds: [
        'string'
      ]
    };
    describe('#p2sVpnGatewaysDisconnectP2sVpnConnections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.p2sVpnGatewaysDisconnectP2sVpnConnections(p2SVpnGatewaysResourceGroupName, p2SVpnGatewaysP2sVpnGatewayName, p2SVpnGatewaysP2sVpnGatewaysDisconnectP2sVpnConnectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('P2SVpnGateways', 'p2sVpnGatewaysDisconnectP2sVpnConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const p2SVpnGatewaysP2sVpnGatewaysUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#p2sVpnGatewaysUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.p2sVpnGatewaysUpdateTags(p2SVpnGatewaysResourceGroupName, p2SVpnGatewaysGatewayName, p2SVpnGatewaysP2sVpnGatewaysUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('P2SVpnGateways', 'p2sVpnGatewaysUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usagesLocation = 'fakedata';
    describe('#usagesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usagesList(usagesLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usages', 'usagesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosProtectionPlansList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosProtectionPlans', 'ddosProtectionPlansList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ddosProtectionPlansResourceGroupName = 'fakedata';
    describe('#ddosProtectionPlansListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosProtectionPlansListByResourceGroup(ddosProtectionPlansResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosProtectionPlans', 'ddosProtectionPlansListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ddosProtectionPlansDdosProtectionPlanName = 'fakedata';
    const ddosProtectionPlansDdosProtectionPlansCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {},
      properties: {
        resourceGuid: 'string',
        provisioningState: 'Updating',
        virtualNetworks: [
          {
            id: 'string'
          }
        ]
      },
      etag: 'string'
    };
    describe('#ddosProtectionPlansCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosProtectionPlansCreateOrUpdate(ddosProtectionPlansResourceGroupName, ddosProtectionPlansDdosProtectionPlanName, ddosProtectionPlansDdosProtectionPlansCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosProtectionPlans', 'ddosProtectionPlansCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ddosProtectionPlansDdosProtectionPlansUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#ddosProtectionPlansUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosProtectionPlansUpdateTags(ddosProtectionPlansResourceGroupName, ddosProtectionPlansDdosProtectionPlanName, ddosProtectionPlansDdosProtectionPlansUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosProtectionPlans', 'ddosProtectionPlansUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosProtectionPlansGet(ddosProtectionPlansResourceGroupName, ddosProtectionPlansDdosProtectionPlanName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.properties);
                assert.equal('string', data.response.etag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosProtectionPlans', 'ddosProtectionPlansGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitAuthorizationsResourceGroupName = 'fakedata';
    const expressRouteCircuitAuthorizationsCircuitName = 'fakedata';
    describe('#expressRouteCircuitAuthorizationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsList(expressRouteCircuitAuthorizationsResourceGroupName, expressRouteCircuitAuthorizationsCircuitName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitAuthorizations', 'expressRouteCircuitAuthorizationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitAuthorizationsAuthorizationName = 'fakedata';
    const expressRouteCircuitAuthorizationsExpressRouteCircuitAuthorizationsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#expressRouteCircuitAuthorizationsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsCreateOrUpdate(expressRouteCircuitAuthorizationsResourceGroupName, expressRouteCircuitAuthorizationsCircuitName, expressRouteCircuitAuthorizationsAuthorizationName, expressRouteCircuitAuthorizationsExpressRouteCircuitAuthorizationsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitAuthorizations', 'expressRouteCircuitAuthorizationsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitAuthorizationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsGet(expressRouteCircuitAuthorizationsResourceGroupName, expressRouteCircuitAuthorizationsCircuitName, expressRouteCircuitAuthorizationsAuthorizationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitAuthorizations', 'expressRouteCircuitAuthorizationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitPeeringsResourceGroupName = 'fakedata';
    const expressRouteCircuitPeeringsCircuitName = 'fakedata';
    describe('#expressRouteCircuitPeeringsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitPeeringsList(expressRouteCircuitPeeringsResourceGroupName, expressRouteCircuitPeeringsCircuitName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitPeerings', 'expressRouteCircuitPeeringsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitPeeringsPeeringName = 'fakedata';
    const expressRouteCircuitPeeringsExpressRouteCircuitPeeringsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#expressRouteCircuitPeeringsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitPeeringsCreateOrUpdate(expressRouteCircuitPeeringsResourceGroupName, expressRouteCircuitPeeringsCircuitName, expressRouteCircuitPeeringsPeeringName, expressRouteCircuitPeeringsExpressRouteCircuitPeeringsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitPeerings', 'expressRouteCircuitPeeringsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitPeeringsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitPeeringsGet(expressRouteCircuitPeeringsResourceGroupName, expressRouteCircuitPeeringsCircuitName, expressRouteCircuitPeeringsPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitPeerings', 'expressRouteCircuitPeeringsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitConnectionsResourceGroupName = 'fakedata';
    const expressRouteCircuitConnectionsCircuitName = 'fakedata';
    const expressRouteCircuitConnectionsPeeringName = 'fakedata';
    describe('#expressRouteCircuitConnectionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitConnectionsList(expressRouteCircuitConnectionsResourceGroupName, expressRouteCircuitConnectionsCircuitName, expressRouteCircuitConnectionsPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitConnections', 'expressRouteCircuitConnectionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitConnectionsConnectionName = 'fakedata';
    const expressRouteCircuitConnectionsExpressRouteCircuitConnectionsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#expressRouteCircuitConnectionsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitConnectionsCreateOrUpdate(expressRouteCircuitConnectionsResourceGroupName, expressRouteCircuitConnectionsCircuitName, expressRouteCircuitConnectionsPeeringName, expressRouteCircuitConnectionsConnectionName, expressRouteCircuitConnectionsExpressRouteCircuitConnectionsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitConnections', 'expressRouteCircuitConnectionsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitConnectionsGet(expressRouteCircuitConnectionsResourceGroupName, expressRouteCircuitConnectionsCircuitName, expressRouteCircuitConnectionsPeeringName, expressRouteCircuitConnectionsConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitConnections', 'expressRouteCircuitConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const peerExpressRouteCircuitConnectionsResourceGroupName = 'fakedata';
    const peerExpressRouteCircuitConnectionsCircuitName = 'fakedata';
    const peerExpressRouteCircuitConnectionsPeeringName = 'fakedata';
    describe('#peerExpressRouteCircuitConnectionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsList(peerExpressRouteCircuitConnectionsResourceGroupName, peerExpressRouteCircuitConnectionsCircuitName, peerExpressRouteCircuitConnectionsPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PeerExpressRouteCircuitConnections', 'peerExpressRouteCircuitConnectionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const peerExpressRouteCircuitConnectionsConnectionName = 'fakedata';
    describe('#peerExpressRouteCircuitConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsGet(peerExpressRouteCircuitConnectionsResourceGroupName, peerExpressRouteCircuitConnectionsCircuitName, peerExpressRouteCircuitConnectionsPeeringName, peerExpressRouteCircuitConnectionsConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PeerExpressRouteCircuitConnections', 'peerExpressRouteCircuitConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuits', 'expressRouteCircuitsListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitsResourceGroupName = 'fakedata';
    describe('#expressRouteCircuitsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsList(expressRouteCircuitsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuits', 'expressRouteCircuitsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitsCircuitName = 'fakedata';
    const expressRouteCircuitsExpressRouteCircuitsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#expressRouteCircuitsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsCreateOrUpdate(expressRouteCircuitsResourceGroupName, expressRouteCircuitsCircuitName, expressRouteCircuitsExpressRouteCircuitsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuits', 'expressRouteCircuitsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitsExpressRouteCircuitsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#expressRouteCircuitsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsUpdateTags(expressRouteCircuitsResourceGroupName, expressRouteCircuitsCircuitName, expressRouteCircuitsExpressRouteCircuitsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuits', 'expressRouteCircuitsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsGet(expressRouteCircuitsResourceGroupName, expressRouteCircuitsCircuitName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuits', 'expressRouteCircuitsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitArpTableResourceGroupName = 'fakedata';
    const expressRouteCircuitArpTableCircuitName = 'fakedata';
    const expressRouteCircuitArpTablePeeringName = 'fakedata';
    const expressRouteCircuitArpTableDevicePath = 'fakedata';
    describe('#expressRouteCircuitsListArpTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsListArpTable(expressRouteCircuitArpTableResourceGroupName, expressRouteCircuitArpTableCircuitName, expressRouteCircuitArpTablePeeringName, expressRouteCircuitArpTableDevicePath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitArpTable', 'expressRouteCircuitsListArpTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitRoutesTableResourceGroupName = 'fakedata';
    const expressRouteCircuitRoutesTableCircuitName = 'fakedata';
    const expressRouteCircuitRoutesTablePeeringName = 'fakedata';
    const expressRouteCircuitRoutesTableDevicePath = 'fakedata';
    describe('#expressRouteCircuitsListRoutesTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTable(expressRouteCircuitRoutesTableResourceGroupName, expressRouteCircuitRoutesTableCircuitName, expressRouteCircuitRoutesTablePeeringName, expressRouteCircuitRoutesTableDevicePath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitRoutesTable', 'expressRouteCircuitsListRoutesTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitRoutesTableSummaryResourceGroupName = 'fakedata';
    const expressRouteCircuitRoutesTableSummaryCircuitName = 'fakedata';
    const expressRouteCircuitRoutesTableSummaryPeeringName = 'fakedata';
    const expressRouteCircuitRoutesTableSummaryDevicePath = 'fakedata';
    describe('#expressRouteCircuitsListRoutesTableSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTableSummary(expressRouteCircuitRoutesTableSummaryResourceGroupName, expressRouteCircuitRoutesTableSummaryCircuitName, expressRouteCircuitRoutesTableSummaryPeeringName, expressRouteCircuitRoutesTableSummaryDevicePath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitRoutesTableSummary', 'expressRouteCircuitsListRoutesTableSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCircuitStatsResourceGroupName = 'fakedata';
    const expressRouteCircuitStatsCircuitName = 'fakedata';
    const expressRouteCircuitStatsPeeringName = 'fakedata';
    describe('#expressRouteCircuitsGetPeeringStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsGetPeeringStats(expressRouteCircuitStatsResourceGroupName, expressRouteCircuitStatsCircuitName, expressRouteCircuitStatsPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.primarybytesIn);
                assert.equal(7, data.response.primarybytesOut);
                assert.equal(5, data.response.secondarybytesIn);
                assert.equal(8, data.response.secondarybytesOut);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitStats', 'expressRouteCircuitsGetPeeringStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsGetStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCircuitsGetStats(expressRouteCircuitStatsResourceGroupName, expressRouteCircuitStatsCircuitName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.primarybytesIn);
                assert.equal(6, data.response.primarybytesOut);
                assert.equal(2, data.response.secondarybytesIn);
                assert.equal(7, data.response.secondarybytesOut);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitStats', 'expressRouteCircuitsGetStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteServiceProvidersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteServiceProvidersList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteServiceProviders', 'expressRouteServiceProvidersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeTablesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteTables', 'routeTablesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeTablesResourceGroupName = 'fakedata';
    describe('#routeTablesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeTablesList(routeTablesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteTables', 'routeTablesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeTablesRouteTableName = 'fakedata';
    const routeTablesRouteTablesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#routeTablesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeTablesCreateOrUpdate(routeTablesResourceGroupName, routeTablesRouteTableName, routeTablesRouteTablesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteTables', 'routeTablesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeTablesRouteTablesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#routeTablesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeTablesUpdateTags(routeTablesResourceGroupName, routeTablesRouteTableName, routeTablesRouteTablesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteTables', 'routeTablesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeTablesGet(routeTablesResourceGroupName, routeTablesRouteTableName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteTables', 'routeTablesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routesResourceGroupName = 'fakedata';
    const routesRouteTableName = 'fakedata';
    describe('#routesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routesList(routesResourceGroupName, routesRouteTableName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routes', 'routesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routesRouteName = 'fakedata';
    const routesRoutesCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#routesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routesCreateOrUpdate(routesResourceGroupName, routesRouteTableName, routesRouteName, routesRoutesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routes', 'routesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routesGet(routesResourceGroupName, routesRouteTableName, routesRouteName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routes', 'routesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ddosCustomPoliciesResourceGroupName = 'fakedata';
    const ddosCustomPoliciesDdosCustomPolicyName = 'fakedata';
    const ddosCustomPoliciesDdosCustomPoliciesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#ddosCustomPoliciesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosCustomPoliciesCreateOrUpdate(ddosCustomPoliciesResourceGroupName, ddosCustomPoliciesDdosCustomPolicyName, ddosCustomPoliciesDdosCustomPoliciesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosCustomPolicies', 'ddosCustomPoliciesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ddosCustomPoliciesDdosCustomPoliciesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#ddosCustomPoliciesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosCustomPoliciesUpdateTags(ddosCustomPoliciesResourceGroupName, ddosCustomPoliciesDdosCustomPolicyName, ddosCustomPoliciesDdosCustomPoliciesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosCustomPolicies', 'ddosCustomPoliciesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosCustomPoliciesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ddosCustomPoliciesGet(ddosCustomPoliciesResourceGroupName, ddosCustomPoliciesDdosCustomPolicyName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosCustomPolicies', 'ddosCustomPoliciesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnections', 'expressRouteCrossConnectionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionsResourceGroupName = 'fakedata';
    describe('#expressRouteCrossConnectionsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsListByResourceGroup(expressRouteCrossConnectionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnections', 'expressRouteCrossConnectionsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionsCrossConnectionName = 'fakedata';
    const expressRouteCrossConnectionsExpressRouteCrossConnectionsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#expressRouteCrossConnectionsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsCreateOrUpdate(expressRouteCrossConnectionsResourceGroupName, expressRouteCrossConnectionsCrossConnectionName, expressRouteCrossConnectionsExpressRouteCrossConnectionsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnections', 'expressRouteCrossConnectionsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionsExpressRouteCrossConnectionsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#expressRouteCrossConnectionsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsUpdateTags(expressRouteCrossConnectionsResourceGroupName, expressRouteCrossConnectionsCrossConnectionName, expressRouteCrossConnectionsExpressRouteCrossConnectionsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnections', 'expressRouteCrossConnectionsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsGet(expressRouteCrossConnectionsResourceGroupName, expressRouteCrossConnectionsCrossConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnections', 'expressRouteCrossConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionPeeringsResourceGroupName = 'fakedata';
    const expressRouteCrossConnectionPeeringsCrossConnectionName = 'fakedata';
    describe('#expressRouteCrossConnectionPeeringsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsList(expressRouteCrossConnectionPeeringsResourceGroupName, expressRouteCrossConnectionPeeringsCrossConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionPeerings', 'expressRouteCrossConnectionPeeringsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionPeeringsPeeringName = 'fakedata';
    const expressRouteCrossConnectionPeeringsExpressRouteCrossConnectionPeeringsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#expressRouteCrossConnectionPeeringsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsCreateOrUpdate(expressRouteCrossConnectionPeeringsResourceGroupName, expressRouteCrossConnectionPeeringsCrossConnectionName, expressRouteCrossConnectionPeeringsPeeringName, expressRouteCrossConnectionPeeringsExpressRouteCrossConnectionPeeringsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionPeerings', 'expressRouteCrossConnectionPeeringsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionPeeringsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsGet(expressRouteCrossConnectionPeeringsResourceGroupName, expressRouteCrossConnectionPeeringsCrossConnectionName, expressRouteCrossConnectionPeeringsPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionPeerings', 'expressRouteCrossConnectionPeeringsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionArpTableResourceGroupName = 'fakedata';
    const expressRouteCrossConnectionArpTableCrossConnectionName = 'fakedata';
    const expressRouteCrossConnectionArpTablePeeringName = 'fakedata';
    const expressRouteCrossConnectionArpTableDevicePath = 'fakedata';
    describe('#expressRouteCrossConnectionsListArpTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsListArpTable(expressRouteCrossConnectionArpTableResourceGroupName, expressRouteCrossConnectionArpTableCrossConnectionName, expressRouteCrossConnectionArpTablePeeringName, expressRouteCrossConnectionArpTableDevicePath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionArpTable', 'expressRouteCrossConnectionsListArpTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionRouteTableSummaryResourceGroupName = 'fakedata';
    const expressRouteCrossConnectionRouteTableSummaryCrossConnectionName = 'fakedata';
    const expressRouteCrossConnectionRouteTableSummaryPeeringName = 'fakedata';
    const expressRouteCrossConnectionRouteTableSummaryDevicePath = 'fakedata';
    describe('#expressRouteCrossConnectionsListRoutesTableSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTableSummary(expressRouteCrossConnectionRouteTableSummaryResourceGroupName, expressRouteCrossConnectionRouteTableSummaryCrossConnectionName, expressRouteCrossConnectionRouteTableSummaryPeeringName, expressRouteCrossConnectionRouteTableSummaryDevicePath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionRouteTableSummary', 'expressRouteCrossConnectionsListRoutesTableSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteCrossConnectionRouteTableResourceGroupName = 'fakedata';
    const expressRouteCrossConnectionRouteTableCrossConnectionName = 'fakedata';
    const expressRouteCrossConnectionRouteTablePeeringName = 'fakedata';
    const expressRouteCrossConnectionRouteTableDevicePath = 'fakedata';
    describe('#expressRouteCrossConnectionsListRoutesTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTable(expressRouteCrossConnectionRouteTableResourceGroupName, expressRouteCrossConnectionRouteTableCrossConnectionName, expressRouteCrossConnectionRouteTablePeeringName, expressRouteCrossConnectionRouteTableDevicePath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionRouteTable', 'expressRouteCrossConnectionsListRoutesTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPAddresses', 'publicIPAddressesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicIPAddressesResourceGroupName = 'fakedata';
    describe('#publicIPAddressesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesList(publicIPAddressesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPAddresses', 'publicIPAddressesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicIPAddressesPublicIpAddressName = 'fakedata';
    const publicIPAddressesPublicIPAddressesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#publicIPAddressesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesCreateOrUpdate(publicIPAddressesResourceGroupName, publicIPAddressesPublicIpAddressName, publicIPAddressesPublicIPAddressesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPAddresses', 'publicIPAddressesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicIPAddressesPublicIPAddressesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#publicIPAddressesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesUpdateTags(publicIPAddressesResourceGroupName, publicIPAddressesPublicIpAddressName, publicIPAddressesPublicIPAddressesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPAddresses', 'publicIPAddressesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPAddressesGet(publicIPAddressesResourceGroupName, publicIPAddressesPublicIpAddressName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPAddresses', 'publicIPAddressesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysResourceGroupName = 'fakedata';
    const virtualNetworkGatewaysVirtualNetworkGatewayConnectionName = 'fakedata';
    const virtualNetworkGatewaysVirtualNetworkGatewaysVpnDeviceConfigurationScriptBodyParam = {
      vendor: 'string',
      deviceFamily: 'string',
      firmwareVersion: 'string'
    };
    describe('#virtualNetworkGatewaysVpnDeviceConfigurationScript - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysVpnDeviceConfigurationScript(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayConnectionName, virtualNetworkGatewaysVirtualNetworkGatewaysVpnDeviceConfigurationScriptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysVpnDeviceConfigurationScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewayName = 'fakedata';
    const virtualNetworkGatewaysVirtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnectionsBodyParam = {
      vpnConnectionIds: [
        'string'
      ]
    };
    describe('#virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysGeneratevpnclientpackageBodyParam = {
      processorArchitecture: 'X86',
      authenticationMethod: 'EAPMSCHAPv2',
      radiusServerAuthCertificate: 'string',
      clientRootCertificates: [
        'string'
      ]
    };
    describe('#virtualNetworkGatewaysGeneratevpnclientpackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysGeneratevpnclientpackage(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysGeneratevpnclientpackageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGeneratevpnclientpackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysGenerateVpnProfileBodyParam = {
      processorArchitecture: 'Amd64',
      authenticationMethod: 'EAPTLS',
      radiusServerAuthCertificate: 'string',
      clientRootCertificates: [
        'string'
      ]
    };
    describe('#virtualNetworkGatewaysGenerateVpnProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysGenerateVpnProfile(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysGenerateVpnProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGenerateVpnProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysPeer = 'fakedata';
    describe('#virtualNetworkGatewaysGetAdvertisedRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysGetAdvertisedRoutes(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysPeer, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGetAdvertisedRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetBgpPeerStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysGetBgpPeerStatus(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysPeer, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGetBgpPeerStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetLearnedRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysGetLearnedRoutes(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGetLearnedRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetVpnclientConnectionHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnclientConnectionHealth(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGetVpnclientConnectionHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetVpnclientIpsecParameters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnclientIpsecParameters(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.saLifeTimeSeconds);
                assert.equal(6, data.response.saDataSizeKilobytes);
                assert.equal('AES128', data.response.ipsecEncryption);
                assert.equal('GCMAES128', data.response.ipsecIntegrity);
                assert.equal('AES128', data.response.ikeEncryption);
                assert.equal('GCMAES256', data.response.ikeIntegrity);
                assert.equal('DHGroup2048', data.response.dhGroup);
                assert.equal('PFS1', data.response.pfsGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGetVpnclientIpsecParameters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetVpnProfilePackageUrl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnProfilePackageUrl(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGetVpnProfilePackageUrl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysReset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysReset(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysReset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysResetVpnClientSharedKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysResetVpnClientSharedKey(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysResetVpnClientSharedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysSetVpnclientIpsecParametersBodyParam = {
      saLifeTimeSeconds: 5,
      saDataSizeKilobytes: 3,
      ipsecEncryption: 'AES256',
      ipsecIntegrity: 'GCMAES192',
      ikeEncryption: 'GCMAES256',
      ikeIntegrity: 'MD5',
      dhGroup: 'ECP256',
      pfsGroup: 'None'
    };
    describe('#virtualNetworkGatewaysSetVpnclientIpsecParameters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysSetVpnclientIpsecParameters(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysSetVpnclientIpsecParametersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.saLifeTimeSeconds);
                assert.equal(8, data.response.saDataSizeKilobytes);
                assert.equal('AES256', data.response.ipsecEncryption);
                assert.equal('SHA256', data.response.ipsecIntegrity);
                assert.equal('AES256', data.response.ikeEncryption);
                assert.equal('SHA384', data.response.ikeIntegrity);
                assert.equal('ECP256', data.response.dhGroup);
                assert.equal('ECP256', data.response.pfsGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysSetVpnclientIpsecParameters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysStartPacketCaptureBodyParam = {
      filterData: 'string'
    };
    describe('#virtualNetworkGatewaysStartPacketCapture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysStartPacketCapture(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysStartPacketCaptureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysStartPacketCapture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysStopPacketCaptureBodyParam = {
      sasUrl: 'string'
    };
    describe('#virtualNetworkGatewaysStopPacketCapture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysStopPacketCapture(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysStopPacketCaptureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysStopPacketCapture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysSupportedVpnDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysSupportedVpnDevices(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysSupportedVpnDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysList(virtualNetworkGatewaysResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysCreateOrUpdateBodyParam = {};
    describe('#virtualNetworkGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysCreateOrUpdate(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewaysVirtualNetworkGatewaysUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#virtualNetworkGatewaysUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysUpdateTags(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, virtualNetworkGatewaysVirtualNetworkGatewaysUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysGet(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysListConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewaysListConnections(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysListConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewayConnectionsResourceGroupName = 'fakedata';
    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName = 'fakedata';
    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsResetSharedKeyBodyParam = {
      keyLength: 1
    };
    describe('#virtualNetworkGatewayConnectionsResetSharedKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsResetSharedKey(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsResetSharedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.keyLength);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsResetSharedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsStartPacketCaptureBodyParam = {
      filterData: 'string'
    };
    describe('#virtualNetworkGatewayConnectionsStartPacketCapture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStartPacketCapture(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsStartPacketCaptureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsStartPacketCapture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsStopPacketCaptureBodyParam = {
      sasUrl: 'string'
    };
    describe('#virtualNetworkGatewayConnectionsStopPacketCapture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStopPacketCapture(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsStopPacketCaptureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsStopPacketCapture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsList(virtualNetworkGatewayConnectionsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsCreateOrUpdateBodyParam = {};
    describe('#virtualNetworkGatewayConnectionsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsCreateOrUpdate(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#virtualNetworkGatewayConnectionsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsUpdateTags(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsGet(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsSetSharedKeyBodyParam = {};
    describe('#virtualNetworkGatewayConnectionsSetSharedKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsSetSharedKey(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionsSetSharedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsSetSharedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsGetSharedKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsGetSharedKey(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsGetSharedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const localNetworkGatewaysResourceGroupName = 'fakedata';
    describe('#localNetworkGatewaysList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.localNetworkGatewaysList(localNetworkGatewaysResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalNetworkGateways', 'localNetworkGatewaysList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const localNetworkGatewaysLocalNetworkGatewayName = 'fakedata';
    const localNetworkGatewaysLocalNetworkGatewaysCreateOrUpdateBodyParam = {};
    describe('#localNetworkGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.localNetworkGatewaysCreateOrUpdate(localNetworkGatewaysResourceGroupName, localNetworkGatewaysLocalNetworkGatewayName, localNetworkGatewaysLocalNetworkGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalNetworkGateways', 'localNetworkGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const localNetworkGatewaysLocalNetworkGatewaysUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#localNetworkGatewaysUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.localNetworkGatewaysUpdateTags(localNetworkGatewaysResourceGroupName, localNetworkGatewaysLocalNetworkGatewayName, localNetworkGatewaysLocalNetworkGatewaysUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalNetworkGateways', 'localNetworkGatewaysUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.localNetworkGatewaysGet(localNetworkGatewaysResourceGroupName, localNetworkGatewaysLocalNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalNetworkGateways', 'localNetworkGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysListBySubscription - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteGatewaysListBySubscription((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteGateways', 'expressRouteGatewaysListBySubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteGatewaysResourceGroupName = 'fakedata';
    describe('#expressRouteGatewaysListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteGatewaysListByResourceGroup(expressRouteGatewaysResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteGateways', 'expressRouteGatewaysListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteGatewaysExpressRouteGatewayName = 'fakedata';
    const expressRouteGatewaysExpressRouteGatewaysCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#expressRouteGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteGatewaysCreateOrUpdate(expressRouteGatewaysResourceGroupName, expressRouteGatewaysExpressRouteGatewayName, expressRouteGatewaysExpressRouteGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteGateways', 'expressRouteGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteGatewaysGet(expressRouteGatewaysResourceGroupName, expressRouteGatewaysExpressRouteGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteGateways', 'expressRouteGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteConnectionsResourceGroupName = 'fakedata';
    const expressRouteConnectionsExpressRouteGatewayName = 'fakedata';
    describe('#expressRouteConnectionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteConnectionsList(expressRouteConnectionsResourceGroupName, expressRouteConnectionsExpressRouteGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteConnections', 'expressRouteConnectionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteConnectionsConnectionName = 'fakedata';
    const expressRouteConnectionsExpressRouteConnectionsCreateOrUpdateBodyParam = {};
    describe('#expressRouteConnectionsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteConnectionsCreateOrUpdate(expressRouteConnectionsResourceGroupName, expressRouteConnectionsExpressRouteGatewayName, expressRouteConnectionsConnectionName, expressRouteConnectionsExpressRouteConnectionsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteConnections', 'expressRouteConnectionsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteConnectionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteConnectionsGet(expressRouteConnectionsResourceGroupName, expressRouteConnectionsExpressRouteGatewayName, expressRouteConnectionsConnectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteConnections', 'expressRouteConnectionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipGroupsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IpGroups', 'ipGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipGroupsResourceGroupName = 'fakedata';
    describe('#ipGroupsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipGroupsListByResourceGroup(ipGroupsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IpGroups', 'ipGroupsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipGroupsIpGroupsName = 'fakedata';
    const ipGroupsIpGroupsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#ipGroupsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipGroupsCreateOrUpdate(ipGroupsResourceGroupName, ipGroupsIpGroupsName, ipGroupsIpGroupsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IpGroups', 'ipGroupsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipGroupsIpGroupsUpdateGroupsBodyParam = {
      tags: {}
    };
    describe('#ipGroupsUpdateGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipGroupsUpdateGroups(ipGroupsResourceGroupName, ipGroupsIpGroupsName, ipGroupsIpGroupsUpdateGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IpGroups', 'ipGroupsUpdateGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipGroupsGet(ipGroupsResourceGroupName, ipGroupsIpGroupsName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IpGroups', 'ipGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webApplicationFirewallPoliciesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebApplicationFirewallPolicies', 'webApplicationFirewallPoliciesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webApplicationFirewallPoliciesResourceGroupName = 'fakedata';
    describe('#webApplicationFirewallPoliciesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webApplicationFirewallPoliciesList(webApplicationFirewallPoliciesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebApplicationFirewallPolicies', 'webApplicationFirewallPoliciesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webApplicationFirewallPoliciesPolicyName = 'fakedata';
    const webApplicationFirewallPoliciesWebApplicationFirewallPoliciesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#webApplicationFirewallPoliciesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webApplicationFirewallPoliciesCreateOrUpdate(webApplicationFirewallPoliciesResourceGroupName, webApplicationFirewallPoliciesPolicyName, webApplicationFirewallPoliciesWebApplicationFirewallPoliciesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebApplicationFirewallPolicies', 'webApplicationFirewallPoliciesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webApplicationFirewallPoliciesGet(webApplicationFirewallPoliciesResourceGroupName, webApplicationFirewallPoliciesPolicyName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebApplicationFirewallPolicies', 'webApplicationFirewallPoliciesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkSecurityGroupsListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSecurityGroups', 'networkSecurityGroupsListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSecurityGroupsResourceGroupName = 'fakedata';
    describe('#networkSecurityGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkSecurityGroupsList(networkSecurityGroupsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSecurityGroups', 'networkSecurityGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSecurityGroupsNetworkSecurityGroupName = 'fakedata';
    const networkSecurityGroupsNetworkSecurityGroupsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#networkSecurityGroupsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkSecurityGroupsCreateOrUpdate(networkSecurityGroupsResourceGroupName, networkSecurityGroupsNetworkSecurityGroupName, networkSecurityGroupsNetworkSecurityGroupsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSecurityGroups', 'networkSecurityGroupsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSecurityGroupsNetworkSecurityGroupsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#networkSecurityGroupsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkSecurityGroupsUpdateTags(networkSecurityGroupsResourceGroupName, networkSecurityGroupsNetworkSecurityGroupName, networkSecurityGroupsNetworkSecurityGroupsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSecurityGroups', 'networkSecurityGroupsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkSecurityGroupsGet(networkSecurityGroupsResourceGroupName, networkSecurityGroupsNetworkSecurityGroupName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSecurityGroups', 'networkSecurityGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityRulesResourceGroupName = 'fakedata';
    const securityRulesNetworkSecurityGroupName = 'fakedata';
    describe('#defaultSecurityRulesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.defaultSecurityRulesList(securityRulesResourceGroupName, securityRulesNetworkSecurityGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRules', 'defaultSecurityRulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityRulesDefaultSecurityRuleName = 'fakedata';
    describe('#defaultSecurityRulesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.defaultSecurityRulesGet(securityRulesResourceGroupName, securityRulesNetworkSecurityGroupName, securityRulesDefaultSecurityRuleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRules', 'defaultSecurityRulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.securityRulesList(securityRulesResourceGroupName, securityRulesNetworkSecurityGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRules', 'securityRulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityRulesSecurityRuleName = 'fakedata';
    const securityRulesSecurityRulesCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#securityRulesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.securityRulesCreateOrUpdate(securityRulesResourceGroupName, securityRulesNetworkSecurityGroupName, securityRulesSecurityRuleName, securityRulesSecurityRulesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRules', 'securityRulesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.securityRulesGet(securityRulesResourceGroupName, securityRulesNetworkSecurityGroupName, securityRulesSecurityRuleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRules', 'securityRulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkProfilesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkProfiles', 'networkProfilesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkProfilesResourceGroupName = 'fakedata';
    describe('#networkProfilesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkProfilesList(networkProfilesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkProfiles', 'networkProfilesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkProfilesNetworkProfileName = 'fakedata';
    const networkProfilesNetworkProfilesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#networkProfilesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkProfilesCreateOrUpdate(networkProfilesResourceGroupName, networkProfilesNetworkProfileName, networkProfilesNetworkProfilesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkProfiles', 'networkProfilesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkProfilesNetworkProfilesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#networkProfilesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkProfilesUpdateTags(networkProfilesResourceGroupName, networkProfilesNetworkProfileName, networkProfilesNetworkProfilesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkProfiles', 'networkProfilesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkProfilesGet(networkProfilesResourceGroupName, networkProfilesNetworkProfileName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkProfiles', 'networkProfilesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallFqdnTagsListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.azureFirewallFqdnTagsListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewallFqdnTags', 'azureFirewallFqdnTagsListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.natGatewaysListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NatGateways', 'natGatewaysListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const natGatewaysResourceGroupName = 'fakedata';
    describe('#natGatewaysList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.natGatewaysList(natGatewaysResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NatGateways', 'natGatewaysList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const natGatewaysNatGatewayName = 'fakedata';
    const natGatewaysNatGatewaysCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#natGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.natGatewaysCreateOrUpdate(natGatewaysResourceGroupName, natGatewaysNatGatewayName, natGatewaysNatGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NatGateways', 'natGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const natGatewaysNatGatewaysUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#natGatewaysUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.natGatewaysUpdateTags(natGatewaysResourceGroupName, natGatewaysNatGatewayName, natGatewaysNatGatewaysUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NatGateways', 'natGatewaysUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.natGatewaysGet(natGatewaysResourceGroupName, natGatewaysNatGatewayName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NatGateways', 'natGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworks', 'virtualNetworksListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworksResourceGroupName = 'fakedata';
    describe('#virtualNetworksList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksList(virtualNetworksResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworks', 'virtualNetworksList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworksVirtualNetworkName = 'fakedata';
    const virtualNetworksVirtualNetworksCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#virtualNetworksCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksCreateOrUpdate(virtualNetworksResourceGroupName, virtualNetworksVirtualNetworkName, virtualNetworksVirtualNetworksCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworks', 'virtualNetworksCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworksVirtualNetworksUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#virtualNetworksUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksUpdateTags(virtualNetworksResourceGroupName, virtualNetworksVirtualNetworkName, virtualNetworksVirtualNetworksUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworks', 'virtualNetworksUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworksGet(virtualNetworksResourceGroupName, virtualNetworksVirtualNetworkName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworks', 'virtualNetworksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetsResourceGroupName = 'fakedata';
    const subnetsVirtualNetworkName = 'fakedata';
    describe('#subnetsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.subnetsList(subnetsResourceGroupName, subnetsVirtualNetworkName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'subnetsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetsSubnetName = 'fakedata';
    const subnetsSubnetsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#subnetsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.subnetsCreateOrUpdate(subnetsResourceGroupName, subnetsVirtualNetworkName, subnetsSubnetName, subnetsSubnetsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'subnetsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.subnetsGet(subnetsResourceGroupName, subnetsVirtualNetworkName, subnetsSubnetName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'subnetsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkPeeringsResourceGroupName = 'fakedata';
    const virtualNetworkPeeringsVirtualNetworkName = 'fakedata';
    describe('#virtualNetworkPeeringsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkPeeringsList(virtualNetworkPeeringsResourceGroupName, virtualNetworkPeeringsVirtualNetworkName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkPeerings', 'virtualNetworkPeeringsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkPeeringsVirtualNetworkPeeringName = 'fakedata';
    const virtualNetworkPeeringsVirtualNetworkPeeringsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#virtualNetworkPeeringsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkPeeringsCreateOrUpdate(virtualNetworkPeeringsResourceGroupName, virtualNetworkPeeringsVirtualNetworkName, virtualNetworkPeeringsVirtualNetworkPeeringName, virtualNetworkPeeringsVirtualNetworkPeeringsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkPeerings', 'virtualNetworkPeeringsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkPeeringsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkPeeringsGet(virtualNetworkPeeringsResourceGroupName, virtualNetworkPeeringsVirtualNetworkName, virtualNetworkPeeringsVirtualNetworkPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkPeerings', 'virtualNetworkPeeringsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPoliciesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicies', 'firewallPoliciesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallPoliciesResourceGroupName = 'fakedata';
    describe('#firewallPoliciesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPoliciesList(firewallPoliciesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicies', 'firewallPoliciesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallPoliciesFirewallPolicyName = 'fakedata';
    const firewallPoliciesFirewallPoliciesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#firewallPoliciesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPoliciesCreateOrUpdate(firewallPoliciesResourceGroupName, firewallPoliciesFirewallPolicyName, firewallPoliciesFirewallPoliciesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicies', 'firewallPoliciesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPoliciesGet(firewallPoliciesResourceGroupName, firewallPoliciesFirewallPolicyName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicies', 'firewallPoliciesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallPolicyRuleGroupsResourceGroupName = 'fakedata';
    const firewallPolicyRuleGroupsFirewallPolicyName = 'fakedata';
    describe('#firewallPolicyRuleGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPolicyRuleGroupsList(firewallPolicyRuleGroupsResourceGroupName, firewallPolicyRuleGroupsFirewallPolicyName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicyRuleGroups', 'firewallPolicyRuleGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallPolicyRuleGroupsRuleGroupName = 'fakedata';
    const firewallPolicyRuleGroupsFirewallPolicyRuleGroupsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#firewallPolicyRuleGroupsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPolicyRuleGroupsCreateOrUpdate(firewallPolicyRuleGroupsResourceGroupName, firewallPolicyRuleGroupsFirewallPolicyName, firewallPolicyRuleGroupsRuleGroupName, firewallPolicyRuleGroupsFirewallPolicyRuleGroupsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicyRuleGroups', 'firewallPolicyRuleGroupsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPolicyRuleGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallPolicyRuleGroupsGet(firewallPolicyRuleGroupsResourceGroupName, firewallPolicyRuleGroupsFirewallPolicyName, firewallPolicyRuleGroupsRuleGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicyRuleGroups', 'firewallPolicyRuleGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancersListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancersListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersResourceGroupName = 'fakedata';
    describe('#loadBalancersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancersList(loadBalancersResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersLoadBalancerName = 'fakedata';
    const loadBalancersLoadBalancersCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#loadBalancersCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancersCreateOrUpdate(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersLoadBalancersCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancersCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersLoadBalancersUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#loadBalancersUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancersUpdateTags(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersLoadBalancersUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancersUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancersGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerBackendAddressPoolsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerBackendAddressPoolsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersBackendAddressPoolName = 'fakedata';
    describe('#loadBalancerBackendAddressPoolsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersBackendAddressPoolName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerBackendAddressPoolsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerFrontendIPConfigurationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerFrontendIPConfigurationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersFrontendIPConfigurationName = 'fakedata';
    describe('#loadBalancerFrontendIPConfigurationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersFrontendIPConfigurationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerFrontendIPConfigurationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inboundNatRulesList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'inboundNatRulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersInboundNatRuleName = 'fakedata';
    const loadBalancersInboundNatRulesCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#inboundNatRulesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inboundNatRulesCreateOrUpdate(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersInboundNatRuleName, loadBalancersInboundNatRulesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'inboundNatRulesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inboundNatRulesGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersInboundNatRuleName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'inboundNatRulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerLoadBalancingRulesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerLoadBalancingRulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersLoadBalancingRuleName = 'fakedata';
    describe('#loadBalancerLoadBalancingRulesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersLoadBalancingRuleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerLoadBalancingRulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerNetworkInterfacesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerNetworkInterfacesList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerNetworkInterfacesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerOutboundRulesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerOutboundRulesList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerOutboundRulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersOutboundRuleName = 'fakedata';
    describe('#loadBalancerOutboundRulesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerOutboundRulesGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersOutboundRuleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerOutboundRulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerProbesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerProbesList(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerProbesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loadBalancersProbeName = 'fakedata';
    describe('#loadBalancerProbesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadBalancerProbesGet(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersProbeName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancerProbesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPPrefixesListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPPrefixes', 'publicIPPrefixesListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicIPPrefixesResourceGroupName = 'fakedata';
    describe('#publicIPPrefixesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPPrefixesList(publicIPPrefixesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPPrefixes', 'publicIPPrefixesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicIPPrefixesPublicIpPrefixName = 'fakedata';
    const publicIPPrefixesPublicIPPrefixesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#publicIPPrefixesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPPrefixesCreateOrUpdate(publicIPPrefixesResourceGroupName, publicIPPrefixesPublicIpPrefixName, publicIPPrefixesPublicIPPrefixesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPPrefixes', 'publicIPPrefixesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicIPPrefixesPublicIPPrefixesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#publicIPPrefixesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPPrefixesUpdateTags(publicIPPrefixesResourceGroupName, publicIPPrefixesPublicIpPrefixName, publicIPPrefixesPublicIPPrefixesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPPrefixes', 'publicIPPrefixesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publicIPPrefixesGet(publicIPPrefixesResourceGroupName, publicIPPrefixesPublicIpPrefixName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPPrefixes', 'publicIPPrefixesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#operationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.operationsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Providers', 'operationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bgpServiceCommunitiesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bgpServiceCommunitiesList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BgpServiceCommunities', 'bgpServiceCommunitiesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGatewaysResourceGroupName = 'fakedata';
    const applicationGatewaysApplicationGatewayName = 'fakedata';
    describe('#applicationGatewaysBackendHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysBackendHealth(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.backendAddressPools));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysBackendHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGatewaysApplicationGatewaysBackendHealthOnDemandBodyParam = {
      protocol: 'Https',
      host: 'string',
      path: 'string',
      timeout: 9,
      pickHostNameFromBackendHttpSettings: false,
      match: {
        body: 'string',
        statusCodes: [
          'string'
        ]
      },
      backendAddressPool: {
        id: 'string'
      },
      backendHttpSettings: {
        id: 'string'
      }
    };
    describe('#applicationGatewaysBackendHealthOnDemand - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysBackendHealthOnDemand(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, applicationGatewaysApplicationGatewaysBackendHealthOnDemandBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.backendAddressPool);
                assert.equal('object', typeof data.response.backendHealthHttpSettings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysBackendHealthOnDemand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysStart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applicationGatewaysStart(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysStart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysStop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applicationGatewaysStop(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableRequestHeaders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAvailableRequestHeaders((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAvailableRequestHeaders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableResponseHeaders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAvailableResponseHeaders((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAvailableResponseHeaders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableServerVariables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAvailableServerVariables((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAvailableServerVariables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableSslOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAvailableSslOptions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAvailableSslOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableSslPredefinedPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAvailableSslPredefinedPolicies((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAvailableSslPredefinedPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGatewaysPredefinedPolicyName = 'fakedata';
    describe('#applicationGatewaysGetSslPredefinedPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysGetSslPredefinedPolicy(applicationGatewaysPredefinedPolicyName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysGetSslPredefinedPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableWafRuleSets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAvailableWafRuleSets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAvailableWafRuleSets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysList(applicationGatewaysResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGatewaysApplicationGatewaysCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#applicationGatewaysCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysCreateOrUpdate(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, applicationGatewaysApplicationGatewaysCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGatewaysApplicationGatewaysUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#applicationGatewaysUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysUpdateTags(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, applicationGatewaysApplicationGatewaysUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGatewaysGet(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsLocationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsLocationsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePortsLocations', 'expressRoutePortsLocationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRoutePortsLocationsLocationName = 'fakedata';
    describe('#expressRoutePortsLocationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsLocationsGet(expressRoutePortsLocationsLocationName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePortsLocations', 'expressRoutePortsLocationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePorts', 'expressRoutePortsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRoutePortsResourceGroupName = 'fakedata';
    describe('#expressRoutePortsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsListByResourceGroup(expressRoutePortsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePorts', 'expressRoutePortsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRoutePortsExpressRoutePortName = 'fakedata';
    const expressRoutePortsExpressRoutePortsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#expressRoutePortsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsCreateOrUpdate(expressRoutePortsResourceGroupName, expressRoutePortsExpressRoutePortName, expressRoutePortsExpressRoutePortsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePorts', 'expressRoutePortsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRoutePortsExpressRoutePortsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#expressRoutePortsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsUpdateTags(expressRoutePortsResourceGroupName, expressRoutePortsExpressRoutePortName, expressRoutePortsExpressRoutePortsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePorts', 'expressRoutePortsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRoutePortsGet(expressRoutePortsResourceGroupName, expressRoutePortsExpressRoutePortName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePorts', 'expressRoutePortsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteLinksResourceGroupName = 'fakedata';
    const expressRouteLinksExpressRoutePortName = 'fakedata';
    describe('#expressRouteLinksList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteLinksList(expressRouteLinksResourceGroupName, expressRouteLinksExpressRoutePortName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteLinks', 'expressRouteLinksList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expressRouteLinksLinkName = 'fakedata';
    describe('#expressRouteLinksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expressRouteLinksGet(expressRouteLinksResourceGroupName, expressRouteLinksExpressRoutePortName, expressRouteLinksLinkName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteLinks', 'expressRouteLinksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsListBySubscription - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateEndpointsListBySubscription((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateEndpoints', 'privateEndpointsListBySubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateEndpointsResourceGroupName = 'fakedata';
    describe('#privateEndpointsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateEndpointsList(privateEndpointsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateEndpoints', 'privateEndpointsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateEndpointsPrivateEndpointName = 'fakedata';
    const privateEndpointsPrivateEndpointsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#privateEndpointsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateEndpointsCreateOrUpdate(privateEndpointsResourceGroupName, privateEndpointsPrivateEndpointName, privateEndpointsPrivateEndpointsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateEndpoints', 'privateEndpointsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateEndpointsGet(privateEndpointsResourceGroupName, privateEndpointsPrivateEndpointName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateEndpoints', 'privateEndpointsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationSecurityGroupsListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationSecurityGroups', 'applicationSecurityGroupsListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationSecurityGroupsResourceGroupName = 'fakedata';
    describe('#applicationSecurityGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationSecurityGroupsList(applicationSecurityGroupsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationSecurityGroups', 'applicationSecurityGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationSecurityGroupsApplicationSecurityGroupName = 'fakedata';
    const applicationSecurityGroupsApplicationSecurityGroupsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#applicationSecurityGroupsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationSecurityGroupsCreateOrUpdate(applicationSecurityGroupsResourceGroupName, applicationSecurityGroupsApplicationSecurityGroupName, applicationSecurityGroupsApplicationSecurityGroupsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationSecurityGroups', 'applicationSecurityGroupsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationSecurityGroupsApplicationSecurityGroupsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#applicationSecurityGroupsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationSecurityGroupsUpdateTags(applicationSecurityGroupsResourceGroupName, applicationSecurityGroupsApplicationSecurityGroupName, applicationSecurityGroupsApplicationSecurityGroupsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationSecurityGroups', 'applicationSecurityGroupsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationSecurityGroupsGet(applicationSecurityGroupsResourceGroupName, applicationSecurityGroupsApplicationSecurityGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationSecurityGroups', 'applicationSecurityGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPoliciesList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicies', 'serviceEndpointPoliciesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceEndpointPoliciesResourceGroupName = 'fakedata';
    describe('#serviceEndpointPoliciesListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPoliciesListByResourceGroup(serviceEndpointPoliciesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicies', 'serviceEndpointPoliciesListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceEndpointPoliciesServiceEndpointPolicyName = 'fakedata';
    const serviceEndpointPoliciesServiceEndpointPoliciesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#serviceEndpointPoliciesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPoliciesCreateOrUpdate(serviceEndpointPoliciesResourceGroupName, serviceEndpointPoliciesServiceEndpointPolicyName, serviceEndpointPoliciesServiceEndpointPoliciesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicies', 'serviceEndpointPoliciesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceEndpointPoliciesServiceEndpointPoliciesUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#serviceEndpointPoliciesUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPoliciesUpdateTags(serviceEndpointPoliciesResourceGroupName, serviceEndpointPoliciesServiceEndpointPolicyName, serviceEndpointPoliciesServiceEndpointPoliciesUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicies', 'serviceEndpointPoliciesUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPoliciesGet(serviceEndpointPoliciesResourceGroupName, serviceEndpointPoliciesServiceEndpointPolicyName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicies', 'serviceEndpointPoliciesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceEndpointPolicyDefinitionsResourceGroupName = 'fakedata';
    const serviceEndpointPolicyDefinitionsServiceEndpointPolicyName = 'fakedata';
    describe('#serviceEndpointPolicyDefinitionsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsListByResourceGroup(serviceEndpointPolicyDefinitionsResourceGroupName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicyDefinitions', 'serviceEndpointPolicyDefinitionsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceEndpointPolicyDefinitionsServiceEndpointPolicyDefinitionName = 'fakedata';
    const serviceEndpointPolicyDefinitionsServiceEndpointPolicyDefinitionsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#serviceEndpointPolicyDefinitionsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsCreateOrUpdate(serviceEndpointPolicyDefinitionsResourceGroupName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyDefinitionName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyDefinitionsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicyDefinitions', 'serviceEndpointPolicyDefinitionsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPolicyDefinitionsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsGet(serviceEndpointPolicyDefinitionsResourceGroupName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyDefinitionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicyDefinitions', 'serviceEndpointPolicyDefinitionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRoutersList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouters', 'virtualRoutersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualRoutersResourceGroupName = 'fakedata';
    describe('#virtualRoutersListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRoutersListByResourceGroup(virtualRoutersResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouters', 'virtualRoutersListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualRoutersVirtualRouterName = 'fakedata';
    const virtualRoutersVirtualRoutersCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#virtualRoutersCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRoutersCreateOrUpdate(virtualRoutersResourceGroupName, virtualRoutersVirtualRouterName, virtualRoutersVirtualRoutersCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouters', 'virtualRoutersCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRoutersGet(virtualRoutersResourceGroupName, virtualRoutersVirtualRouterName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouters', 'virtualRoutersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualRouterPeeringsResourceGroupName = 'fakedata';
    const virtualRouterPeeringsVirtualRouterName = 'fakedata';
    describe('#virtualRouterPeeringsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRouterPeeringsList(virtualRouterPeeringsResourceGroupName, virtualRouterPeeringsVirtualRouterName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouterPeerings', 'virtualRouterPeeringsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualRouterPeeringsPeeringName = 'fakedata';
    const virtualRouterPeeringsVirtualRouterPeeringsCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#virtualRouterPeeringsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRouterPeeringsCreateOrUpdate(virtualRouterPeeringsResourceGroupName, virtualRouterPeeringsVirtualRouterName, virtualRouterPeeringsPeeringName, virtualRouterPeeringsVirtualRouterPeeringsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouterPeerings', 'virtualRouterPeeringsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRouterPeeringsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualRouterPeeringsGet(virtualRouterPeeringsResourceGroupName, virtualRouterPeeringsVirtualRouterName, virtualRouterPeeringsPeeringName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouterPeerings', 'virtualRouterPeeringsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkTapResourceGroupName = 'fakedata';
    const virtualNetworkTapTapName = 'fakedata';
    const virtualNetworkTapVirtualNetworkTapsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#virtualNetworkTapsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkTapsCreateOrUpdate(virtualNetworkTapResourceGroupName, virtualNetworkTapTapName, virtualNetworkTapVirtualNetworkTapsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkTap', 'virtualNetworkTapsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkTapVirtualNetworkTapsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#virtualNetworkTapsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkTapsUpdateTags(virtualNetworkTapResourceGroupName, virtualNetworkTapTapName, virtualNetworkTapVirtualNetworkTapsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkTap', 'virtualNetworkTapsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkTapsGet(virtualNetworkTapResourceGroupName, virtualNetworkTapTapName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkTap', 'virtualNetworkTapsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkTapsListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkTaps', 'virtualNetworkTapsListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualNetworkTapsResourceGroupName = 'fakedata';
    describe('#virtualNetworkTapsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualNetworkTapsListByResourceGroup(virtualNetworkTapsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkTaps', 'virtualNetworkTapsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.azureFirewallsListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewalls', 'azureFirewallsListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureFirewallsResourceGroupName = 'fakedata';
    describe('#azureFirewallsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.azureFirewallsList(azureFirewallsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewalls', 'azureFirewallsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureFirewallsAzureFirewallName = 'fakedata';
    const azureFirewallsAzureFirewallsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#azureFirewallsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.azureFirewallsCreateOrUpdate(azureFirewallsResourceGroupName, azureFirewallsAzureFirewallName, azureFirewallsAzureFirewallsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewalls', 'azureFirewallsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureFirewallsAzureFirewallsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#azureFirewallsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.azureFirewallsUpdateTags(azureFirewallsResourceGroupName, azureFirewallsAzureFirewallName, azureFirewallsAzureFirewallsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewalls', 'azureFirewallsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.azureFirewallsGet(azureFirewallsResourceGroupName, azureFirewallsAzureFirewallName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewalls', 'azureFirewallsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersResourceGroupName = 'fakedata';
    const networkWatchersNetworkWatcherName = 'fakedata';
    const networkWatchersNetworkWatchersListAvailableProvidersBodyParam = {
      azureLocations: [
        'string'
      ],
      country: 'string',
      state: 'string',
      city: 'string'
    };
    describe('#networkWatchersListAvailableProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersListAvailableProviders(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersListAvailableProvidersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.countries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersListAvailableProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetAzureReachabilityReportBodyParam = {
      providerLocation: {},
      startTime: 'string',
      endTime: 'string'
    };
    describe('#networkWatchersGetAzureReachabilityReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetAzureReachabilityReport(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetAzureReachabilityReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.aggregationLevel);
                assert.equal('object', typeof data.response.providerLocation);
                assert.equal(true, Array.isArray(data.response.reachabilityReport));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetAzureReachabilityReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersSetFlowLogConfigurationBodyParam = {
      targetResourceId: 'string',
      properties: {}
    };
    describe('#networkWatchersSetFlowLogConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersSetFlowLogConfiguration(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersSetFlowLogConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.targetResourceId);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.flowAnalyticsConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersSetFlowLogConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersCheckConnectivityBodyParam = {
      source: {},
      destination: {}
    };
    describe('#networkWatchersCheckConnectivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersCheckConnectivity(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersCheckConnectivityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.hops));
                assert.equal('Connected', data.response.connectionStatus);
                assert.equal(1, data.response.avgLatencyInMs);
                assert.equal(1, data.response.minLatencyInMs);
                assert.equal(4, data.response.maxLatencyInMs);
                assert.equal(6, data.response.probesSent);
                assert.equal(9, data.response.probesFailed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersCheckConnectivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersVerifyIPFlowBodyParam = {
      targetResourceId: 'string',
      direction: 'Inbound',
      protocol: 'TCP',
      localPort: 'string',
      remotePort: 'string',
      localIPAddress: 'string',
      remoteIPAddress: 'string'
    };
    describe('#networkWatchersVerifyIPFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersVerifyIPFlow(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersVerifyIPFlowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Allow', data.response.access);
                assert.equal('string', data.response.ruleName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersVerifyIPFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetNetworkConfigurationDiagnosticBodyParam = {
      targetResourceId: 'string',
      profiles: [
        {}
      ]
    };
    describe('#networkWatchersGetNetworkConfigurationDiagnostic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetNetworkConfigurationDiagnostic(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetNetworkConfigurationDiagnosticBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetNetworkConfigurationDiagnostic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetNextHopBodyParam = {
      targetResourceId: 'string',
      sourceIPAddress: 'string',
      destinationIPAddress: 'string'
    };
    describe('#networkWatchersGetNextHop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetNextHop(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetNextHopBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('VnetLocal', data.response.nextHopType);
                assert.equal('string', data.response.nextHopIpAddress);
                assert.equal('string', data.response.routeTableId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetNextHop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetFlowLogStatusBodyParam = {
      targetResourceId: 'string'
    };
    describe('#networkWatchersGetFlowLogStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetFlowLogStatus(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetFlowLogStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.targetResourceId);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.flowAnalyticsConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetFlowLogStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetTroubleshootingResultBodyParam = {
      targetResourceId: 'string'
    };
    describe('#networkWatchersGetTroubleshootingResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetTroubleshootingResult(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetTroubleshootingResultBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.startTime);
                assert.equal('string', data.response.endTime);
                assert.equal('string', data.response.code);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetTroubleshootingResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetVMSecurityRulesBodyParam = {
      targetResourceId: 'string'
    };
    describe('#networkWatchersGetVMSecurityRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetVMSecurityRules(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetVMSecurityRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.networkInterfaces));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetVMSecurityRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetTopologyBodyParam = {
      targetResourceGroupName: 'string',
      targetVirtualNetwork: {
        id: 'string'
      },
      targetSubnet: {
        id: 'string'
      }
    };
    describe('#networkWatchersGetTopology - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetTopology(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetTopologyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.createdDateTime);
                assert.equal('string', data.response.lastModified);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersGetTroubleshootingBodyParam = {
      targetResourceId: 'string',
      properties: {}
    };
    describe('#networkWatchersGetTroubleshooting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGetTroubleshooting(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersGetTroubleshootingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.startTime);
                assert.equal('string', data.response.endTime);
                assert.equal('string', data.response.code);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGetTroubleshooting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersListAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersListAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersList(networkWatchersResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#networkWatchersCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersCreateOrUpdate(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkWatchersNetworkWatchersUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#networkWatchersUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersUpdateTags(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, networkWatchersNetworkWatchersUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkWatchersGet(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packetCapturesResourceGroupName = 'fakedata';
    const packetCapturesNetworkWatcherName = 'fakedata';
    const packetCapturesPacketCaptureName = 'fakedata';
    describe('#packetCapturesGetStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.packetCapturesGetStatus(packetCapturesResourceGroupName, packetCapturesNetworkWatcherName, packetCapturesPacketCaptureName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.captureStartTime);
                assert.equal('Running', data.response.packetCaptureStatus);
                assert.equal('string', data.response.stopReason);
                assert.equal(true, Array.isArray(data.response.packetCaptureError));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCaptures', 'packetCapturesGetStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesStop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.packetCapturesStop(packetCapturesResourceGroupName, packetCapturesNetworkWatcherName, packetCapturesPacketCaptureName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCaptures', 'packetCapturesStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.packetCapturesList(packetCapturesResourceGroupName, packetCapturesNetworkWatcherName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCaptures', 'packetCapturesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packetCapturesPacketCapturesCreateBodyParam = {
      properties: {}
    };
    describe('#packetCapturesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.packetCapturesCreate(packetCapturesResourceGroupName, packetCapturesNetworkWatcherName, packetCapturesPacketCaptureName, packetCapturesPacketCapturesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCaptures', 'packetCapturesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.packetCapturesGet(packetCapturesResourceGroupName, packetCapturesNetworkWatcherName, packetCapturesPacketCaptureName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.etag);
                assert.equal('object', typeof data.response.properties);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCaptures', 'packetCapturesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const connectionMonitorsResourceGroupName = 'fakedata';
    const connectionMonitorsNetworkWatcherName = 'fakedata';
    const connectionMonitorsConnectionMonitorName = 'fakedata';
    describe('#connectionMonitorsQuery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectionMonitorsQuery(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Inactive', data.response.sourceStatus);
                assert.equal(true, Array.isArray(data.response.states));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsStart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.connectionMonitorsStart(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsStart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsStop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.connectionMonitorsStop(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectionMonitorsList(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const connectionMonitorsConnectionMonitorsCreateOrUpdateBodyParam = {
      properties: {}
    };
    describe('#connectionMonitorsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectionMonitorsCreateOrUpdate(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, connectionMonitorsConnectionMonitorsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const connectionMonitorsConnectionMonitorsUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#connectionMonitorsUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectionMonitorsUpdateTags(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, connectionMonitorsConnectionMonitorsUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectionMonitorsGet(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.etag);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.properties);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowLogsResourceGroupName = 'fakedata';
    const flowLogsNetworkWatcherName = 'fakedata';
    describe('#flowLogsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowLogsList(flowLogsResourceGroupName, flowLogsNetworkWatcherName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowLogs', 'flowLogsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowLogsFlowLogName = 'fakedata';
    const flowLogsFlowLogsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#flowLogsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowLogsCreateOrUpdate(flowLogsResourceGroupName, flowLogsNetworkWatcherName, flowLogsFlowLogName, flowLogsFlowLogsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowLogs', 'flowLogsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowLogsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowLogsGet(flowLogsResourceGroupName, flowLogsNetworkWatcherName, flowLogsFlowLogName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowLogs', 'flowLogsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateLinkServicesLocation = 'fakedata';
    const privateLinkServicesPrivateLinkServicesCheckPrivateLinkServiceVisibilityBodyParam = {
      privateLinkServiceAlias: 'string'
    };
    describe('#privateLinkServicesCheckPrivateLinkServiceVisibility - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibility(privateLinkServicesLocation, privateLinkServicesPrivateLinkServicesCheckPrivateLinkServiceVisibilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.visible);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesCheckPrivateLinkServiceVisibility', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateLinkServicesResourceGroupName = 'fakedata';
    const privateLinkServicesPrivateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroupBodyParam = {
      privateLinkServiceAlias: 'string'
    };
    describe('#privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup(privateLinkServicesLocation, privateLinkServicesResourceGroupName, privateLinkServicesPrivateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.visible);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListAutoApprovedPrivateLinkServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesListAutoApprovedPrivateLinkServices(privateLinkServicesLocation, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesListAutoApprovedPrivateLinkServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListBySubscription - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesListBySubscription((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesListBySubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup(privateLinkServicesLocation, privateLinkServicesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesList(privateLinkServicesResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateLinkServicesServiceName = 'fakedata';
    describe('#privateLinkServicesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesGet(privateLinkServicesResourceGroupName, privateLinkServicesServiceName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListPrivateEndpointConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesListPrivateEndpointConnections(privateLinkServicesResourceGroupName, privateLinkServicesServiceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesListPrivateEndpointConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateLinkServicesPeConnectionName = 'fakedata';
    const privateLinkServicesPrivateLinkServicesUpdatePrivateEndpointConnectionBodyParam = {
      id: 'string'
    };
    describe('#privateLinkServicesUpdatePrivateEndpointConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesUpdatePrivateEndpointConnection(privateLinkServicesResourceGroupName, privateLinkServicesServiceName, privateLinkServicesPeConnectionName, privateLinkServicesPrivateLinkServicesUpdatePrivateEndpointConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesUpdatePrivateEndpointConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesGetPrivateEndpointConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesGetPrivateEndpointConnection(privateLinkServicesResourceGroupName, privateLinkServicesServiceName, privateLinkServicesPeConnectionName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesGetPrivateEndpointConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateLinkServiceResourceGroupName = 'fakedata';
    const privateLinkServiceServiceName = 'fakedata';
    const privateLinkServicePrivateLinkServicesCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#privateLinkServicesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.privateLinkServicesCreateOrUpdate(privateLinkServiceResourceGroupName, privateLinkServiceServiceName, privateLinkServicePrivateLinkServicesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkService', 'privateLinkServicesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bastionHostsList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BastionHosts', 'bastionHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bastionHostsResourceGroupName = 'fakedata';
    describe('#bastionHostsListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bastionHostsListByResourceGroup(bastionHostsResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BastionHosts', 'bastionHostsListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bastionHostsBastionHostName = 'fakedata';
    const bastionHostsBastionHostsCreateOrUpdateBodyParam = {
      id: 'string',
      name: 'string',
      type: 'string',
      location: 'string',
      tags: {}
    };
    describe('#bastionHostsCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bastionHostsCreateOrUpdate(bastionHostsResourceGroupName, bastionHostsBastionHostName, bastionHostsBastionHostsCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BastionHosts', 'bastionHostsCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bastionHostsGet(bastionHostsResourceGroupName, bastionHostsBastionHostName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BastionHosts', 'bastionHostsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFiltersList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilters', 'routeFiltersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeFiltersResourceGroupName = 'fakedata';
    describe('#routeFiltersListByResourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFiltersListByResourceGroup(routeFiltersResourceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilters', 'routeFiltersListByResourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeFiltersRouteFilterName = 'fakedata';
    const routeFiltersRouteFiltersCreateOrUpdateBodyParam = {
      location: 'string'
    };
    describe('#routeFiltersCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFiltersCreateOrUpdate(routeFiltersResourceGroupName, routeFiltersRouteFilterName, routeFiltersRouteFiltersCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilters', 'routeFiltersCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeFiltersRouteFiltersUpdateTagsBodyParam = {
      tags: {}
    };
    describe('#routeFiltersUpdateTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFiltersUpdateTags(routeFiltersResourceGroupName, routeFiltersRouteFilterName, routeFiltersRouteFiltersUpdateTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilters', 'routeFiltersUpdateTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFiltersGet(routeFiltersResourceGroupName, routeFiltersRouteFilterName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilters', 'routeFiltersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeFilterRulesResourceGroupName = 'fakedata';
    const routeFilterRulesRouteFilterName = 'fakedata';
    describe('#routeFilterRulesListByRouteFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFilterRulesListByRouteFilter(routeFilterRulesResourceGroupName, routeFilterRulesRouteFilterName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('string', data.response.nextLink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilterRules', 'routeFilterRulesListByRouteFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeFilterRulesRuleName = 'fakedata';
    const routeFilterRulesRouteFilterRulesCreateOrUpdateBodyParam = {
      id: 'string'
    };
    describe('#routeFilterRulesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFilterRulesCreateOrUpdate(routeFilterRulesResourceGroupName, routeFilterRulesRouteFilterName, routeFilterRulesRuleName, routeFilterRulesRouteFilterRulesCreateOrUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilterRules', 'routeFilterRulesCreateOrUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFilterRulesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.routeFilterRulesGet(routeFilterRulesResourceGroupName, routeFilterRulesRouteFilterName, routeFilterRulesRuleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilterRules', 'routeFilterRulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkInterfacesDelete(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfacesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkInterfaceTapConfigurationsDelete(networkInterfacesResourceGroupName, networkInterfacesNetworkInterfaceName, networkInterfacesTapConfigurationName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkInterfaces', 'networkInterfaceTapConfigurationsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.p2sVpnGatewaysDelete(subscriptionsResourceGroupName, subscriptionsGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'p2sVpnGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualHubsDelete(subscriptionsResourceGroupName, subscriptionsVirtualHubName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualHubRouteTableV2sDelete(subscriptionsResourceGroupName, subscriptionsVirtualHubName, subscriptionsRouteTableName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualHubRouteTableV2sDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualWansDelete(subscriptionsResourceGroupName, subscriptionsVirtualWANName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'virtualWansDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vpnGatewaysDelete(subscriptionsResourceGroupName, subscriptionsGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vpnConnectionsDelete(subscriptionsResourceGroupName, subscriptionsGatewayName, subscriptionsConnectionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnConnectionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vpnServerConfigurationsDelete(subscriptionsResourceGroupName, subscriptionsVpnServerConfigurationName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnServerConfigurationsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vpnSitesDelete(subscriptionsResourceGroupName, subscriptionsVpnSiteName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'vpnSitesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ddosProtectionPlansDelete(ddosProtectionPlansResourceGroupName, ddosProtectionPlansDdosProtectionPlanName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosProtectionPlans', 'ddosProtectionPlansDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitAuthorizationsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsDelete(expressRouteCircuitAuthorizationsResourceGroupName, expressRouteCircuitAuthorizationsCircuitName, expressRouteCircuitAuthorizationsAuthorizationName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitAuthorizations', 'expressRouteCircuitAuthorizationsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitPeeringsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteCircuitPeeringsDelete(expressRouteCircuitPeeringsResourceGroupName, expressRouteCircuitPeeringsCircuitName, expressRouteCircuitPeeringsPeeringName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitPeerings', 'expressRouteCircuitPeeringsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitConnectionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteCircuitConnectionsDelete(expressRouteCircuitConnectionsResourceGroupName, expressRouteCircuitConnectionsCircuitName, expressRouteCircuitConnectionsPeeringName, expressRouteCircuitConnectionsConnectionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuitConnections', 'expressRouteCircuitConnectionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteCircuitsDelete(expressRouteCircuitsResourceGroupName, expressRouteCircuitsCircuitName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCircuits', 'expressRouteCircuitsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.routeTablesDelete(routeTablesResourceGroupName, routeTablesRouteTableName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteTables', 'routeTablesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.routesDelete(routesResourceGroupName, routesRouteTableName, routesRouteName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routes', 'routesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosCustomPoliciesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ddosCustomPoliciesDelete(ddosCustomPoliciesResourceGroupName, ddosCustomPoliciesDdosCustomPolicyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DdosCustomPolicies', 'ddosCustomPoliciesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionPeeringsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsDelete(expressRouteCrossConnectionPeeringsResourceGroupName, expressRouteCrossConnectionPeeringsCrossConnectionName, expressRouteCrossConnectionPeeringsPeeringName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteCrossConnectionPeerings', 'expressRouteCrossConnectionPeeringsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.publicIPAddressesDelete(publicIPAddressesResourceGroupName, publicIPAddressesPublicIpAddressName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPAddresses', 'publicIPAddressesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewaysDelete(virtualNetworkGatewaysResourceGroupName, virtualNetworkGatewaysVirtualNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGateways', 'virtualNetworkGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsDelete(virtualNetworkGatewayConnectionsResourceGroupName, virtualNetworkGatewayConnectionsVirtualNetworkGatewayConnectionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkGatewayConnections', 'virtualNetworkGatewayConnectionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.localNetworkGatewaysDelete(localNetworkGatewaysResourceGroupName, localNetworkGatewaysLocalNetworkGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalNetworkGateways', 'localNetworkGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteGatewaysDelete(expressRouteGatewaysResourceGroupName, expressRouteGatewaysExpressRouteGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteGateways', 'expressRouteGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteConnectionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRouteConnectionsDelete(expressRouteConnectionsResourceGroupName, expressRouteConnectionsExpressRouteGatewayName, expressRouteConnectionsConnectionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRouteConnections', 'expressRouteConnectionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ipGroupsDelete(ipGroupsResourceGroupName, ipGroupsIpGroupsName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IpGroups', 'ipGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webApplicationFirewallPoliciesDelete(webApplicationFirewallPoliciesResourceGroupName, webApplicationFirewallPoliciesPolicyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebApplicationFirewallPolicies', 'webApplicationFirewallPoliciesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkSecurityGroupsDelete(networkSecurityGroupsResourceGroupName, networkSecurityGroupsNetworkSecurityGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSecurityGroups', 'networkSecurityGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.securityRulesDelete(securityRulesResourceGroupName, securityRulesNetworkSecurityGroupName, securityRulesSecurityRuleName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRules', 'securityRulesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkProfilesDelete(networkProfilesResourceGroupName, networkProfilesNetworkProfileName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkProfiles', 'networkProfilesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.natGatewaysDelete(natGatewaysResourceGroupName, natGatewaysNatGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NatGateways', 'natGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworksDelete(virtualNetworksResourceGroupName, virtualNetworksVirtualNetworkName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworks', 'virtualNetworksDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.subnetsDelete(subnetsResourceGroupName, subnetsVirtualNetworkName, subnetsSubnetName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'subnetsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkPeeringsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkPeeringsDelete(virtualNetworkPeeringsResourceGroupName, virtualNetworkPeeringsVirtualNetworkName, virtualNetworkPeeringsVirtualNetworkPeeringName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkPeerings', 'virtualNetworkPeeringsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.firewallPoliciesDelete(firewallPoliciesResourceGroupName, firewallPoliciesFirewallPolicyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicies', 'firewallPoliciesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPolicyRuleGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.firewallPolicyRuleGroupsDelete(firewallPolicyRuleGroupsResourceGroupName, firewallPolicyRuleGroupsFirewallPolicyName, firewallPolicyRuleGroupsRuleGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallPolicyRuleGroups', 'firewallPolicyRuleGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loadBalancersDelete(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'loadBalancersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inboundNatRulesDelete(loadBalancersResourceGroupName, loadBalancersLoadBalancerName, loadBalancersInboundNatRuleName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoadBalancers', 'inboundNatRulesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.publicIPPrefixesDelete(publicIPPrefixesResourceGroupName, publicIPPrefixesPublicIpPrefixName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicIPPrefixes', 'publicIPPrefixesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applicationGatewaysDelete(applicationGatewaysResourceGroupName, applicationGatewaysApplicationGatewayName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGateways', 'applicationGatewaysDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.expressRoutePortsDelete(expressRoutePortsResourceGroupName, expressRoutePortsExpressRoutePortName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpressRoutePorts', 'expressRoutePortsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.privateEndpointsDelete(privateEndpointsResourceGroupName, privateEndpointsPrivateEndpointName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateEndpoints', 'privateEndpointsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applicationSecurityGroupsDelete(applicationSecurityGroupsResourceGroupName, applicationSecurityGroupsApplicationSecurityGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationSecurityGroups', 'applicationSecurityGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.serviceEndpointPoliciesDelete(serviceEndpointPoliciesResourceGroupName, serviceEndpointPoliciesServiceEndpointPolicyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicies', 'serviceEndpointPoliciesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPolicyDefinitionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsDelete(serviceEndpointPolicyDefinitionsResourceGroupName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyName, serviceEndpointPolicyDefinitionsServiceEndpointPolicyDefinitionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceEndpointPolicyDefinitions', 'serviceEndpointPolicyDefinitionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualRoutersDelete(virtualRoutersResourceGroupName, virtualRoutersVirtualRouterName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouters', 'virtualRoutersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRouterPeeringsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualRouterPeeringsDelete(virtualRouterPeeringsResourceGroupName, virtualRouterPeeringsVirtualRouterName, virtualRouterPeeringsPeeringName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualRouterPeerings', 'virtualRouterPeeringsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualNetworkTapsDelete(virtualNetworkTapResourceGroupName, virtualNetworkTapTapName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualNetworkTap', 'virtualNetworkTapsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.azureFirewallsDelete(azureFirewallsResourceGroupName, azureFirewallsAzureFirewallName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureFirewalls', 'azureFirewallsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkWatchersDelete(networkWatchersResourceGroupName, networkWatchersNetworkWatcherName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkWatchers', 'networkWatchersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.packetCapturesDelete(packetCapturesResourceGroupName, packetCapturesNetworkWatcherName, packetCapturesPacketCaptureName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCaptures', 'packetCapturesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.connectionMonitorsDelete(connectionMonitorsResourceGroupName, connectionMonitorsNetworkWatcherName, connectionMonitorsConnectionMonitorName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectionMonitors', 'connectionMonitorsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowLogsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.flowLogsDelete(flowLogsResourceGroupName, flowLogsNetworkWatcherName, flowLogsFlowLogName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowLogs', 'flowLogsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.privateLinkServicesDelete(privateLinkServicesResourceGroupName, privateLinkServicesServiceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesDeletePrivateEndpointConnection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.privateLinkServicesDeletePrivateEndpointConnection(privateLinkServicesResourceGroupName, privateLinkServicesServiceName, privateLinkServicesPeConnectionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateLinkServices', 'privateLinkServicesDeletePrivateEndpointConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bastionHostsDelete(bastionHostsResourceGroupName, bastionHostsBastionHostName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BastionHosts', 'bastionHostsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.routeFiltersDelete(routeFiltersResourceGroupName, routeFiltersRouteFilterName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilters', 'routeFiltersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFilterRulesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.routeFilterRulesDelete(routeFilterRulesResourceGroupName, routeFilterRulesRouteFilterName, routeFilterRulesRuleName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteFilterRules', 'routeFilterRulesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vmName = 'fakedata';

    describe('#virtualMachinesPowerOff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesPowerOff(subscriptionsResourceGroupName, vmName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://foo.com/operationstatus', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesRestart - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesRestart(subscriptionsResourceGroupName, vmName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesStart - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesStart(subscriptionsResourceGroupName, vmName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://foo.com/operationstatus', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesRunCommand - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesRunCommand(subscriptionsResourceGroupName, vmName, {}, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('ComponentStatus/StdOut/succeeded', data.response.value[0].code);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesCreateOrUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesCreateOrUpdate(subscriptionsResourceGroupName, vmName, {}, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('/subscriptions/{subscription-id}/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/myVM', data.response.id);
                assert.equal('Microsoft.Compute/virtualMachines', data.response.type);
                assert.equal('myVM', data.response.name);
                assert.equal('westus', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesGet(subscriptionsResourceGroupName, vmName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('/subscriptions/{subscription-id}/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/myVM', data.response.id);
                assert.equal('Microsoft.Compute/virtualMachines', data.response.type);
                assert.equal('myVM', data.response.name);
                assert.equal('West US', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesList(subscriptionsResourceGroupName, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal('Microsoft.Compute/virtualMachines', data.response.value[0].type);
                assert.equal('eastus', data.response.value[0].location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesListAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesListAll(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesDelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualMachinesDelete(subscriptionsResourceGroupName, vmName, false, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://management.azure.com/subscriptions/{subscriptionId}/providers/Microsoft.Compute/locations/westus/operations/{operationId}&monitor=true&api-version=2023-03-01', data.response.Location);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
