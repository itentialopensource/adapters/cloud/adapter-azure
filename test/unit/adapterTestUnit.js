/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-azure',
      type: 'Azure',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Azure = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Azure Adapter Test', () => {
  describe('Azure Class Tests', () => {
    const a = new Azure(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('azure'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('azure'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Azure', pronghornDotJson.export);
          assert.equal('Azure', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-azure', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('azure'));
          assert.equal('Azure', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-azure', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-azure', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#networkInterfacesDelete - errors', () => {
      it('should have a networkInterfacesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGet - errors', () => {
      it('should have a networkInterfacesGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesCreateOrUpdate - errors', () => {
      it('should have a networkInterfacesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkInterfacesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesUpdateTags - errors', () => {
      it('should have a networkInterfacesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkInterfacesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListAll - errors', () => {
      it('should have a networkInterfacesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesList - errors', () => {
      it('should have a networkInterfacesList function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGetEffectiveRouteTable - errors', () => {
      it('should have a networkInterfacesGetEffectiveRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesGetEffectiveRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesGetEffectiveRouteTable(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetEffectiveRouteTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesGetEffectiveRouteTable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetEffectiveRouteTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListEffectiveNetworkSecurityGroups - errors', () => {
      it('should have a networkInterfacesListEffectiveNetworkSecurityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesListEffectiveNetworkSecurityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesListEffectiveNetworkSecurityGroups(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListEffectiveNetworkSecurityGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesListEffectiveNetworkSecurityGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListEffectiveNetworkSecurityGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceIPConfigurationsList - errors', () => {
      it('should have a networkInterfaceIPConfigurationsList function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceIPConfigurationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceIPConfigurationsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceIPConfigurationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceIPConfigurationsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceIPConfigurationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceIPConfigurationsGet - errors', () => {
      it('should have a networkInterfaceIPConfigurationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceIPConfigurationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceIPConfigurationsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceIPConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceIPConfigurationsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceIPConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipConfigurationName', (done) => {
        try {
          a.networkInterfaceIPConfigurationsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceIPConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceLoadBalancersList - errors', () => {
      it('should have a networkInterfaceLoadBalancersList function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceLoadBalancersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceLoadBalancersList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceLoadBalancersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceLoadBalancersList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceLoadBalancersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsDelete - errors', () => {
      it('should have a networkInterfaceTapConfigurationsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceTapConfigurationsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapConfigurationName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tapConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsGet - errors', () => {
      it('should have a networkInterfaceTapConfigurationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceTapConfigurationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapConfigurationName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tapConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsCreateOrUpdate - errors', () => {
      it('should have a networkInterfaceTapConfigurationsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceTapConfigurationsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapConfigurationName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tapConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapConfigurationParameters', (done) => {
        try {
          a.networkInterfaceTapConfigurationsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tapConfigurationParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfaceTapConfigurationsList - errors', () => {
      it('should have a networkInterfaceTapConfigurationsList function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfaceTapConfigurationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfaceTapConfigurationsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfaceTapConfigurationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces - errors', () => {
      it('should have a networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineIndex', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualmachineIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListVirtualMachineScaleSetNetworkInterfaces - errors', () => {
      it('should have a networkInterfacesListVirtualMachineScaleSetNetworkInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesListVirtualMachineScaleSetNetworkInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetNetworkInterfaces(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetNetworkInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetNetworkInterfaces('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetNetworkInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGetVirtualMachineScaleSetNetworkInterface - errors', () => {
      it('should have a networkInterfacesGetVirtualMachineScaleSetNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesGetVirtualMachineScaleSetNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetNetworkInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetNetworkInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineIndex', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetNetworkInterface('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualmachineIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetNetworkInterface('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesListVirtualMachineScaleSetIpConfigurations - errors', () => {
      it('should have a networkInterfacesListVirtualMachineScaleSetIpConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesListVirtualMachineScaleSetIpConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetIpConfigurations(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetIpConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetIpConfigurations('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetIpConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineIndex', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetIpConfigurations('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualmachineIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetIpConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesListVirtualMachineScaleSetIpConfigurations('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesListVirtualMachineScaleSetIpConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkInterfacesGetVirtualMachineScaleSetIpConfiguration - errors', () => {
      it('should have a networkInterfacesGetVirtualMachineScaleSetIpConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetIpConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetIpConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineIndex', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualmachineIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetIpConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetIpConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipConfigurationName', (done) => {
        try {
          a.networkInterfacesGetVirtualMachineScaleSetIpConfiguration('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkInterfacesGetVirtualMachineScaleSetIpConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableServiceAliasesList - errors', () => {
      it('should have a availableServiceAliasesList function', (done) => {
        try {
          assert.equal(true, typeof a.availableServiceAliasesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availableServiceAliasesList(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableServiceAliasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableServiceAliasesListByResourceGroup - errors', () => {
      it('should have a availableServiceAliasesListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.availableServiceAliasesListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.availableServiceAliasesListByResourceGroup(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableServiceAliasesListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availableServiceAliasesListByResourceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableServiceAliasesListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDnsNameAvailability - errors', () => {
      it('should have a checkDnsNameAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.checkDnsNameAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.checkDnsNameAvailability(null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-checkDnsNameAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainNameLabel', (done) => {
        try {
          a.checkDnsNameAvailability('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainNameLabel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-checkDnsNameAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceTagsList - errors', () => {
      it('should have a serviceTagsList function', (done) => {
        try {
          assert.equal(true, typeof a.serviceTagsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.serviceTagsList(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceTagsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableDelegationsList - errors', () => {
      it('should have a availableDelegationsList function', (done) => {
        try {
          assert.equal(true, typeof a.availableDelegationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availableDelegationsList(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableDelegationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableResourceGroupDelegationsList - errors', () => {
      it('should have a availableResourceGroupDelegationsList function', (done) => {
        try {
          assert.equal(true, typeof a.availableResourceGroupDelegationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availableResourceGroupDelegationsList(null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableResourceGroupDelegationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.availableResourceGroupDelegationsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableResourceGroupDelegationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableEndpointServicesList - errors', () => {
      it('should have a availableEndpointServicesList function', (done) => {
        try {
          assert.equal(true, typeof a.availableEndpointServicesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availableEndpointServicesList(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availableEndpointServicesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsPrepareNetworkPolicies - errors', () => {
      it('should have a subnetsPrepareNetworkPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.subnetsPrepareNetworkPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.subnetsPrepareNetworkPolicies(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsPrepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.subnetsPrepareNetworkPolicies('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsPrepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.subnetsPrepareNetworkPolicies('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsPrepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prepareNetworkPoliciesRequestParameters', (done) => {
        try {
          a.subnetsPrepareNetworkPolicies('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'prepareNetworkPoliciesRequestParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsPrepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsUnprepareNetworkPolicies - errors', () => {
      it('should have a subnetsUnprepareNetworkPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.subnetsUnprepareNetworkPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.subnetsUnprepareNetworkPolicies(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsUnprepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.subnetsUnprepareNetworkPolicies('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsUnprepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.subnetsUnprepareNetworkPolicies('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsUnprepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing unprepareNetworkPoliciesRequestParameters', (done) => {
        try {
          a.subnetsUnprepareNetworkPolicies('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'unprepareNetworkPoliciesRequestParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsUnprepareNetworkPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resourceNavigationLinksList - errors', () => {
      it('should have a resourceNavigationLinksList function', (done) => {
        try {
          assert.equal(true, typeof a.resourceNavigationLinksList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.resourceNavigationLinksList(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-resourceNavigationLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.resourceNavigationLinksList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-resourceNavigationLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.resourceNavigationLinksList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-resourceNavigationLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceAssociationLinksList - errors', () => {
      it('should have a serviceAssociationLinksList function', (done) => {
        try {
          assert.equal(true, typeof a.serviceAssociationLinksList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceAssociationLinksList(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceAssociationLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.serviceAssociationLinksList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceAssociationLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.serviceAssociationLinksList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceAssociationLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksCheckIPAddressAvailability - errors', () => {
      it('should have a virtualNetworksCheckIPAddressAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksCheckIPAddressAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksCheckIPAddressAvailability(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksCheckIPAddressAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworksCheckIPAddressAvailability('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksCheckIPAddressAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.virtualNetworksCheckIPAddressAvailability('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksCheckIPAddressAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksListUsage - errors', () => {
      it('should have a virtualNetworksListUsage function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksListUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksListUsage(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksListUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworksListUsage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksListUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansGet - errors', () => {
      it('should have a virtualWansGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualWansGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualWansGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.virtualWansGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansCreateOrUpdate - errors', () => {
      it('should have a virtualWansCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualWansCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualWansCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.virtualWansCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wANParameters', (done) => {
        try {
          a.virtualWansCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'wANParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansDelete - errors', () => {
      it('should have a virtualWansDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualWansDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualWansDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.virtualWansDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansListByResourceGroup - errors', () => {
      it('should have a virtualWansListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.virtualWansListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualWansListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansList - errors', () => {
      it('should have a virtualWansList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualWansList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesGet - errors', () => {
      it('should have a vpnSitesGet function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSitesGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteName', (done) => {
        try {
          a.vpnSitesGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnSiteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesCreateOrUpdate - errors', () => {
      it('should have a vpnSitesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSitesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteName', (done) => {
        try {
          a.vpnSitesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnSiteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteParameters', (done) => {
        try {
          a.vpnSitesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnSiteParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesDelete - errors', () => {
      it('should have a vpnSitesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSitesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteName', (done) => {
        try {
          a.vpnSitesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnSiteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesListByResourceGroup - errors', () => {
      it('should have a vpnSitesListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSitesListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSiteLinksGet - errors', () => {
      it('should have a vpnSiteLinksGet function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSiteLinksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSiteLinksGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteName', (done) => {
        try {
          a.vpnSiteLinksGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnSiteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteLinkName', (done) => {
        try {
          a.vpnSiteLinksGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnSiteLinkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSiteLinksListByVpnSite - errors', () => {
      it('should have a vpnSiteLinksListByVpnSite function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSiteLinksListByVpnSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSiteLinksListByVpnSite(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinksListByVpnSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteName', (done) => {
        try {
          a.vpnSiteLinksListByVpnSite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnSiteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinksListByVpnSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesList - errors', () => {
      it('should have a vpnSitesList function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesConfigurationDownload - errors', () => {
      it('should have a vpnSitesConfigurationDownload function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesConfigurationDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSitesConfigurationDownload(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesConfigurationDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.vpnSitesConfigurationDownload('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesConfigurationDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.vpnSitesConfigurationDownload('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesConfigurationDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#supportedSecurityProviders - errors', () => {
      it('should have a supportedSecurityProviders function', (done) => {
        try {
          assert.equal(true, typeof a.supportedSecurityProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.supportedSecurityProviders(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-supportedSecurityProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.supportedSecurityProviders('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-supportedSecurityProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsGet - errors', () => {
      it('should have a vpnServerConfigurationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnServerConfigurationsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnServerConfigurationName', (done) => {
        try {
          a.vpnServerConfigurationsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnServerConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsCreateOrUpdate - errors', () => {
      it('should have a vpnServerConfigurationsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnServerConfigurationsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnServerConfigurationName', (done) => {
        try {
          a.vpnServerConfigurationsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnServerConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnServerConfigurationParameters', (done) => {
        try {
          a.vpnServerConfigurationsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnServerConfigurationParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsDelete - errors', () => {
      it('should have a vpnServerConfigurationsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnServerConfigurationsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnServerConfigurationName', (done) => {
        try {
          a.vpnServerConfigurationsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnServerConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsListByResourceGroup - errors', () => {
      it('should have a vpnServerConfigurationsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnServerConfigurationsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsList - errors', () => {
      it('should have a vpnServerConfigurationsList function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsGet - errors', () => {
      it('should have a virtualHubsGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsCreateOrUpdate - errors', () => {
      it('should have a virtualHubsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubParameters', (done) => {
        try {
          a.virtualHubsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsDelete - errors', () => {
      it('should have a virtualHubsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsListByResourceGroup - errors', () => {
      it('should have a virtualHubsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsList - errors', () => {
      it('should have a virtualHubsList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hubVirtualNetworkConnectionsGet - errors', () => {
      it('should have a hubVirtualNetworkConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.hubVirtualNetworkConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.hubVirtualNetworkConnectionsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-hubVirtualNetworkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.hubVirtualNetworkConnectionsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-hubVirtualNetworkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.hubVirtualNetworkConnectionsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-hubVirtualNetworkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hubVirtualNetworkConnectionsList - errors', () => {
      it('should have a hubVirtualNetworkConnectionsList function', (done) => {
        try {
          assert.equal(true, typeof a.hubVirtualNetworkConnectionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.hubVirtualNetworkConnectionsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-hubVirtualNetworkConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.hubVirtualNetworkConnectionsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-hubVirtualNetworkConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysGet - errors', () => {
      it('should have a vpnGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnGatewaysGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnGatewaysGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysCreateOrUpdate - errors', () => {
      it('should have a vpnGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnGatewayParameters', (done) => {
        try {
          a.vpnGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnGatewayParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysDelete - errors', () => {
      it('should have a vpnGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysListByResourceGroup - errors', () => {
      it('should have a vpnGatewaysListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnGatewaysListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysList - errors', () => {
      it('should have a vpnGatewaysList function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsGet - errors', () => {
      it('should have a vpnConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.vpnConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnConnectionsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnConnectionsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.vpnConnectionsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsCreateOrUpdate - errors', () => {
      it('should have a vpnConnectionsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.vpnConnectionsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnConnectionsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnConnectionsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.vpnConnectionsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnConnectionParameters', (done) => {
        try {
          a.vpnConnectionsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnConnectionParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsDelete - errors', () => {
      it('should have a vpnConnectionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.vpnConnectionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnConnectionsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnConnectionsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.vpnConnectionsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSiteLinkConnectionsGet - errors', () => {
      it('should have a vpnSiteLinkConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSiteLinkConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSiteLinkConnectionsGet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnSiteLinkConnectionsGet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.vpnSiteLinkConnectionsGet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkConnectionName', (done) => {
        try {
          a.vpnSiteLinkConnectionsGet('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSiteLinkConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnConnectionsListByVpnGateway - errors', () => {
      it('should have a vpnConnectionsListByVpnGateway function', (done) => {
        try {
          assert.equal(true, typeof a.vpnConnectionsListByVpnGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnConnectionsListByVpnGateway(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsListByVpnGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnConnectionsListByVpnGateway('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnConnectionsListByVpnGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnLinkConnectionsListByVpnConnection - errors', () => {
      it('should have a vpnLinkConnectionsListByVpnConnection function', (done) => {
        try {
          assert.equal(true, typeof a.vpnLinkConnectionsListByVpnConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnLinkConnectionsListByVpnConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnLinkConnectionsListByVpnConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnLinkConnectionsListByVpnConnection('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnLinkConnectionsListByVpnConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.vpnLinkConnectionsListByVpnConnection('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnLinkConnectionsListByVpnConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysGet - errors', () => {
      it('should have a p2sVpnGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysCreateOrUpdate - errors', () => {
      it('should have a p2sVpnGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2SVpnGatewayParameters', (done) => {
        try {
          a.p2sVpnGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'p2SVpnGatewayParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysDelete - errors', () => {
      it('should have a p2sVpnGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysListByResourceGroup - errors', () => {
      it('should have a p2sVpnGatewaysListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysList - errors', () => {
      it('should have a p2sVpnGatewaysList function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsAssociatedWithVirtualWanList - errors', () => {
      it('should have a vpnServerConfigurationsAssociatedWithVirtualWanList function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsAssociatedWithVirtualWanList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnServerConfigurationsAssociatedWithVirtualWanList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsAssociatedWithVirtualWanList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.vpnServerConfigurationsAssociatedWithVirtualWanList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsAssociatedWithVirtualWanList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generatevirtualwanvpnserverconfigurationvpnprofile - errors', () => {
      it('should have a generatevirtualwanvpnserverconfigurationvpnprofile function', (done) => {
        try {
          assert.equal(true, typeof a.generatevirtualwanvpnserverconfigurationvpnprofile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.generatevirtualwanvpnserverconfigurationvpnprofile(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-generatevirtualwanvpnserverconfigurationvpnprofile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.generatevirtualwanvpnserverconfigurationvpnprofile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-generatevirtualwanvpnserverconfigurationvpnprofile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnClientParams', (done) => {
        try {
          a.generatevirtualwanvpnserverconfigurationvpnprofile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnClientParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-generatevirtualwanvpnserverconfigurationvpnprofile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sGet - errors', () => {
      it('should have a virtualHubRouteTableV2sGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubRouteTableV2sGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubRouteTableV2sGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubRouteTableV2sGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.virtualHubRouteTableV2sGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sCreateOrUpdate - errors', () => {
      it('should have a virtualHubRouteTableV2sCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubRouteTableV2sCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubRouteTableV2sCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubRouteTableV2sCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.virtualHubRouteTableV2sCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubRouteTableV2Parameters', (done) => {
        try {
          a.virtualHubRouteTableV2sCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubRouteTableV2Parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sDelete - errors', () => {
      it('should have a virtualHubRouteTableV2sDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubRouteTableV2sDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubRouteTableV2sDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubRouteTableV2sDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.virtualHubRouteTableV2sDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubRouteTableV2sList - errors', () => {
      it('should have a virtualHubRouteTableV2sList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubRouteTableV2sList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubRouteTableV2sList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubRouteTableV2sList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubRouteTableV2sList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses - errors', () => {
      it('should have a publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses - errors', () => {
      it('should have a publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineIndex', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualmachineIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipConfigurationName', (done) => {
        try {
          a.publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress - errors', () => {
      it('should have a publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualMachineScaleSetName', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualMachineScaleSetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineIndex', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'virtualmachineIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceName', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipConfigurationName', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'ipConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpAddressName', (done) => {
        try {
          a.publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpAddressName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availablePrivateEndpointTypesList - errors', () => {
      it('should have a availablePrivateEndpointTypesList function', (done) => {
        try {
          assert.equal(true, typeof a.availablePrivateEndpointTypesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availablePrivateEndpointTypesList(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availablePrivateEndpointTypesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availablePrivateEndpointTypesListByResourceGroup - errors', () => {
      it('should have a availablePrivateEndpointTypesListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.availablePrivateEndpointTypesListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.availablePrivateEndpointTypesListByResourceGroup(null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availablePrivateEndpointTypesListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.availablePrivateEndpointTypesListByResourceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-availablePrivateEndpointTypesListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualWansUpdateTags - errors', () => {
      it('should have a virtualWansUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.virtualWansUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualWansUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualWANName', (done) => {
        try {
          a.virtualWansUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualWANName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wANParameters', (done) => {
        try {
          a.virtualWansUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'wANParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualWansUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualHubsUpdateTags - errors', () => {
      it('should have a virtualHubsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.virtualHubsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualHubsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubName', (done) => {
        try {
          a.virtualHubsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualHubName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualHubParameters', (done) => {
        try {
          a.virtualHubsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualHubParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualHubsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnSitesUpdateTags - errors', () => {
      it('should have a vpnSitesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.vpnSitesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnSitesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteName', (done) => {
        try {
          a.vpnSitesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnSiteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnSiteParameters', (done) => {
        try {
          a.vpnSitesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnSiteParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnSitesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnServerConfigurationsUpdateTags - errors', () => {
      it('should have a vpnServerConfigurationsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.vpnServerConfigurationsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnServerConfigurationsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnServerConfigurationName', (done) => {
        try {
          a.vpnServerConfigurationsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnServerConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnServerConfigurationParameters', (done) => {
        try {
          a.vpnServerConfigurationsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnServerConfigurationParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnServerConfigurationsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysUpdateTags - errors', () => {
      it('should have a vpnGatewaysUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnGatewaysUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnGatewaysUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnGatewayParameters', (done) => {
        try {
          a.vpnGatewaysUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnGatewayParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vpnGatewaysReset - errors', () => {
      it('should have a vpnGatewaysReset function', (done) => {
        try {
          assert.equal(true, typeof a.vpnGatewaysReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.vpnGatewaysReset(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.vpnGatewaysReset('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-vpnGatewaysReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysUpdateTags - errors', () => {
      it('should have a p2sVpnGatewaysUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2SVpnGatewayParameters', (done) => {
        try {
          a.p2sVpnGatewaysUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'p2SVpnGatewayParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysGenerateVpnProfile - errors', () => {
      it('should have a p2sVpnGatewaysGenerateVpnProfile function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysGenerateVpnProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysGenerateVpnProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGenerateVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysGenerateVpnProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGenerateVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.p2sVpnGatewaysGenerateVpnProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGenerateVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysGetP2sVpnConnectionHealth - errors', () => {
      it('should have a p2sVpnGatewaysGetP2sVpnConnectionHealth function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysGetP2sVpnConnectionHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealth(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGetP2sVpnConnectionHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGetP2sVpnConnectionHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed - errors', () => {
      it('should have a p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayName', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#p2sVpnGatewaysDisconnectP2sVpnConnections - errors', () => {
      it('should have a p2sVpnGatewaysDisconnectP2sVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.p2sVpnGatewaysDisconnectP2sVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.p2sVpnGatewaysDisconnectP2sVpnConnections(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysDisconnectP2sVpnConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2sVpnGatewayName', (done) => {
        try {
          a.p2sVpnGatewaysDisconnectP2sVpnConnections('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2sVpnGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysDisconnectP2sVpnConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.p2sVpnGatewaysDisconnectP2sVpnConnections('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-p2sVpnGatewaysDisconnectP2sVpnConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usagesList - errors', () => {
      it('should have a usagesList function', (done) => {
        try {
          assert.equal(true, typeof a.usagesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.usagesList(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-usagesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansDelete - errors', () => {
      it('should have a ddosProtectionPlansDelete function', (done) => {
        try {
          assert.equal(true, typeof a.ddosProtectionPlansDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosProtectionPlansDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosProtectionPlanName', (done) => {
        try {
          a.ddosProtectionPlansDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ddosProtectionPlanName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansGet - errors', () => {
      it('should have a ddosProtectionPlansGet function', (done) => {
        try {
          assert.equal(true, typeof a.ddosProtectionPlansGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosProtectionPlansGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosProtectionPlanName', (done) => {
        try {
          a.ddosProtectionPlansGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ddosProtectionPlanName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansCreateOrUpdate - errors', () => {
      it('should have a ddosProtectionPlansCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.ddosProtectionPlansCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosProtectionPlansCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosProtectionPlanName', (done) => {
        try {
          a.ddosProtectionPlansCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ddosProtectionPlanName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.ddosProtectionPlansCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansUpdateTags - errors', () => {
      it('should have a ddosProtectionPlansUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.ddosProtectionPlansUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosProtectionPlansUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosProtectionPlanName', (done) => {
        try {
          a.ddosProtectionPlansUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ddosProtectionPlanName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.ddosProtectionPlansUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansList - errors', () => {
      it('should have a ddosProtectionPlansList function', (done) => {
        try {
          assert.equal(true, typeof a.ddosProtectionPlansList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosProtectionPlansListByResourceGroup - errors', () => {
      it('should have a ddosProtectionPlansListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.ddosProtectionPlansListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosProtectionPlansListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosProtectionPlansListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitAuthorizationsDelete - errors', () => {
      it('should have a expressRouteCircuitAuthorizationsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitAuthorizationsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authorizationName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'authorizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitAuthorizationsGet - errors', () => {
      it('should have a expressRouteCircuitAuthorizationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitAuthorizationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authorizationName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'authorizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitAuthorizationsCreateOrUpdate - errors', () => {
      it('should have a expressRouteCircuitAuthorizationsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitAuthorizationsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authorizationName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'authorizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authorizationParameters', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'authorizationParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitAuthorizationsList - errors', () => {
      it('should have a expressRouteCircuitAuthorizationsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitAuthorizationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitAuthorizationsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitAuthorizationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitPeeringsDelete - errors', () => {
      it('should have a expressRouteCircuitPeeringsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitPeeringsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitPeeringsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitPeeringsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitPeeringsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitPeeringsGet - errors', () => {
      it('should have a expressRouteCircuitPeeringsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitPeeringsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitPeeringsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitPeeringsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitPeeringsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitPeeringsCreateOrUpdate - errors', () => {
      it('should have a expressRouteCircuitPeeringsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitPeeringsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitPeeringsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitPeeringsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitPeeringsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringParameters', (done) => {
        try {
          a.expressRouteCircuitPeeringsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitPeeringsList - errors', () => {
      it('should have a expressRouteCircuitPeeringsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitPeeringsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitPeeringsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitPeeringsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitConnectionsDelete - errors', () => {
      it('should have a expressRouteCircuitConnectionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitConnectionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitConnectionsDelete(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitConnectionsDelete('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitConnectionsDelete('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.expressRouteCircuitConnectionsDelete('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitConnectionsGet - errors', () => {
      it('should have a expressRouteCircuitConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitConnectionsGet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitConnectionsGet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitConnectionsGet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.expressRouteCircuitConnectionsGet('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitConnectionsCreateOrUpdate - errors', () => {
      it('should have a expressRouteCircuitConnectionsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitConnectionsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitConnectionsCreateOrUpdate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitConnectionsCreateOrUpdate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitConnectionsCreateOrUpdate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.expressRouteCircuitConnectionsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteCircuitConnectionParameters', (done) => {
        try {
          a.expressRouteCircuitConnectionsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRouteCircuitConnectionParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitConnectionsList - errors', () => {
      it('should have a expressRouteCircuitConnectionsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitConnectionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitConnectionsList(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitConnectionsList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitConnectionsList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#peerExpressRouteCircuitConnectionsGet - errors', () => {
      it('should have a peerExpressRouteCircuitConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.peerExpressRouteCircuitConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsGet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsGet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsGet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsGet('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#peerExpressRouteCircuitConnectionsList - errors', () => {
      it('should have a peerExpressRouteCircuitConnectionsList function', (done) => {
        try {
          assert.equal(true, typeof a.peerExpressRouteCircuitConnectionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsList(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.peerExpressRouteCircuitConnectionsList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-peerExpressRouteCircuitConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsDelete - errors', () => {
      it('should have a expressRouteCircuitsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsGet - errors', () => {
      it('should have a expressRouteCircuitsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsCreateOrUpdate - errors', () => {
      it('should have a expressRouteCircuitsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.expressRouteCircuitsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsUpdateTags - errors', () => {
      it('should have a expressRouteCircuitsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.expressRouteCircuitsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsList - errors', () => {
      it('should have a expressRouteCircuitsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsListAll - errors', () => {
      it('should have a expressRouteCircuitsListAll function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsListArpTable - errors', () => {
      it('should have a expressRouteCircuitsListArpTable function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsListArpTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsListArpTable(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsListArpTable('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitsListArpTable('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicePath', (done) => {
        try {
          a.expressRouteCircuitsListArpTable('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsListRoutesTable - errors', () => {
      it('should have a expressRouteCircuitsListRoutesTable function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsListRoutesTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTable(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTable('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTable('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicePath', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTable('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsListRoutesTableSummary - errors', () => {
      it('should have a expressRouteCircuitsListRoutesTableSummary function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsListRoutesTableSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTableSummary(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTableSummary('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTableSummary('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicePath', (done) => {
        try {
          a.expressRouteCircuitsListRoutesTableSummary('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsGetStats - errors', () => {
      it('should have a expressRouteCircuitsGetStats function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsGetStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsGetStats(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGetStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsGetStats('fakeparam', null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGetStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCircuitsGetPeeringStats - errors', () => {
      it('should have a expressRouteCircuitsGetPeeringStats function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCircuitsGetPeeringStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCircuitsGetPeeringStats(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGetPeeringStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitName', (done) => {
        try {
          a.expressRouteCircuitsGetPeeringStats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'circuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGetPeeringStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCircuitsGetPeeringStats('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCircuitsGetPeeringStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteServiceProvidersList - errors', () => {
      it('should have a expressRouteServiceProvidersList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteServiceProvidersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesDelete - errors', () => {
      it('should have a routeTablesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.routeTablesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeTablesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routeTablesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesGet - errors', () => {
      it('should have a routeTablesGet function', (done) => {
        try {
          assert.equal(true, typeof a.routeTablesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeTablesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routeTablesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesCreateOrUpdate - errors', () => {
      it('should have a routeTablesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.routeTablesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeTablesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routeTablesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.routeTablesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesUpdateTags - errors', () => {
      it('should have a routeTablesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.routeTablesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeTablesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routeTablesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.routeTablesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesList - errors', () => {
      it('should have a routeTablesList function', (done) => {
        try {
          assert.equal(true, typeof a.routeTablesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeTablesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeTablesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeTablesListAll - errors', () => {
      it('should have a routeTablesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.routeTablesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routesDelete - errors', () => {
      it('should have a routesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.routesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeName', (done) => {
        try {
          a.routesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routesGet - errors', () => {
      it('should have a routesGet function', (done) => {
        try {
          assert.equal(true, typeof a.routesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeName', (done) => {
        try {
          a.routesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routesCreateOrUpdate - errors', () => {
      it('should have a routesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.routesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routesCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routesCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeName', (done) => {
        try {
          a.routesCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeParameters', (done) => {
        try {
          a.routesCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routesList - errors', () => {
      it('should have a routesList function', (done) => {
        try {
          assert.equal(true, typeof a.routesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableName', (done) => {
        try {
          a.routesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosCustomPoliciesDelete - errors', () => {
      it('should have a ddosCustomPoliciesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.ddosCustomPoliciesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosCustomPoliciesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosCustomPolicyName', (done) => {
        try {
          a.ddosCustomPoliciesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ddosCustomPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosCustomPoliciesGet - errors', () => {
      it('should have a ddosCustomPoliciesGet function', (done) => {
        try {
          assert.equal(true, typeof a.ddosCustomPoliciesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosCustomPoliciesGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosCustomPolicyName', (done) => {
        try {
          a.ddosCustomPoliciesGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ddosCustomPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosCustomPoliciesCreateOrUpdate - errors', () => {
      it('should have a ddosCustomPoliciesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.ddosCustomPoliciesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosCustomPoliciesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosCustomPolicyName', (done) => {
        try {
          a.ddosCustomPoliciesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ddosCustomPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.ddosCustomPoliciesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ddosCustomPoliciesUpdateTags - errors', () => {
      it('should have a ddosCustomPoliciesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.ddosCustomPoliciesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ddosCustomPoliciesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ddosCustomPolicyName', (done) => {
        try {
          a.ddosCustomPoliciesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ddosCustomPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.ddosCustomPoliciesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ddosCustomPoliciesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsList - errors', () => {
      it('should have a expressRouteCrossConnectionsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsListByResourceGroup - errors', () => {
      it('should have a expressRouteCrossConnectionsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsGet - errors', () => {
      it('should have a expressRouteCrossConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsCreateOrUpdate - errors', () => {
      it('should have a expressRouteCrossConnectionsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.expressRouteCrossConnectionsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsUpdateTags - errors', () => {
      it('should have a expressRouteCrossConnectionsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionParameters', (done) => {
        try {
          a.expressRouteCrossConnectionsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'crossConnectionParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionPeeringsList - errors', () => {
      it('should have a expressRouteCrossConnectionPeeringsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionPeeringsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionPeeringsDelete - errors', () => {
      it('should have a expressRouteCrossConnectionPeeringsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionPeeringsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionPeeringsGet - errors', () => {
      it('should have a expressRouteCrossConnectionPeeringsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionPeeringsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionPeeringsCreateOrUpdate - errors', () => {
      it('should have a expressRouteCrossConnectionPeeringsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionPeeringsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringParameters', (done) => {
        try {
          a.expressRouteCrossConnectionPeeringsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsListArpTable - errors', () => {
      it('should have a expressRouteCrossConnectionsListArpTable function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsListArpTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsListArpTable(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionsListArpTable('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCrossConnectionsListArpTable('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicePath', (done) => {
        try {
          a.expressRouteCrossConnectionsListArpTable('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListArpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsListRoutesTableSummary - errors', () => {
      it('should have a expressRouteCrossConnectionsListRoutesTableSummary function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsListRoutesTableSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTableSummary(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTableSummary('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTableSummary('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicePath', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTableSummary('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTableSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteCrossConnectionsListRoutesTable - errors', () => {
      it('should have a expressRouteCrossConnectionsListRoutesTable function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteCrossConnectionsListRoutesTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTable(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crossConnectionName', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTable('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'crossConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTable('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicePath', (done) => {
        try {
          a.expressRouteCrossConnectionsListRoutesTable('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteCrossConnectionsListRoutesTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesDelete - errors', () => {
      it('should have a publicIPAddressesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpAddressName', (done) => {
        try {
          a.publicIPAddressesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'publicIpAddressName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesGet - errors', () => {
      it('should have a publicIPAddressesGet function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpAddressName', (done) => {
        try {
          a.publicIPAddressesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpAddressName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesCreateOrUpdate - errors', () => {
      it('should have a publicIPAddressesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpAddressName', (done) => {
        try {
          a.publicIPAddressesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpAddressName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.publicIPAddressesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesUpdateTags - errors', () => {
      it('should have a publicIPAddressesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpAddressName', (done) => {
        try {
          a.publicIPAddressesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpAddressName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.publicIPAddressesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesListAll - errors', () => {
      it('should have a publicIPAddressesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPAddressesList - errors', () => {
      it('should have a publicIPAddressesList function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPAddressesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPAddressesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPAddressesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysCreateOrUpdate - errors', () => {
      it('should have a virtualNetworkGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGet - errors', () => {
      it('should have a virtualNetworkGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysDelete - errors', () => {
      it('should have a virtualNetworkGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysUpdateTags - errors', () => {
      it('should have a virtualNetworkGatewaysUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewaysUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysList - errors', () => {
      it('should have a virtualNetworkGatewaysList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysListConnections - errors', () => {
      it('should have a virtualNetworkGatewaysListConnections function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysListConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysListConnections(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysListConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysListConnections('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysListConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysReset - errors', () => {
      it('should have a virtualNetworkGatewaysReset function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysReset(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysReset('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysResetVpnClientSharedKey - errors', () => {
      it('should have a virtualNetworkGatewaysResetVpnClientSharedKey function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysResetVpnClientSharedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysResetVpnClientSharedKey(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysResetVpnClientSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysResetVpnClientSharedKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysResetVpnClientSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGeneratevpnclientpackage - errors', () => {
      it('should have a virtualNetworkGatewaysGeneratevpnclientpackage function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGeneratevpnclientpackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGeneratevpnclientpackage(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGeneratevpnclientpackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGeneratevpnclientpackage('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGeneratevpnclientpackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewaysGeneratevpnclientpackage('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGeneratevpnclientpackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGenerateVpnProfile - errors', () => {
      it('should have a virtualNetworkGatewaysGenerateVpnProfile function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGenerateVpnProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGenerateVpnProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGenerateVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGenerateVpnProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGenerateVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewaysGenerateVpnProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGenerateVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetVpnProfilePackageUrl - errors', () => {
      it('should have a virtualNetworkGatewaysGetVpnProfilePackageUrl function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGetVpnProfilePackageUrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnProfilePackageUrl(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetVpnProfilePackageUrl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnProfilePackageUrl('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetVpnProfilePackageUrl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetBgpPeerStatus - errors', () => {
      it('should have a virtualNetworkGatewaysGetBgpPeerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGetBgpPeerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGetBgpPeerStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetBgpPeerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGetBgpPeerStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetBgpPeerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysSupportedVpnDevices - errors', () => {
      it('should have a virtualNetworkGatewaysSupportedVpnDevices function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysSupportedVpnDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysSupportedVpnDevices(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysSupportedVpnDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysSupportedVpnDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysSupportedVpnDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetLearnedRoutes - errors', () => {
      it('should have a virtualNetworkGatewaysGetLearnedRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGetLearnedRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGetLearnedRoutes(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetLearnedRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGetLearnedRoutes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetLearnedRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetAdvertisedRoutes - errors', () => {
      it('should have a virtualNetworkGatewaysGetAdvertisedRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGetAdvertisedRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGetAdvertisedRoutes(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetAdvertisedRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGetAdvertisedRoutes('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetAdvertisedRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peer', (done) => {
        try {
          a.virtualNetworkGatewaysGetAdvertisedRoutes('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetAdvertisedRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysSetVpnclientIpsecParameters - errors', () => {
      it('should have a virtualNetworkGatewaysSetVpnclientIpsecParameters function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysSetVpnclientIpsecParameters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysSetVpnclientIpsecParameters(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysSetVpnclientIpsecParameters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysSetVpnclientIpsecParameters('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysSetVpnclientIpsecParameters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnclientIpsecParams', (done) => {
        try {
          a.virtualNetworkGatewaysSetVpnclientIpsecParameters('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnclientIpsecParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysSetVpnclientIpsecParameters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetVpnclientIpsecParameters - errors', () => {
      it('should have a virtualNetworkGatewaysGetVpnclientIpsecParameters function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGetVpnclientIpsecParameters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnclientIpsecParameters(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetVpnclientIpsecParameters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnclientIpsecParameters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetVpnclientIpsecParameters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysVpnDeviceConfigurationScript - errors', () => {
      it('should have a virtualNetworkGatewaysVpnDeviceConfigurationScript function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysVpnDeviceConfigurationScript === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysVpnDeviceConfigurationScript(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysVpnDeviceConfigurationScript', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewaysVpnDeviceConfigurationScript('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysVpnDeviceConfigurationScript', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewaysVpnDeviceConfigurationScript('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysVpnDeviceConfigurationScript', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysStartPacketCapture - errors', () => {
      it('should have a virtualNetworkGatewaysStartPacketCapture function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysStartPacketCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysStartPacketCapture(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysStartPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysStartPacketCapture('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysStartPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysStopPacketCapture - errors', () => {
      it('should have a virtualNetworkGatewaysStopPacketCapture function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysStopPacketCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysStopPacketCapture(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysStopPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysStopPacketCapture('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysStopPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewaysStopPacketCapture('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysStopPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysGetVpnclientConnectionHealth - errors', () => {
      it('should have a virtualNetworkGatewaysGetVpnclientConnectionHealth function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysGetVpnclientConnectionHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnclientConnectionHealth(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetVpnclientConnectionHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysGetVpnclientConnectionHealth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysGetVpnclientConnectionHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections - errors', () => {
      it('should have a virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayName', (done) => {
        try {
          a.virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsCreateOrUpdate - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsGet - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsDelete - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsUpdateTags - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsSetSharedKey - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsSetSharedKey function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsSetSharedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsSetSharedKey(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsSetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsSetSharedKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsSetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsSetSharedKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsSetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsGetSharedKey - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsGetSharedKey function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsGetSharedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsGetSharedKey(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsGetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsGetSharedKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsGetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsList - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsResetSharedKey - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsResetSharedKey function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsResetSharedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsResetSharedKey(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsResetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsResetSharedKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsResetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsResetSharedKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsResetSharedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsStartPacketCapture - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsStartPacketCapture function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsStartPacketCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStartPacketCapture(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsStartPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStartPacketCapture('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsStartPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkGatewayConnectionsStopPacketCapture - errors', () => {
      it('should have a virtualNetworkGatewayConnectionsStopPacketCapture function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkGatewayConnectionsStopPacketCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStopPacketCapture(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsStopPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkGatewayConnectionName', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStopPacketCapture('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkGatewayConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsStopPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkGatewayConnectionsStopPacketCapture('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkGatewayConnectionsStopPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysCreateOrUpdate - errors', () => {
      it('should have a localNetworkGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.localNetworkGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.localNetworkGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localNetworkGatewayName', (done) => {
        try {
          a.localNetworkGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'localNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.localNetworkGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysGet - errors', () => {
      it('should have a localNetworkGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.localNetworkGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.localNetworkGatewaysGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localNetworkGatewayName', (done) => {
        try {
          a.localNetworkGatewaysGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'localNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysDelete - errors', () => {
      it('should have a localNetworkGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.localNetworkGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.localNetworkGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localNetworkGatewayName', (done) => {
        try {
          a.localNetworkGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'localNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysUpdateTags - errors', () => {
      it('should have a localNetworkGatewaysUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.localNetworkGatewaysUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.localNetworkGatewaysUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localNetworkGatewayName', (done) => {
        try {
          a.localNetworkGatewaysUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'localNetworkGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.localNetworkGatewaysUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#localNetworkGatewaysList - errors', () => {
      it('should have a localNetworkGatewaysList function', (done) => {
        try {
          assert.equal(true, typeof a.localNetworkGatewaysList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.localNetworkGatewaysList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-localNetworkGatewaysList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysListBySubscription - errors', () => {
      it('should have a expressRouteGatewaysListBySubscription function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteGatewaysListBySubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysListByResourceGroup - errors', () => {
      it('should have a expressRouteGatewaysListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteGatewaysListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteGatewaysListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysCreateOrUpdate - errors', () => {
      it('should have a expressRouteGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing putExpressRouteGatewayParameters', (done) => {
        try {
          a.expressRouteGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'putExpressRouteGatewayParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysGet - errors', () => {
      it('should have a expressRouteGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteGatewaysGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteGatewaysGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteGatewaysDelete - errors', () => {
      it('should have a expressRouteGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteConnectionsCreateOrUpdate - errors', () => {
      it('should have a expressRouteConnectionsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteConnectionsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteConnectionsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteConnectionsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.expressRouteConnectionsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing putExpressRouteConnectionParameters', (done) => {
        try {
          a.expressRouteConnectionsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'putExpressRouteConnectionParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteConnectionsGet - errors', () => {
      it('should have a expressRouteConnectionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteConnectionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteConnectionsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteConnectionsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.expressRouteConnectionsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteConnectionsDelete - errors', () => {
      it('should have a expressRouteConnectionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteConnectionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteConnectionsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteConnectionsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionName', (done) => {
        try {
          a.expressRouteConnectionsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteConnectionsList - errors', () => {
      it('should have a expressRouteConnectionsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteConnectionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteConnectionsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRouteGatewayName', (done) => {
        try {
          a.expressRouteConnectionsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRouteGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteConnectionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsGet - errors', () => {
      it('should have a ipGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.ipGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ipGroupsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipGroupsName', (done) => {
        try {
          a.ipGroupsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipGroupsName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsCreateOrUpdate - errors', () => {
      it('should have a ipGroupsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.ipGroupsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ipGroupsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipGroupsName', (done) => {
        try {
          a.ipGroupsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipGroupsName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.ipGroupsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsUpdateGroups - errors', () => {
      it('should have a ipGroupsUpdateGroups function', (done) => {
        try {
          assert.equal(true, typeof a.ipGroupsUpdateGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ipGroupsUpdateGroups(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsUpdateGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipGroupsName', (done) => {
        try {
          a.ipGroupsUpdateGroups('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipGroupsName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsUpdateGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.ipGroupsUpdateGroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsUpdateGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsDelete - errors', () => {
      it('should have a ipGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.ipGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ipGroupsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipGroupsName', (done) => {
        try {
          a.ipGroupsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipGroupsName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsListByResourceGroup - errors', () => {
      it('should have a ipGroupsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.ipGroupsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.ipGroupsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-ipGroupsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipGroupsList - errors', () => {
      it('should have a ipGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.ipGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesList - errors', () => {
      it('should have a webApplicationFirewallPoliciesList function', (done) => {
        try {
          assert.equal(true, typeof a.webApplicationFirewallPoliciesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.webApplicationFirewallPoliciesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesListAll - errors', () => {
      it('should have a webApplicationFirewallPoliciesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.webApplicationFirewallPoliciesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesGet - errors', () => {
      it('should have a webApplicationFirewallPoliciesGet function', (done) => {
        try {
          assert.equal(true, typeof a.webApplicationFirewallPoliciesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.webApplicationFirewallPoliciesGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.webApplicationFirewallPoliciesGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesCreateOrUpdate - errors', () => {
      it('should have a webApplicationFirewallPoliciesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.webApplicationFirewallPoliciesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.webApplicationFirewallPoliciesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.webApplicationFirewallPoliciesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.webApplicationFirewallPoliciesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webApplicationFirewallPoliciesDelete - errors', () => {
      it('should have a webApplicationFirewallPoliciesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.webApplicationFirewallPoliciesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.webApplicationFirewallPoliciesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.webApplicationFirewallPoliciesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-webApplicationFirewallPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsDelete - errors', () => {
      it('should have a networkSecurityGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.networkSecurityGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkSecurityGroupsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.networkSecurityGroupsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsGet - errors', () => {
      it('should have a networkSecurityGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkSecurityGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkSecurityGroupsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.networkSecurityGroupsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsCreateOrUpdate - errors', () => {
      it('should have a networkSecurityGroupsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.networkSecurityGroupsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkSecurityGroupsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.networkSecurityGroupsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkSecurityGroupsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsUpdateTags - errors', () => {
      it('should have a networkSecurityGroupsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.networkSecurityGroupsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkSecurityGroupsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.networkSecurityGroupsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkSecurityGroupsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsListAll - errors', () => {
      it('should have a networkSecurityGroupsListAll function', (done) => {
        try {
          assert.equal(true, typeof a.networkSecurityGroupsListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkSecurityGroupsList - errors', () => {
      it('should have a networkSecurityGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.networkSecurityGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkSecurityGroupsList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkSecurityGroupsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesDelete - errors', () => {
      it('should have a securityRulesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.securityRulesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.securityRulesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.securityRulesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityRuleName', (done) => {
        try {
          a.securityRulesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesGet - errors', () => {
      it('should have a securityRulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.securityRulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.securityRulesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.securityRulesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityRuleName', (done) => {
        try {
          a.securityRulesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesCreateOrUpdate - errors', () => {
      it('should have a securityRulesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.securityRulesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.securityRulesCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.securityRulesCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityRuleName', (done) => {
        try {
          a.securityRulesCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'securityRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityRuleParameters', (done) => {
        try {
          a.securityRulesCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityRuleParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityRulesList - errors', () => {
      it('should have a securityRulesList function', (done) => {
        try {
          assert.equal(true, typeof a.securityRulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.securityRulesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.securityRulesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-securityRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#defaultSecurityRulesList - errors', () => {
      it('should have a defaultSecurityRulesList function', (done) => {
        try {
          assert.equal(true, typeof a.defaultSecurityRulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.defaultSecurityRulesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-defaultSecurityRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.defaultSecurityRulesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-defaultSecurityRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#defaultSecurityRulesGet - errors', () => {
      it('should have a defaultSecurityRulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.defaultSecurityRulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.defaultSecurityRulesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-defaultSecurityRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSecurityGroupName', (done) => {
        try {
          a.defaultSecurityRulesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-defaultSecurityRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultSecurityRuleName', (done) => {
        try {
          a.defaultSecurityRulesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'defaultSecurityRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-defaultSecurityRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesDelete - errors', () => {
      it('should have a networkProfilesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.networkProfilesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkProfilesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkProfileName', (done) => {
        try {
          a.networkProfilesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesGet - errors', () => {
      it('should have a networkProfilesGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkProfilesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkProfilesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkProfileName', (done) => {
        try {
          a.networkProfilesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesCreateOrUpdate - errors', () => {
      it('should have a networkProfilesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.networkProfilesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkProfilesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkProfileName', (done) => {
        try {
          a.networkProfilesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkProfilesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesUpdateTags - errors', () => {
      it('should have a networkProfilesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.networkProfilesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkProfilesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkProfileName', (done) => {
        try {
          a.networkProfilesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkProfilesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesListAll - errors', () => {
      it('should have a networkProfilesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.networkProfilesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkProfilesList - errors', () => {
      it('should have a networkProfilesList function', (done) => {
        try {
          assert.equal(true, typeof a.networkProfilesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkProfilesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkProfilesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallFqdnTagsListAll - errors', () => {
      it('should have a azureFirewallFqdnTagsListAll function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallFqdnTagsListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysDelete - errors', () => {
      it('should have a natGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.natGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.natGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing natGatewayName', (done) => {
        try {
          a.natGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'natGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysGet - errors', () => {
      it('should have a natGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.natGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.natGatewaysGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing natGatewayName', (done) => {
        try {
          a.natGatewaysGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'natGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysCreateOrUpdate - errors', () => {
      it('should have a natGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.natGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.natGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing natGatewayName', (done) => {
        try {
          a.natGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'natGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.natGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysUpdateTags - errors', () => {
      it('should have a natGatewaysUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.natGatewaysUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.natGatewaysUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing natGatewayName', (done) => {
        try {
          a.natGatewaysUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'natGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.natGatewaysUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysListAll - errors', () => {
      it('should have a natGatewaysListAll function', (done) => {
        try {
          assert.equal(true, typeof a.natGatewaysListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#natGatewaysList - errors', () => {
      it('should have a natGatewaysList function', (done) => {
        try {
          assert.equal(true, typeof a.natGatewaysList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.natGatewaysList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-natGatewaysList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksDelete - errors', () => {
      it('should have a virtualNetworksDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworksDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksGet - errors', () => {
      it('should have a virtualNetworksGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworksGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksCreateOrUpdate - errors', () => {
      it('should have a virtualNetworksCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworksCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworksCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksUpdateTags - errors', () => {
      it('should have a virtualNetworksUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworksUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworksUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksListAll - errors', () => {
      it('should have a virtualNetworksListAll function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworksList - errors', () => {
      it('should have a virtualNetworksList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworksList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworksList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsDelete - errors', () => {
      it('should have a subnetsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.subnetsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.subnetsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.subnetsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.subnetsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsGet - errors', () => {
      it('should have a subnetsGet function', (done) => {
        try {
          assert.equal(true, typeof a.subnetsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.subnetsGet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.subnetsGet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.subnetsGet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsCreateOrUpdate - errors', () => {
      it('should have a subnetsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.subnetsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.subnetsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.subnetsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetName', (done) => {
        try {
          a.subnetsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetParameters', (done) => {
        try {
          a.subnetsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#subnetsList - errors', () => {
      it('should have a subnetsList function', (done) => {
        try {
          assert.equal(true, typeof a.subnetsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.subnetsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.subnetsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-subnetsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkPeeringsDelete - errors', () => {
      it('should have a virtualNetworkPeeringsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkPeeringsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkPeeringsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworkPeeringsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkPeeringName', (done) => {
        try {
          a.virtualNetworkPeeringsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkPeeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkPeeringsGet - errors', () => {
      it('should have a virtualNetworkPeeringsGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkPeeringsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkPeeringsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworkPeeringsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkPeeringName', (done) => {
        try {
          a.virtualNetworkPeeringsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkPeeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkPeeringsCreateOrUpdate - errors', () => {
      it('should have a virtualNetworkPeeringsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkPeeringsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkPeeringsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworkPeeringsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkPeeringName', (done) => {
        try {
          a.virtualNetworkPeeringsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkPeeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkPeeringParameters', (done) => {
        try {
          a.virtualNetworkPeeringsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkPeeringParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkPeeringsList - errors', () => {
      it('should have a virtualNetworkPeeringsList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkPeeringsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkPeeringsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.virtualNetworkPeeringsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesDelete - errors', () => {
      it('should have a firewallPoliciesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPoliciesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPoliciesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPoliciesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesGet - errors', () => {
      it('should have a firewallPoliciesGet function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPoliciesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPoliciesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPoliciesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesCreateOrUpdate - errors', () => {
      it('should have a firewallPoliciesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPoliciesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPoliciesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPoliciesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.firewallPoliciesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesList - errors', () => {
      it('should have a firewallPoliciesList function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPoliciesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPoliciesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPoliciesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPoliciesListAll - errors', () => {
      it('should have a firewallPoliciesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPoliciesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPolicyRuleGroupsDelete - errors', () => {
      it('should have a firewallPolicyRuleGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPolicyRuleGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPolicyRuleGroupsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPolicyRuleGroupsGet - errors', () => {
      it('should have a firewallPolicyRuleGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPolicyRuleGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPolicyRuleGroupsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPolicyRuleGroupsCreateOrUpdate - errors', () => {
      it('should have a firewallPolicyRuleGroupsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPolicyRuleGroupsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPolicyRuleGroupsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.firewallPolicyRuleGroupsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewallPolicyRuleGroupsList - errors', () => {
      it('should have a firewallPolicyRuleGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.firewallPolicyRuleGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.firewallPolicyRuleGroupsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyName', (done) => {
        try {
          a.firewallPolicyRuleGroupsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'firewallPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-firewallPolicyRuleGroupsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersDelete - errors', () => {
      it('should have a loadBalancersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancersDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancersDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersGet - errors', () => {
      it('should have a loadBalancersGet function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancersGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancersGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersCreateOrUpdate - errors', () => {
      it('should have a loadBalancersCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancersCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancersCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancersCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.loadBalancersCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersUpdateTags - errors', () => {
      it('should have a loadBalancersUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancersUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancersUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancersUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.loadBalancersUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersListAll - errors', () => {
      it('should have a loadBalancersListAll function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancersListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancersList - errors', () => {
      it('should have a loadBalancersList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancersList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerBackendAddressPoolsList - errors', () => {
      it('should have a loadBalancerBackendAddressPoolsList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerBackendAddressPoolsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerBackendAddressPoolsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerBackendAddressPoolsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerBackendAddressPoolsGet - errors', () => {
      it('should have a loadBalancerBackendAddressPoolsGet function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerBackendAddressPoolsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerBackendAddressPoolsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerBackendAddressPoolsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backendAddressPoolName', (done) => {
        try {
          a.loadBalancerBackendAddressPoolsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'backendAddressPoolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerBackendAddressPoolsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerFrontendIPConfigurationsList - errors', () => {
      it('should have a loadBalancerFrontendIPConfigurationsList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerFrontendIPConfigurationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerFrontendIPConfigurationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerFrontendIPConfigurationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerFrontendIPConfigurationsGet - errors', () => {
      it('should have a loadBalancerFrontendIPConfigurationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerFrontendIPConfigurationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerFrontendIPConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerFrontendIPConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing frontendIPConfigurationName', (done) => {
        try {
          a.loadBalancerFrontendIPConfigurationsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'frontendIPConfigurationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerFrontendIPConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesList - errors', () => {
      it('should have a inboundNatRulesList function', (done) => {
        try {
          assert.equal(true, typeof a.inboundNatRulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.inboundNatRulesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.inboundNatRulesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesDelete - errors', () => {
      it('should have a inboundNatRulesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.inboundNatRulesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.inboundNatRulesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.inboundNatRulesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboundNatRuleName', (done) => {
        try {
          a.inboundNatRulesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'inboundNatRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesGet - errors', () => {
      it('should have a inboundNatRulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.inboundNatRulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.inboundNatRulesGet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.inboundNatRulesGet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboundNatRuleName', (done) => {
        try {
          a.inboundNatRulesGet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'inboundNatRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboundNatRulesCreateOrUpdate - errors', () => {
      it('should have a inboundNatRulesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.inboundNatRulesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.inboundNatRulesCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.inboundNatRulesCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboundNatRuleName', (done) => {
        try {
          a.inboundNatRulesCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'inboundNatRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboundNatRuleParameters', (done) => {
        try {
          a.inboundNatRulesCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'inboundNatRuleParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-inboundNatRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerLoadBalancingRulesList - errors', () => {
      it('should have a loadBalancerLoadBalancingRulesList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerLoadBalancingRulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerLoadBalancingRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerLoadBalancingRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerLoadBalancingRulesGet - errors', () => {
      it('should have a loadBalancerLoadBalancingRulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerLoadBalancingRulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerLoadBalancingRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerLoadBalancingRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancingRuleName', (done) => {
        try {
          a.loadBalancerLoadBalancingRulesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancingRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerLoadBalancingRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerOutboundRulesList - errors', () => {
      it('should have a loadBalancerOutboundRulesList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerOutboundRulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerOutboundRulesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerOutboundRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerOutboundRulesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerOutboundRulesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerOutboundRulesGet - errors', () => {
      it('should have a loadBalancerOutboundRulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerOutboundRulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerOutboundRulesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerOutboundRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerOutboundRulesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerOutboundRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outboundRuleName', (done) => {
        try {
          a.loadBalancerOutboundRulesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'outboundRuleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerOutboundRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerNetworkInterfacesList - errors', () => {
      it('should have a loadBalancerNetworkInterfacesList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerNetworkInterfacesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerNetworkInterfacesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerNetworkInterfacesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerNetworkInterfacesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerNetworkInterfacesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerProbesList - errors', () => {
      it('should have a loadBalancerProbesList function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerProbesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerProbesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerProbesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerProbesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerProbesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadBalancerProbesGet - errors', () => {
      it('should have a loadBalancerProbesGet function', (done) => {
        try {
          assert.equal(true, typeof a.loadBalancerProbesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.loadBalancerProbesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerProbesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadBalancerName', (done) => {
        try {
          a.loadBalancerProbesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loadBalancerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerProbesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing probeName', (done) => {
        try {
          a.loadBalancerProbesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'probeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-loadBalancerProbesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesDelete - errors', () => {
      it('should have a publicIPPrefixesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPPrefixesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPPrefixesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpPrefixName', (done) => {
        try {
          a.publicIPPrefixesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'publicIpPrefixName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesGet - errors', () => {
      it('should have a publicIPPrefixesGet function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPPrefixesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPPrefixesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpPrefixName', (done) => {
        try {
          a.publicIPPrefixesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpPrefixName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesCreateOrUpdate - errors', () => {
      it('should have a publicIPPrefixesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPPrefixesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPPrefixesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpPrefixName', (done) => {
        try {
          a.publicIPPrefixesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpPrefixName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.publicIPPrefixesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesUpdateTags - errors', () => {
      it('should have a publicIPPrefixesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPPrefixesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPPrefixesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIpPrefixName', (done) => {
        try {
          a.publicIPPrefixesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'publicIpPrefixName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.publicIPPrefixesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesListAll - errors', () => {
      it('should have a publicIPPrefixesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPPrefixesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publicIPPrefixesList - errors', () => {
      it('should have a publicIPPrefixesList function', (done) => {
        try {
          assert.equal(true, typeof a.publicIPPrefixesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.publicIPPrefixesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-publicIPPrefixesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#operationsList - errors', () => {
      it('should have a operationsList function', (done) => {
        try {
          assert.equal(true, typeof a.operationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bgpServiceCommunitiesList - errors', () => {
      it('should have a bgpServiceCommunitiesList function', (done) => {
        try {
          assert.equal(true, typeof a.bgpServiceCommunitiesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysDelete - errors', () => {
      it('should have a applicationGatewaysDelete function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysGet - errors', () => {
      it('should have a applicationGatewaysGet function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysCreateOrUpdate - errors', () => {
      it('should have a applicationGatewaysCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.applicationGatewaysCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysUpdateTags - errors', () => {
      it('should have a applicationGatewaysUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.applicationGatewaysUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysList - errors', () => {
      it('should have a applicationGatewaysList function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAll - errors', () => {
      it('should have a applicationGatewaysListAll function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysStart - errors', () => {
      it('should have a applicationGatewaysStart function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysStart(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysStart('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysStop - errors', () => {
      it('should have a applicationGatewaysStop function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysStop(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysStop('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysBackendHealth - errors', () => {
      it('should have a applicationGatewaysBackendHealth function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysBackendHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysBackendHealth(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysBackendHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysBackendHealth('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysBackendHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysBackendHealthOnDemand - errors', () => {
      it('should have a applicationGatewaysBackendHealthOnDemand function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysBackendHealthOnDemand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationGatewaysBackendHealthOnDemand(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysBackendHealthOnDemand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationGatewayName', (done) => {
        try {
          a.applicationGatewaysBackendHealthOnDemand('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationGatewayName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysBackendHealthOnDemand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing probeRequest', (done) => {
        try {
          a.applicationGatewaysBackendHealthOnDemand('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'probeRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysBackendHealthOnDemand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableServerVariables - errors', () => {
      it('should have a applicationGatewaysListAvailableServerVariables function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAvailableServerVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableRequestHeaders - errors', () => {
      it('should have a applicationGatewaysListAvailableRequestHeaders function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAvailableRequestHeaders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableResponseHeaders - errors', () => {
      it('should have a applicationGatewaysListAvailableResponseHeaders function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAvailableResponseHeaders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableWafRuleSets - errors', () => {
      it('should have a applicationGatewaysListAvailableWafRuleSets function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAvailableWafRuleSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableSslOptions - errors', () => {
      it('should have a applicationGatewaysListAvailableSslOptions function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAvailableSslOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysListAvailableSslPredefinedPolicies - errors', () => {
      it('should have a applicationGatewaysListAvailableSslPredefinedPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysListAvailableSslPredefinedPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGatewaysGetSslPredefinedPolicy - errors', () => {
      it('should have a applicationGatewaysGetSslPredefinedPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGatewaysGetSslPredefinedPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing predefinedPolicyName', (done) => {
        try {
          a.applicationGatewaysGetSslPredefinedPolicy(null, (data, error) => {
            try {
              const displayE = 'predefinedPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationGatewaysGetSslPredefinedPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsLocationsList - errors', () => {
      it('should have a expressRoutePortsLocationsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsLocationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsLocationsGet - errors', () => {
      it('should have a expressRoutePortsLocationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsLocationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationName', (done) => {
        try {
          a.expressRoutePortsLocationsGet(null, (data, error) => {
            try {
              const displayE = 'locationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsLocationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsDelete - errors', () => {
      it('should have a expressRoutePortsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRoutePortsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRoutePortName', (done) => {
        try {
          a.expressRoutePortsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRoutePortName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsGet - errors', () => {
      it('should have a expressRoutePortsGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRoutePortsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRoutePortName', (done) => {
        try {
          a.expressRoutePortsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRoutePortName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsCreateOrUpdate - errors', () => {
      it('should have a expressRoutePortsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRoutePortsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRoutePortName', (done) => {
        try {
          a.expressRoutePortsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'expressRoutePortName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.expressRoutePortsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsUpdateTags - errors', () => {
      it('should have a expressRoutePortsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRoutePortsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRoutePortName', (done) => {
        try {
          a.expressRoutePortsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'expressRoutePortName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.expressRoutePortsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsListByResourceGroup - errors', () => {
      it('should have a expressRoutePortsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRoutePortsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRoutePortsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRoutePortsList - errors', () => {
      it('should have a expressRoutePortsList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRoutePortsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteLinksGet - errors', () => {
      it('should have a expressRouteLinksGet function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteLinksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteLinksGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRoutePortName', (done) => {
        try {
          a.expressRouteLinksGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'expressRoutePortName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkName', (done) => {
        try {
          a.expressRouteLinksGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expressRouteLinksList - errors', () => {
      it('should have a expressRouteLinksList function', (done) => {
        try {
          assert.equal(true, typeof a.expressRouteLinksList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.expressRouteLinksList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expressRoutePortName', (done) => {
        try {
          a.expressRouteLinksList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expressRoutePortName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-expressRouteLinksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsDelete - errors', () => {
      it('should have a privateEndpointsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.privateEndpointsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateEndpointsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing privateEndpointName', (done) => {
        try {
          a.privateEndpointsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'privateEndpointName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsGet - errors', () => {
      it('should have a privateEndpointsGet function', (done) => {
        try {
          assert.equal(true, typeof a.privateEndpointsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateEndpointsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing privateEndpointName', (done) => {
        try {
          a.privateEndpointsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'privateEndpointName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsCreateOrUpdate - errors', () => {
      it('should have a privateEndpointsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.privateEndpointsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateEndpointsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing privateEndpointName', (done) => {
        try {
          a.privateEndpointsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'privateEndpointName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.privateEndpointsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsList - errors', () => {
      it('should have a privateEndpointsList function', (done) => {
        try {
          assert.equal(true, typeof a.privateEndpointsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateEndpointsList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateEndpointsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateEndpointsListBySubscription - errors', () => {
      it('should have a privateEndpointsListBySubscription function', (done) => {
        try {
          assert.equal(true, typeof a.privateEndpointsListBySubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsDelete - errors', () => {
      it('should have a applicationSecurityGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.applicationSecurityGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationSecurityGroupsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationSecurityGroupName', (done) => {
        try {
          a.applicationSecurityGroupsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsGet - errors', () => {
      it('should have a applicationSecurityGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.applicationSecurityGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationSecurityGroupsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationSecurityGroupName', (done) => {
        try {
          a.applicationSecurityGroupsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsCreateOrUpdate - errors', () => {
      it('should have a applicationSecurityGroupsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.applicationSecurityGroupsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationSecurityGroupsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationSecurityGroupName', (done) => {
        try {
          a.applicationSecurityGroupsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.applicationSecurityGroupsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsUpdateTags - errors', () => {
      it('should have a applicationSecurityGroupsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.applicationSecurityGroupsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationSecurityGroupsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationSecurityGroupName', (done) => {
        try {
          a.applicationSecurityGroupsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationSecurityGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.applicationSecurityGroupsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsListAll - errors', () => {
      it('should have a applicationSecurityGroupsListAll function', (done) => {
        try {
          assert.equal(true, typeof a.applicationSecurityGroupsListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationSecurityGroupsList - errors', () => {
      it('should have a applicationSecurityGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.applicationSecurityGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.applicationSecurityGroupsList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-applicationSecurityGroupsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesDelete - errors', () => {
      it('should have a serviceEndpointPoliciesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPoliciesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPoliciesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPoliciesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesGet - errors', () => {
      it('should have a serviceEndpointPoliciesGet function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPoliciesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPoliciesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPoliciesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesCreateOrUpdate - errors', () => {
      it('should have a serviceEndpointPoliciesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPoliciesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPoliciesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPoliciesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.serviceEndpointPoliciesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesUpdateTags - errors', () => {
      it('should have a serviceEndpointPoliciesUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPoliciesUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPoliciesUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPoliciesUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.serviceEndpointPoliciesUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesList - errors', () => {
      it('should have a serviceEndpointPoliciesList function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPoliciesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPoliciesListByResourceGroup - errors', () => {
      it('should have a serviceEndpointPoliciesListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPoliciesListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPoliciesListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPoliciesListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPolicyDefinitionsDelete - errors', () => {
      it('should have a serviceEndpointPolicyDefinitionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPolicyDefinitionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyDefinitionName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyDefinitionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPolicyDefinitionsGet - errors', () => {
      it('should have a serviceEndpointPolicyDefinitionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPolicyDefinitionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyDefinitionName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyDefinitionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPolicyDefinitionsCreateOrUpdate - errors', () => {
      it('should have a serviceEndpointPolicyDefinitionsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPolicyDefinitionsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyDefinitionName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyDefinitionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyDefinitions', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyDefinitions is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceEndpointPolicyDefinitionsListByResourceGroup - errors', () => {
      it('should have a serviceEndpointPolicyDefinitionsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.serviceEndpointPolicyDefinitionsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsListByResourceGroup(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointPolicyName', (done) => {
        try {
          a.serviceEndpointPolicyDefinitionsListByResourceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceEndpointPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-serviceEndpointPolicyDefinitionsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersDelete - errors', () => {
      it('should have a virtualRoutersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRoutersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRoutersDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRoutersDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersGet - errors', () => {
      it('should have a virtualRoutersGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRoutersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRoutersGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRoutersGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersCreateOrUpdate - errors', () => {
      it('should have a virtualRoutersCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRoutersCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRoutersCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRoutersCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualRoutersCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersListByResourceGroup - errors', () => {
      it('should have a virtualRoutersListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRoutersListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRoutersListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRoutersListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRoutersList - errors', () => {
      it('should have a virtualRoutersList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRoutersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRouterPeeringsDelete - errors', () => {
      it('should have a virtualRouterPeeringsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRouterPeeringsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRouterPeeringsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRouterPeeringsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.virtualRouterPeeringsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRouterPeeringsGet - errors', () => {
      it('should have a virtualRouterPeeringsGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRouterPeeringsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRouterPeeringsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRouterPeeringsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.virtualRouterPeeringsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRouterPeeringsCreateOrUpdate - errors', () => {
      it('should have a virtualRouterPeeringsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRouterPeeringsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRouterPeeringsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRouterPeeringsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peeringName', (done) => {
        try {
          a.virtualRouterPeeringsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peeringName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualRouterPeeringsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualRouterPeeringsList - errors', () => {
      it('should have a virtualRouterPeeringsList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualRouterPeeringsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualRouterPeeringsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualRouterName', (done) => {
        try {
          a.virtualRouterPeeringsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'virtualRouterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualRouterPeeringsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsDelete - errors', () => {
      it('should have a virtualNetworkTapsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkTapsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkTapsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapName', (done) => {
        try {
          a.virtualNetworkTapsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tapName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsGet - errors', () => {
      it('should have a virtualNetworkTapsGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkTapsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkTapsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapName', (done) => {
        try {
          a.virtualNetworkTapsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tapName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsCreateOrUpdate - errors', () => {
      it('should have a virtualNetworkTapsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkTapsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkTapsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapName', (done) => {
        try {
          a.virtualNetworkTapsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tapName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualNetworkTapsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsUpdateTags - errors', () => {
      it('should have a virtualNetworkTapsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkTapsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkTapsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapName', (done) => {
        try {
          a.virtualNetworkTapsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tapName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tapParameters', (done) => {
        try {
          a.virtualNetworkTapsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tapParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsListAll - errors', () => {
      it('should have a virtualNetworkTapsListAll function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkTapsListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualNetworkTapsListByResourceGroup - errors', () => {
      it('should have a virtualNetworkTapsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.virtualNetworkTapsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualNetworkTapsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualNetworkTapsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsDelete - errors', () => {
      it('should have a azureFirewallsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.azureFirewallsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureFirewallName', (done) => {
        try {
          a.azureFirewallsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'azureFirewallName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsGet - errors', () => {
      it('should have a azureFirewallsGet function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.azureFirewallsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureFirewallName', (done) => {
        try {
          a.azureFirewallsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'azureFirewallName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsCreateOrUpdate - errors', () => {
      it('should have a azureFirewallsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.azureFirewallsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureFirewallName', (done) => {
        try {
          a.azureFirewallsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'azureFirewallName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.azureFirewallsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsUpdateTags - errors', () => {
      it('should have a azureFirewallsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.azureFirewallsUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureFirewallName', (done) => {
        try {
          a.azureFirewallsUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'azureFirewallName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.azureFirewallsUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsList - errors', () => {
      it('should have a azureFirewallsList function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.azureFirewallsList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-azureFirewallsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#azureFirewallsListAll - errors', () => {
      it('should have a azureFirewallsListAll function', (done) => {
        try {
          assert.equal(true, typeof a.azureFirewallsListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersCreateOrUpdate - errors', () => {
      it('should have a networkWatchersCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGet - errors', () => {
      it('should have a networkWatchersGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersDelete - errors', () => {
      it('should have a networkWatchersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersUpdateTags - errors', () => {
      it('should have a networkWatchersUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersList - errors', () => {
      it('should have a networkWatchersList function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersListAll - errors', () => {
      it('should have a networkWatchersListAll function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetTopology - errors', () => {
      it('should have a networkWatchersGetTopology function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetTopology === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetTopology(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTopology', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetTopology('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTopology', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetTopology('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTopology', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersVerifyIPFlow - errors', () => {
      it('should have a networkWatchersVerifyIPFlow function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersVerifyIPFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersVerifyIPFlow(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersVerifyIPFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersVerifyIPFlow('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersVerifyIPFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersVerifyIPFlow('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersVerifyIPFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetNextHop - errors', () => {
      it('should have a networkWatchersGetNextHop function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetNextHop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetNextHop(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetNextHop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetNextHop('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetNextHop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetNextHop('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetNextHop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetVMSecurityRules - errors', () => {
      it('should have a networkWatchersGetVMSecurityRules function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetVMSecurityRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetVMSecurityRules(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetVMSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetVMSecurityRules('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetVMSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetVMSecurityRules('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetVMSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetTroubleshooting - errors', () => {
      it('should have a networkWatchersGetTroubleshooting function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetTroubleshooting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetTroubleshooting(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTroubleshooting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetTroubleshooting('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTroubleshooting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetTroubleshooting('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTroubleshooting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetTroubleshootingResult - errors', () => {
      it('should have a networkWatchersGetTroubleshootingResult function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetTroubleshootingResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetTroubleshootingResult(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTroubleshootingResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetTroubleshootingResult('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTroubleshootingResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetTroubleshootingResult('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetTroubleshootingResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersSetFlowLogConfiguration - errors', () => {
      it('should have a networkWatchersSetFlowLogConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersSetFlowLogConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersSetFlowLogConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersSetFlowLogConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersSetFlowLogConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersSetFlowLogConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersSetFlowLogConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersSetFlowLogConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetFlowLogStatus - errors', () => {
      it('should have a networkWatchersGetFlowLogStatus function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetFlowLogStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetFlowLogStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetFlowLogStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetFlowLogStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetFlowLogStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetFlowLogStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetFlowLogStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersCheckConnectivity - errors', () => {
      it('should have a networkWatchersCheckConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersCheckConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersCheckConnectivity(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersCheckConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersCheckConnectivity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersCheckConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersCheckConnectivity('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersCheckConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetAzureReachabilityReport - errors', () => {
      it('should have a networkWatchersGetAzureReachabilityReport function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetAzureReachabilityReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetAzureReachabilityReport(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetAzureReachabilityReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetAzureReachabilityReport('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetAzureReachabilityReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetAzureReachabilityReport('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetAzureReachabilityReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersListAvailableProviders - errors', () => {
      it('should have a networkWatchersListAvailableProviders function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersListAvailableProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersListAvailableProviders(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersListAvailableProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersListAvailableProviders('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersListAvailableProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersListAvailableProviders('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersListAvailableProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkWatchersGetNetworkConfigurationDiagnostic - errors', () => {
      it('should have a networkWatchersGetNetworkConfigurationDiagnostic function', (done) => {
        try {
          assert.equal(true, typeof a.networkWatchersGetNetworkConfigurationDiagnostic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.networkWatchersGetNetworkConfigurationDiagnostic(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetNetworkConfigurationDiagnostic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.networkWatchersGetNetworkConfigurationDiagnostic('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetNetworkConfigurationDiagnostic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.networkWatchersGetNetworkConfigurationDiagnostic('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-networkWatchersGetNetworkConfigurationDiagnostic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesCreate - errors', () => {
      it('should have a packetCapturesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.packetCapturesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.packetCapturesCreate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.packetCapturesCreate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packetCaptureName', (done) => {
        try {
          a.packetCapturesCreate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'packetCaptureName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.packetCapturesCreate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesGet - errors', () => {
      it('should have a packetCapturesGet function', (done) => {
        try {
          assert.equal(true, typeof a.packetCapturesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.packetCapturesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.packetCapturesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packetCaptureName', (done) => {
        try {
          a.packetCapturesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'packetCaptureName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesDelete - errors', () => {
      it('should have a packetCapturesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.packetCapturesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.packetCapturesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.packetCapturesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packetCaptureName', (done) => {
        try {
          a.packetCapturesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'packetCaptureName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesStop - errors', () => {
      it('should have a packetCapturesStop function', (done) => {
        try {
          assert.equal(true, typeof a.packetCapturesStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.packetCapturesStop(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.packetCapturesStop('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packetCaptureName', (done) => {
        try {
          a.packetCapturesStop('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'packetCaptureName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesGetStatus - errors', () => {
      it('should have a packetCapturesGetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.packetCapturesGetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.packetCapturesGetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesGetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.packetCapturesGetStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesGetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packetCaptureName', (done) => {
        try {
          a.packetCapturesGetStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'packetCaptureName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesGetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packetCapturesList - errors', () => {
      it('should have a packetCapturesList function', (done) => {
        try {
          assert.equal(true, typeof a.packetCapturesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.packetCapturesList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.packetCapturesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-packetCapturesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsCreateOrUpdate - errors', () => {
      it('should have a connectionMonitorsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.connectionMonitorsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsGet - errors', () => {
      it('should have a connectionMonitorsGet function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsDelete - errors', () => {
      it('should have a connectionMonitorsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsUpdateTags - errors', () => {
      it('should have a connectionMonitorsUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsUpdateTags(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsUpdateTags('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsUpdateTags('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.connectionMonitorsUpdateTags('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsStop - errors', () => {
      it('should have a connectionMonitorsStop function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsStop(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsStop('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsStop('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsStart - errors', () => {
      it('should have a connectionMonitorsStart function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsStart(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsStart('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsStart('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsQuery - errors', () => {
      it('should have a connectionMonitorsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionMonitorName', (done) => {
        try {
          a.connectionMonitorsQuery('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionMonitorName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionMonitorsList - errors', () => {
      it('should have a connectionMonitorsList function', (done) => {
        try {
          assert.equal(true, typeof a.connectionMonitorsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.connectionMonitorsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.connectionMonitorsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-connectionMonitorsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowLogsCreateOrUpdate - errors', () => {
      it('should have a flowLogsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.flowLogsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.flowLogsCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.flowLogsCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowLogName', (done) => {
        try {
          a.flowLogsCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'flowLogName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.flowLogsCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowLogsGet - errors', () => {
      it('should have a flowLogsGet function', (done) => {
        try {
          assert.equal(true, typeof a.flowLogsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.flowLogsGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.flowLogsGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowLogName', (done) => {
        try {
          a.flowLogsGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'flowLogName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowLogsDelete - errors', () => {
      it('should have a flowLogsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.flowLogsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.flowLogsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.flowLogsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowLogName', (done) => {
        try {
          a.flowLogsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'flowLogName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowLogsList - errors', () => {
      it('should have a flowLogsList function', (done) => {
        try {
          assert.equal(true, typeof a.flowLogsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.flowLogsList(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkWatcherName', (done) => {
        try {
          a.flowLogsList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkWatcherName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-flowLogsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesDelete - errors', () => {
      it('should have a privateLinkServicesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesGet - errors', () => {
      it('should have a privateLinkServicesGet function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesList - errors', () => {
      it('should have a privateLinkServicesList function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesList(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListBySubscription - errors', () => {
      it('should have a privateLinkServicesListBySubscription function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesListBySubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesGetPrivateEndpointConnection - errors', () => {
      it('should have a privateLinkServicesGetPrivateEndpointConnection function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesGetPrivateEndpointConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesGetPrivateEndpointConnection(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesGetPrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesGetPrivateEndpointConnection('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesGetPrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peConnectionName', (done) => {
        try {
          a.privateLinkServicesGetPrivateEndpointConnection('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesGetPrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesUpdatePrivateEndpointConnection - errors', () => {
      it('should have a privateLinkServicesUpdatePrivateEndpointConnection function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesUpdatePrivateEndpointConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesUpdatePrivateEndpointConnection(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesUpdatePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesUpdatePrivateEndpointConnection('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesUpdatePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peConnectionName', (done) => {
        try {
          a.privateLinkServicesUpdatePrivateEndpointConnection('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'peConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesUpdatePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.privateLinkServicesUpdatePrivateEndpointConnection('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesUpdatePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesDeletePrivateEndpointConnection - errors', () => {
      it('should have a privateLinkServicesDeletePrivateEndpointConnection function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesDeletePrivateEndpointConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesDeletePrivateEndpointConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesDeletePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesDeletePrivateEndpointConnection('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesDeletePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peConnectionName', (done) => {
        try {
          a.privateLinkServicesDeletePrivateEndpointConnection('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'peConnectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesDeletePrivateEndpointConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListPrivateEndpointConnections - errors', () => {
      it('should have a privateLinkServicesListPrivateEndpointConnections function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesListPrivateEndpointConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesListPrivateEndpointConnections(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesListPrivateEndpointConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesListPrivateEndpointConnections('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesListPrivateEndpointConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesCheckPrivateLinkServiceVisibility - errors', () => {
      it('should have a privateLinkServicesCheckPrivateLinkServiceVisibility function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesCheckPrivateLinkServiceVisibility === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibility(null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCheckPrivateLinkServiceVisibility', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibility('fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCheckPrivateLinkServiceVisibility', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup - errors', () => {
      it('should have a privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListAutoApprovedPrivateLinkServices - errors', () => {
      it('should have a privateLinkServicesListAutoApprovedPrivateLinkServices function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesListAutoApprovedPrivateLinkServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.privateLinkServicesListAutoApprovedPrivateLinkServices(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesListAutoApprovedPrivateLinkServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup - errors', () => {
      it('should have a privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup(null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#privateLinkServicesCreateOrUpdate - errors', () => {
      it('should have a privateLinkServicesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.privateLinkServicesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.privateLinkServicesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.privateLinkServicesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.privateLinkServicesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-privateLinkServicesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsDelete - errors', () => {
      it('should have a bastionHostsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.bastionHostsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.bastionHostsDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bastionHostName', (done) => {
        try {
          a.bastionHostsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bastionHostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsGet - errors', () => {
      it('should have a bastionHostsGet function', (done) => {
        try {
          assert.equal(true, typeof a.bastionHostsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.bastionHostsGet(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bastionHostName', (done) => {
        try {
          a.bastionHostsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bastionHostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsCreateOrUpdate - errors', () => {
      it('should have a bastionHostsCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.bastionHostsCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.bastionHostsCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bastionHostName', (done) => {
        try {
          a.bastionHostsCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bastionHostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.bastionHostsCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsList - errors', () => {
      it('should have a bastionHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.bastionHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bastionHostsListByResourceGroup - errors', () => {
      it('should have a bastionHostsListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.bastionHostsListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.bastionHostsListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-bastionHostsListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersDelete - errors', () => {
      it('should have a routeFiltersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.routeFiltersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFiltersDelete(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFiltersDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersGet - errors', () => {
      it('should have a routeFiltersGet function', (done) => {
        try {
          assert.equal(true, typeof a.routeFiltersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFiltersGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFiltersGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersCreateOrUpdate - errors', () => {
      it('should have a routeFiltersCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.routeFiltersCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFiltersCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFiltersCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterParameters', (done) => {
        try {
          a.routeFiltersCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeFilterParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersUpdateTags - errors', () => {
      it('should have a routeFiltersUpdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.routeFiltersUpdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFiltersUpdateTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFiltersUpdateTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.routeFiltersUpdateTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersUpdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersListByResourceGroup - errors', () => {
      it('should have a routeFiltersListByResourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.routeFiltersListByResourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFiltersListByResourceGroup(null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFiltersListByResourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFiltersList - errors', () => {
      it('should have a routeFiltersList function', (done) => {
        try {
          assert.equal(true, typeof a.routeFiltersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFilterRulesDelete - errors', () => {
      it('should have a routeFilterRulesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.routeFilterRulesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFilterRulesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFilterRulesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.routeFilterRulesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFilterRulesGet - errors', () => {
      it('should have a routeFilterRulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.routeFilterRulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFilterRulesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFilterRulesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.routeFilterRulesGet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFilterRulesCreateOrUpdate - errors', () => {
      it('should have a routeFilterRulesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.routeFilterRulesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFilterRulesCreateOrUpdate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFilterRulesCreateOrUpdate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.routeFilterRulesCreateOrUpdate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterRuleParameters', (done) => {
        try {
          a.routeFilterRulesCreateOrUpdate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeFilterRuleParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#routeFilterRulesListByRouteFilter - errors', () => {
      it('should have a routeFilterRulesListByRouteFilter function', (done) => {
        try {
          assert.equal(true, typeof a.routeFilterRulesListByRouteFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.routeFilterRulesListByRouteFilter(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesListByRouteFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeFilterName', (done) => {
        try {
          a.routeFilterRulesListByRouteFilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeFilterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-routeFilterRulesListByRouteFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesListAll - errors', () => {
      it('should have a virtualMachinesListAll function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesListAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesDelete - errors', () => {
      it('should have a virtualMachinesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesList - errors', () => {
      it('should have a virtualMachinesList function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesList(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesGet - errors', () => {
      it('should have a virtualMachinesGet function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesGet(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesCreateOrUpdate - errors', () => {
      it('should have a virtualMachinesCreateOrUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesCreateOrUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesCreateOrUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesCreateOrUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parameters', (done) => {
        try {
          a.virtualMachinesCreateOrUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesCreateOrUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesRunCommand - errors', () => {
      it('should have a virtualMachinesRunCommand function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesRunCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesRunCommand(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesRunCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesRunCommand('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesRunCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.virtualMachinesRunCommand('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesRunCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesStart - errors', () => {
      it('should have a virtualMachinesStart function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesStart(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesStart('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesRestart - errors', () => {
      it('should have a virtualMachinesRestart function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesRestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesRestart(null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesRestart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesRestart('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesRestart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualMachinesPowerOff - errors', () => {
      it('should have a virtualMachinesPowerOff function', (done) => {
        try {
          assert.equal(true, typeof a.virtualMachinesPowerOff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceGroupName', (done) => {
        try {
          a.virtualMachinesPowerOff(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesPowerOff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.virtualMachinesPowerOff('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure-adapter-virtualMachinesPowerOff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
