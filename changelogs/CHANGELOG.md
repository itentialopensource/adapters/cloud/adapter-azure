
## 0.1.6 [08-02-2023]

* Changes for auth and add deployment

See merge request itentialopensource/adapters/cloud/adapter-azure!4

---

## 0.1.5 [04-04-2023]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-azure!2

---

## 0.1.4 [04-04-2023]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-azure!2

---

## 0.1.3 [03-02-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-azure!2

---

## 0.1.2 [07-07-2020]

* migration

See merge request itentialopensource/adapters/cloud/adapter-azure!1

---

## 0.1.1 [03-27-2020]

* Bug fixes and performance improvements

See commit 5187b7c

---
