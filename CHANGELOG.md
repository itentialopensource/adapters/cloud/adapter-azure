
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_21:11PM

See merge request itentialopensource/adapters/adapter-azure!22

---

## 0.3.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-azure!20

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_19:24PM

See merge request itentialopensource/adapters/adapter-azure!19

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_21:05PM

See merge request itentialopensource/adapters/adapter-azure!18

---

## 0.3.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-azure!17

---

## 0.2.8 [04-26-2024]

* update utils version

See merge request itentialopensource/adapters/cloud/adapter-azure!16

---

## 0.2.7 [04-26-2024]

* change to add resource

See merge request itentialopensource/adapters/cloud/adapter-azure!15

---

## 0.2.6 [04-17-2024]

* Patch/adapt 3323

See merge request itentialopensource/adapters/cloud/adapter-azure!14

---

## 0.2.5 [03-28-2024]

* Changes made at 2024.03.28_13:24PM

See merge request itentialopensource/adapters/cloud/adapter-azure!13

---

## 0.2.4 [03-21-2024]

* Changes made at 2024.03.21_13:56PM

See merge request itentialopensource/adapters/cloud/adapter-azure!12

---

## 0.2.3 [03-11-2024]

* Changes made at 2024.03.11_15:37PM

See merge request itentialopensource/adapters/cloud/adapter-azure!11

---

## 0.2.2 [02-28-2024]

* Changes made at 2024.02.28_11:52AM

See merge request itentialopensource/adapters/cloud/adapter-azure!10

---

## 0.2.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-azure!9

---

## 0.2.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-azure!5

---

## 0.1.7 [11-21-2023]

* Update generic entity

See merge request itentialopensource/adapters/cloud/adapter-azure!6

---

## 0.1.6 [08-02-2023]

* Changes for auth and add deployment

See merge request itentialopensource/adapters/cloud/adapter-azure!4

---

## 0.1.5 [04-04-2023]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-azure!2

---

## 0.1.4 [04-04-2023]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-azure!2

---

## 0.1.3 [03-02-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-azure!2

---

## 0.1.2 [07-07-2020]

* migration

See merge request itentialopensource/adapters/cloud/adapter-azure!1

---

## 0.1.1 [03-27-2020]

* Bug fixes and performance improvements

See commit 5187b7c

---
