## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Microsoft Azure. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Microsoft Azure.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of Microsoft Azure. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesDelete(resourceGroupName, networkInterfaceName, callback)</td>
    <td style="padding:15px">Deletes the specified network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesGet(resourceGroupName, networkInterfaceName, expand, callback)</td>
    <td style="padding:15px">Gets information about the specified network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesCreateOrUpdate(resourceGroupName, networkInterfaceName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesUpdateTags(resourceGroupName, networkInterfaceName, parameters, callback)</td>
    <td style="padding:15px">Updates a network interface tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesListAll(callback)</td>
    <td style="padding:15px">Gets all network interfaces in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/networkInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all network interfaces in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesGetEffectiveRouteTable(resourceGroupName, networkInterfaceName, callback)</td>
    <td style="padding:15px">Gets all route tables applied to a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/effectiveRouteTable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesListEffectiveNetworkSecurityGroups(resourceGroupName, networkInterfaceName, callback)</td>
    <td style="padding:15px">Gets all network security groups applied to a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/effectiveNetworkSecurityGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceIPConfigurationsList(resourceGroupName, networkInterfaceName, callback)</td>
    <td style="padding:15px">Get all ip configurations in a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/ipConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceIPConfigurationsGet(resourceGroupName, networkInterfaceName, ipConfigurationName, callback)</td>
    <td style="padding:15px">Gets the specified network interface ip configuration.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/ipConfigurations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceLoadBalancersList(resourceGroupName, networkInterfaceName, callback)</td>
    <td style="padding:15px">List all load balancers in a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/loadBalancers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceTapConfigurationsDelete(resourceGroupName, networkInterfaceName, tapConfigurationName, callback)</td>
    <td style="padding:15px">Deletes the specified tap configuration from the NetworkInterface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/tapConfigurations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceTapConfigurationsGet(resourceGroupName, networkInterfaceName, tapConfigurationName, callback)</td>
    <td style="padding:15px">Get the specified tap configuration on a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/tapConfigurations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceTapConfigurationsCreateOrUpdate(resourceGroupName, networkInterfaceName, tapConfigurationName, tapConfigurationParameters, callback)</td>
    <td style="padding:15px">Creates or updates a Tap configuration in the specified NetworkInterface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/tapConfigurations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfaceTapConfigurationsList(resourceGroupName, networkInterfaceName, callback)</td>
    <td style="padding:15px">Get all Tap configurations in a network interface.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkInterfaces/{pathv3}/tapConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesListVirtualMachineScaleSetVMNetworkInterfaces(resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, callback)</td>
    <td style="padding:15px">Gets information about all network interfaces in a virtual machine in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/microsoft.Compute/virtualMachineScaleSets/{pathv3}/virtualMachines/{pathv4}/networkInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesListVirtualMachineScaleSetNetworkInterfaces(resourceGroupName, virtualMachineScaleSetName, callback)</td>
    <td style="padding:15px">Gets all network interfaces in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/microsoft.Compute/virtualMachineScaleSets/{pathv3}/networkInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesGetVirtualMachineScaleSetNetworkInterface(resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, networkInterfaceName, expand, callback)</td>
    <td style="padding:15px">Get the specified network interface in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/microsoft.Compute/virtualMachineScaleSets/{pathv3}/virtualMachines/{pathv4}/networkInterfaces/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesListVirtualMachineScaleSetIpConfigurations(resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, networkInterfaceName, expand, callback)</td>
    <td style="padding:15px">Get the specified network interface ip configuration in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/microsoft.Compute/virtualMachineScaleSets/{pathv3}/virtualMachines/{pathv4}/networkInterfaces/{pathv5}/ipConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInterfacesGetVirtualMachineScaleSetIpConfiguration(resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, networkInterfaceName, ipConfigurationName, expand, callback)</td>
    <td style="padding:15px">Get the specified network interface ip configuration in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/microsoft.Compute/virtualMachineScaleSets/{pathv3}/virtualMachines/{pathv4}/networkInterfaces/{pathv5}/ipConfigurations/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availableServiceAliasesList(location, callback)</td>
    <td style="padding:15px">Gets all available service aliases for this subscription in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/availableServiceAliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availableServiceAliasesListByResourceGroup(resourceGroupName, location, callback)</td>
    <td style="padding:15px">Gets all available service aliases for this resource group in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/locations/{pathv3}/availableServiceAliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkDnsNameAvailability(location, domainNameLabel, callback)</td>
    <td style="padding:15px">Checks whether a domain name in the cloudapp.azure.com zone is available for use.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/CheckDnsNameAvailability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceTagsList(location, callback)</td>
    <td style="padding:15px">Gets a list of service tag information resources.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/serviceTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availableDelegationsList(location, callback)</td>
    <td style="padding:15px">Gets all of the available subnet delegations for this subscription in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/availableDelegations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availableResourceGroupDelegationsList(location, resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all of the available subnet delegations for this resource group in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/locations/{pathv3}/availableDelegations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availableEndpointServicesList(location, callback)</td>
    <td style="padding:15px">List what values of endpoint services are available for use.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/virtualNetworkAvailableEndpointServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetsPrepareNetworkPolicies(resourceGroupName, virtualNetworkName, subnetName, prepareNetworkPoliciesRequestParameters, callback)</td>
    <td style="padding:15px">Prepares a subnet by applying network intent policies.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}/PrepareNetworkPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetsUnprepareNetworkPolicies(resourceGroupName, virtualNetworkName, subnetName, unprepareNetworkPoliciesRequestParameters, callback)</td>
    <td style="padding:15px">Unprepares a subnet by removing network intent policies.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}/UnprepareNetworkPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourceNavigationLinksList(resourceGroupName, virtualNetworkName, subnetName, callback)</td>
    <td style="padding:15px">Gets a list of resource navigation links for a subnet.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}/ResourceNavigationLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceAssociationLinksList(resourceGroupName, virtualNetworkName, subnetName, callback)</td>
    <td style="padding:15px">Gets a list of service association links for a subnet.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}/ServiceAssociationLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksCheckIPAddressAvailability(resourceGroupName, virtualNetworkName, ipAddress, callback)</td>
    <td style="padding:15px">Checks whether a private IP address is available for use.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/CheckIPAddressAvailability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksListUsage(resourceGroupName, virtualNetworkName, callback)</td>
    <td style="padding:15px">Lists usage stats.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/usages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualWansGet(resourceGroupName, virtualWANName, callback)</td>
    <td style="padding:15px">Retrieves the details of a VirtualWAN.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualWansCreateOrUpdate(resourceGroupName, virtualWANName, wANParameters, callback)</td>
    <td style="padding:15px">Creates a VirtualWAN resource if it doesn't exist else updates the existing VirtualWAN.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualWansDelete(resourceGroupName, virtualWANName, callback)</td>
    <td style="padding:15px">Deletes a VirtualWAN.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualWansListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all the VirtualWANs in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualWansList(callback)</td>
    <td style="padding:15px">Lists all the VirtualWANs in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/virtualWans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesGet(resourceGroupName, vpnSiteName, callback)</td>
    <td style="padding:15px">Retrieves the details of a VPN site.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesCreateOrUpdate(resourceGroupName, vpnSiteName, vpnSiteParameters, callback)</td>
    <td style="padding:15px">Creates a VpnSite resource if it doesn't exist else updates the existing VpnSite.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesDelete(resourceGroupName, vpnSiteName, callback)</td>
    <td style="padding:15px">Deletes a VpnSite.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all the vpnSites in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSiteLinksGet(resourceGroupName, vpnSiteName, vpnSiteLinkName, callback)</td>
    <td style="padding:15px">Retrieves the details of a VPN site link.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites/{pathv3}/vpnSiteLinks/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSiteLinksListByVpnSite(resourceGroupName, vpnSiteName, callback)</td>
    <td style="padding:15px">Lists all the vpnSiteLinks in a resource group for a vpn site.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites/{pathv3}/vpnSiteLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesList(callback)</td>
    <td style="padding:15px">Lists all the VpnSites in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/vpnSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesConfigurationDownload(resourceGroupName, virtualWANName, request, callback)</td>
    <td style="padding:15px">Gives the sas-url to download the configurations for vpn-sites in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}/vpnConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">supportedSecurityProviders(resourceGroupName, virtualWANName, callback)</td>
    <td style="padding:15px">Gives the supported security providers for the virtual wan.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}/supportedSecurityProviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsGet(resourceGroupName, vpnServerConfigurationName, callback)</td>
    <td style="padding:15px">Retrieves the details of a VpnServerConfiguration.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnServerConfigurations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsCreateOrUpdate(resourceGroupName, vpnServerConfigurationName, vpnServerConfigurationParameters, callback)</td>
    <td style="padding:15px">Creates a VpnServerConfiguration resource if it doesn't exist else updates the existing VpnServerConfiguration.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnServerConfigurations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsDelete(resourceGroupName, vpnServerConfigurationName, callback)</td>
    <td style="padding:15px">Deletes a VpnServerConfiguration.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnServerConfigurations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all the vpnServerConfigurations in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnServerConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsList(callback)</td>
    <td style="padding:15px">Lists all the VpnServerConfigurations in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/vpnServerConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubsGet(resourceGroupName, virtualHubName, callback)</td>
    <td style="padding:15px">Retrieves the details of a VirtualHub.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubsCreateOrUpdate(resourceGroupName, virtualHubName, virtualHubParameters, callback)</td>
    <td style="padding:15px">Creates a VirtualHub resource if it doesn't exist else updates the existing VirtualHub.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubsDelete(resourceGroupName, virtualHubName, callback)</td>
    <td style="padding:15px">Deletes a VirtualHub.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all the VirtualHubs in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubsList(callback)</td>
    <td style="padding:15px">Lists all the VirtualHubs in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/virtualHubs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hubVirtualNetworkConnectionsGet(resourceGroupName, virtualHubName, connectionName, callback)</td>
    <td style="padding:15px">Retrieves the details of a HubVirtualNetworkConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}/hubVirtualNetworkConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hubVirtualNetworkConnectionsList(resourceGroupName, virtualHubName, callback)</td>
    <td style="padding:15px">Retrieves the details of all HubVirtualNetworkConnections.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}/hubVirtualNetworkConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysGet(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Retrieves the details of a virtual wan vpn gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysCreateOrUpdate(resourceGroupName, gatewayName, vpnGatewayParameters, callback)</td>
    <td style="padding:15px">Creates a virtual wan vpn gateway if it doesn't exist else updates the existing gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysDelete(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Deletes a virtual wan vpn gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all the VpnGateways in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysList(callback)</td>
    <td style="padding:15px">Lists all the VpnGateways in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/vpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnConnectionsGet(resourceGroupName, gatewayName, connectionName, callback)</td>
    <td style="padding:15px">Retrieves the details of a vpn connection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/vpnConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnConnectionsCreateOrUpdate(resourceGroupName, gatewayName, connectionName, vpnConnectionParameters, callback)</td>
    <td style="padding:15px">Creates a vpn connection to a scalable vpn gateway if it doesn't exist else updates the existing connection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/vpnConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnConnectionsDelete(resourceGroupName, gatewayName, connectionName, callback)</td>
    <td style="padding:15px">Deletes a vpn connection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/vpnConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSiteLinkConnectionsGet(resourceGroupName, gatewayName, connectionName, linkConnectionName, callback)</td>
    <td style="padding:15px">Retrieves the details of a vpn site link connection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/vpnConnections/{pathv4}/vpnLinkConnections/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnConnectionsListByVpnGateway(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Retrieves all vpn connections for a particular virtual wan vpn gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/vpnConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnLinkConnectionsListByVpnConnection(resourceGroupName, gatewayName, connectionName, callback)</td>
    <td style="padding:15px">Retrieves all vpn site link connections for a particular virtual wan vpn gateway vpn connection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/vpnConnections/{pathv4}/vpnLinkConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysGet(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Retrieves the details of a virtual wan p2s vpn gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysCreateOrUpdate(resourceGroupName, gatewayName, p2SVpnGatewayParameters, callback)</td>
    <td style="padding:15px">Creates a virtual wan p2s vpn gateway if it doesn't exist else updates the existing gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysDelete(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Deletes a virtual wan p2s vpn gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all the P2SVpnGateways in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysList(callback)</td>
    <td style="padding:15px">Lists all the P2SVpnGateways in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/p2svpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsAssociatedWithVirtualWanList(resourceGroupName, virtualWANName, callback)</td>
    <td style="padding:15px">Gives the list of VpnServerConfigurations associated with Virtual Wan in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}/vpnServerConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generatevirtualwanvpnserverconfigurationvpnprofile(resourceGroupName, virtualWANName, vpnClientParams, callback)</td>
    <td style="padding:15px">Generates a unique VPN profile for P2S clients for VirtualWan and associated VpnServerConfiguration combination in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}/GenerateVpnProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubRouteTableV2sGet(resourceGroupName, virtualHubName, routeTableName, callback)</td>
    <td style="padding:15px">Retrieves the details of a VirtualHubRouteTableV2.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}/routeTables/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubRouteTableV2sCreateOrUpdate(resourceGroupName, virtualHubName, routeTableName, virtualHubRouteTableV2Parameters, callback)</td>
    <td style="padding:15px">Creates a VirtualHubRouteTableV2 resource if it doesn't exist else updates the existing VirtualHubRouteTableV2.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}/routeTables/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubRouteTableV2sDelete(resourceGroupName, virtualHubName, routeTableName, callback)</td>
    <td style="padding:15px">Deletes a VirtualHubRouteTableV2.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}/routeTables/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubRouteTableV2sList(resourceGroupName, virtualHubName, callback)</td>
    <td style="padding:15px">Retrieves the details of all VirtualHubRouteTableV2s.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}/routeTables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesListVirtualMachineScaleSetPublicIPAddresses(resourceGroupName, virtualMachineScaleSetName, callback)</td>
    <td style="padding:15px">Gets information about all public IP addresses on a virtual machine scale set level.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachineScaleSets/{pathv3}/publicipaddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesListVirtualMachineScaleSetVMPublicIPAddresses(resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, networkInterfaceName, ipConfigurationName, callback)</td>
    <td style="padding:15px">Gets information about all public IP addresses in a virtual machine IP configuration in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachineScaleSets/{pathv3}/virtualMachines/{pathv4}/networkInterfaces/{pathv5}/ipconfigurations/{pathv6}/publicipaddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesGetVirtualMachineScaleSetPublicIPAddress(resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, networkInterfaceName, ipConfigurationName, publicIpAddressName, expand, callback)</td>
    <td style="padding:15px">Get the specified public IP address in a virtual machine scale set.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachineScaleSets/{pathv3}/virtualMachines/{pathv4}/networkInterfaces/{pathv5}/ipconfigurations/{pathv6}/publicipaddresses/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availablePrivateEndpointTypesList(location, callback)</td>
    <td style="padding:15px">Returns all of the resource types that can be linked to a Private Endpoint in this subscription in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/availablePrivateEndpointTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availablePrivateEndpointTypesListByResourceGroup(location, resourceGroupName, callback)</td>
    <td style="padding:15px">Returns all of the resource types that can be linked to a Private Endpoint in this subscription in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/locations/{pathv3}/availablePrivateEndpointTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualWansUpdateTags(resourceGroupName, virtualWANName, wANParameters, callback)</td>
    <td style="padding:15px">Updates a VirtualWAN tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualWans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualHubsUpdateTags(resourceGroupName, virtualHubName, virtualHubParameters, callback)</td>
    <td style="padding:15px">Updates VirtualHub tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualHubs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnSitesUpdateTags(resourceGroupName, vpnSiteName, vpnSiteParameters, callback)</td>
    <td style="padding:15px">Updates VpnSite tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnSites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnServerConfigurationsUpdateTags(resourceGroupName, vpnServerConfigurationName, vpnServerConfigurationParameters, callback)</td>
    <td style="padding:15px">Updates VpnServerConfiguration tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnServerConfigurations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysUpdateTags(resourceGroupName, gatewayName, vpnGatewayParameters, callback)</td>
    <td style="padding:15px">Updates virtual wan vpn gateway tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGatewaysReset(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Resets the primary of the vpn gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/vpnGateways/{pathv3}/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysUpdateTags(resourceGroupName, gatewayName, p2SVpnGatewayParameters, callback)</td>
    <td style="padding:15px">Updates virtual wan p2s vpn gateway tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysGenerateVpnProfile(resourceGroupName, gatewayName, parameters, callback)</td>
    <td style="padding:15px">Generates VPN profile for P2S client of the P2SVpnGateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}/generatevpnprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysGetP2sVpnConnectionHealth(resourceGroupName, gatewayName, callback)</td>
    <td style="padding:15px">Gets the connection health of P2S clients of the virtual wan P2SVpnGateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}/getP2sVpnConnectionHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysGetP2sVpnConnectionHealthDetailed(resourceGroupName, gatewayName, request, callback)</td>
    <td style="padding:15px">Gets the sas url to get the connection health detail of P2S clients of the virtual wan P2SVpnGateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}/getP2sVpnConnectionHealthDetailed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">p2sVpnGatewaysDisconnectP2sVpnConnections(resourceGroupName, p2sVpnGatewayName, request, callback)</td>
    <td style="padding:15px">Disconnect P2S vpn connections of the virtual wan P2SVpnGateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/p2svpnGateways/{pathv3}/disconnectP2sVpnConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usagesList(location, callback)</td>
    <td style="padding:15px">List network usages for a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/usages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosProtectionPlansDelete(resourceGroupName, ddosProtectionPlanName, callback)</td>
    <td style="padding:15px">Deletes the specified DDoS protection plan.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosProtectionPlans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosProtectionPlansGet(resourceGroupName, ddosProtectionPlanName, callback)</td>
    <td style="padding:15px">Gets information about the specified DDoS protection plan.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosProtectionPlans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosProtectionPlansCreateOrUpdate(resourceGroupName, ddosProtectionPlanName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a DDoS protection plan.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosProtectionPlans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosProtectionPlansUpdateTags(resourceGroupName, ddosProtectionPlanName, parameters, callback)</td>
    <td style="padding:15px">Update a DDoS protection plan tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosProtectionPlans/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosProtectionPlansList(callback)</td>
    <td style="padding:15px">Gets all DDoS protection plans in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ddosProtectionPlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosProtectionPlansListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all the DDoS protection plans in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosProtectionPlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitAuthorizationsDelete(resourceGroupName, circuitName, authorizationName, callback)</td>
    <td style="padding:15px">Deletes the specified authorization from the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/authorizations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitAuthorizationsGet(resourceGroupName, circuitName, authorizationName, callback)</td>
    <td style="padding:15px">Gets the specified authorization from the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/authorizations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitAuthorizationsCreateOrUpdate(resourceGroupName, circuitName, authorizationName, authorizationParameters, callback)</td>
    <td style="padding:15px">Creates or updates an authorization in the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/authorizations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitAuthorizationsList(resourceGroupName, circuitName, callback)</td>
    <td style="padding:15px">Gets all authorizations in an express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/authorizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitPeeringsDelete(resourceGroupName, circuitName, peeringName, callback)</td>
    <td style="padding:15px">Deletes the specified peering from the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitPeeringsGet(resourceGroupName, circuitName, peeringName, callback)</td>
    <td style="padding:15px">Gets the specified peering for the express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitPeeringsCreateOrUpdate(resourceGroupName, circuitName, peeringName, peeringParameters, callback)</td>
    <td style="padding:15px">Creates or updates a peering in the specified express route circuits.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitPeeringsList(resourceGroupName, circuitName, callback)</td>
    <td style="padding:15px">Gets all peerings in a specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitConnectionsDelete(resourceGroupName, circuitName, peeringName, connectionName, callback)</td>
    <td style="padding:15px">Deletes the specified Express Route Circuit Connection from the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/connections/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitConnectionsGet(resourceGroupName, circuitName, peeringName, connectionName, callback)</td>
    <td style="padding:15px">Gets the specified Express Route Circuit Connection from the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/connections/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitConnectionsCreateOrUpdate(resourceGroupName, circuitName, peeringName, connectionName, expressRouteCircuitConnectionParameters, callback)</td>
    <td style="padding:15px">Creates or updates a Express Route Circuit Connection in the specified express route circuits.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/connections/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitConnectionsList(resourceGroupName, circuitName, peeringName, callback)</td>
    <td style="padding:15px">Gets all global reach connections associated with a private peering in an express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peerExpressRouteCircuitConnectionsGet(resourceGroupName, circuitName, peeringName, connectionName, callback)</td>
    <td style="padding:15px">Gets the specified Peer Express Route Circuit Connection from the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/peerConnections/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peerExpressRouteCircuitConnectionsList(resourceGroupName, circuitName, peeringName, callback)</td>
    <td style="padding:15px">Gets all global reach peer connections associated with a private peering in an express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/peerConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsDelete(resourceGroupName, circuitName, callback)</td>
    <td style="padding:15px">Deletes the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsGet(resourceGroupName, circuitName, callback)</td>
    <td style="padding:15px">Gets information about the specified express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsCreateOrUpdate(resourceGroupName, circuitName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates an express route circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsUpdateTags(resourceGroupName, circuitName, parameters, callback)</td>
    <td style="padding:15px">Updates an express route circuit tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all the express route circuits in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsListAll(callback)</td>
    <td style="padding:15px">Gets all the express route circuits in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/expressRouteCircuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsListArpTable(resourceGroupName, circuitName, peeringName, devicePath, callback)</td>
    <td style="padding:15px">Gets the currently advertised ARP table associated with the express route circuit in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/arpTables/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsListRoutesTable(resourceGroupName, circuitName, peeringName, devicePath, callback)</td>
    <td style="padding:15px">Gets the currently advertised routes table associated with the express route circuit in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/routeTables/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsListRoutesTableSummary(resourceGroupName, circuitName, peeringName, devicePath, callback)</td>
    <td style="padding:15px">Gets the currently advertised routes table summary associated with the express route circuit in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/routeTablesSummary/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsGetStats(resourceGroupName, circuitName, callback)</td>
    <td style="padding:15px">Gets all the stats from an express route circuit in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCircuitsGetPeeringStats(resourceGroupName, circuitName, peeringName, callback)</td>
    <td style="padding:15px">Gets all stats from an express route circuit in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCircuits/{pathv3}/peerings/{pathv4}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteServiceProvidersList(callback)</td>
    <td style="padding:15px">Gets all the available express route service providers.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/expressRouteServiceProviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeTablesDelete(resourceGroupName, routeTableName, callback)</td>
    <td style="padding:15px">Deletes the specified route table.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeTablesGet(resourceGroupName, routeTableName, expand, callback)</td>
    <td style="padding:15px">Gets the specified route table.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeTablesCreateOrUpdate(resourceGroupName, routeTableName, parameters, callback)</td>
    <td style="padding:15px">Create or updates a route table in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeTablesUpdateTags(resourceGroupName, routeTableName, parameters, callback)</td>
    <td style="padding:15px">Updates a route table tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeTablesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all route tables in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeTablesListAll(callback)</td>
    <td style="padding:15px">Gets all route tables in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/routeTables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routesDelete(resourceGroupName, routeTableName, routeName, callback)</td>
    <td style="padding:15px">Deletes the specified route from a route table.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}/routes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routesGet(resourceGroupName, routeTableName, routeName, callback)</td>
    <td style="padding:15px">Gets the specified route from a route table.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}/routes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routesCreateOrUpdate(resourceGroupName, routeTableName, routeName, routeParameters, callback)</td>
    <td style="padding:15px">Creates or updates a route in the specified route table.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}/routes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routesList(resourceGroupName, routeTableName, callback)</td>
    <td style="padding:15px">Gets all routes in a route table.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeTables/{pathv3}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosCustomPoliciesDelete(resourceGroupName, ddosCustomPolicyName, callback)</td>
    <td style="padding:15px">Deletes the specified DDoS custom policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosCustomPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosCustomPoliciesGet(resourceGroupName, ddosCustomPolicyName, callback)</td>
    <td style="padding:15px">Gets information about the specified DDoS custom policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosCustomPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosCustomPoliciesCreateOrUpdate(resourceGroupName, ddosCustomPolicyName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a DDoS custom policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosCustomPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ddosCustomPoliciesUpdateTags(resourceGroupName, ddosCustomPolicyName, parameters, callback)</td>
    <td style="padding:15px">Update a DDoS custom policy tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ddosCustomPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsList(callback)</td>
    <td style="padding:15px">Retrieves all the ExpressRouteCrossConnections in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/expressRouteCrossConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Retrieves all the ExpressRouteCrossConnections in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsGet(resourceGroupName, crossConnectionName, callback)</td>
    <td style="padding:15px">Gets details about the specified ExpressRouteCrossConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsCreateOrUpdate(resourceGroupName, crossConnectionName, parameters, callback)</td>
    <td style="padding:15px">Update the specified ExpressRouteCrossConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsUpdateTags(resourceGroupName, crossConnectionName, crossConnectionParameters, callback)</td>
    <td style="padding:15px">Updates an express route cross connection tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionPeeringsList(resourceGroupName, crossConnectionName, callback)</td>
    <td style="padding:15px">Gets all peerings in a specified ExpressRouteCrossConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionPeeringsDelete(resourceGroupName, crossConnectionName, peeringName, callback)</td>
    <td style="padding:15px">Deletes the specified peering from the ExpressRouteCrossConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionPeeringsGet(resourceGroupName, crossConnectionName, peeringName, callback)</td>
    <td style="padding:15px">Gets the specified peering for the ExpressRouteCrossConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionPeeringsCreateOrUpdate(resourceGroupName, crossConnectionName, peeringName, peeringParameters, callback)</td>
    <td style="padding:15px">Creates or updates a peering in the specified ExpressRouteCrossConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsListArpTable(resourceGroupName, crossConnectionName, peeringName, devicePath, callback)</td>
    <td style="padding:15px">Gets the currently advertised ARP table associated with the express route cross connection in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings/{pathv4}/arpTables/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsListRoutesTableSummary(resourceGroupName, crossConnectionName, peeringName, devicePath, callback)</td>
    <td style="padding:15px">Gets the route table summary associated with the express route cross connection in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings/{pathv4}/routeTablesSummary/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteCrossConnectionsListRoutesTable(resourceGroupName, crossConnectionName, peeringName, devicePath, callback)</td>
    <td style="padding:15px">Gets the currently advertised routes table associated with the express route cross connection in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteCrossConnections/{pathv3}/peerings/{pathv4}/routeTables/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesDelete(resourceGroupName, publicIpAddressName, callback)</td>
    <td style="padding:15px">Deletes the specified public IP address.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPAddresses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesGet(resourceGroupName, publicIpAddressName, expand, callback)</td>
    <td style="padding:15px">Gets the specified public IP address in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPAddresses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesCreateOrUpdate(resourceGroupName, publicIpAddressName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a static or dynamic public IP address.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPAddresses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesUpdateTags(resourceGroupName, publicIpAddressName, parameters, callback)</td>
    <td style="padding:15px">Updates public IP address tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPAddresses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesListAll(callback)</td>
    <td style="padding:15px">Gets all the public IP addresses in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/publicIPAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPAddressesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all public IP addresses in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysCreateOrUpdate(resourceGroupName, virtualNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGet(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Gets the specified virtual network gateway by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysDelete(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Deletes the specified virtual network gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysUpdateTags(resourceGroupName, virtualNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Updates a virtual network gateway tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all virtual network gateways by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysListConnections(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Gets all the connections in a virtual network gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysReset(resourceGroupName, virtualNetworkGatewayName, gatewayVip, callback)</td>
    <td style="padding:15px">Resets the primary of the virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysResetVpnClientSharedKey(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Resets the VPN client shared key of the virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/resetvpnclientsharedkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGeneratevpnclientpackage(resourceGroupName, virtualNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Generates VPN client package for P2S client of the virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/generatevpnclientpackage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGenerateVpnProfile(resourceGroupName, virtualNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Generates VPN profile for P2S client of the virtual network gateway in the specified resource group. Used for IKEV2 and radius based authentication.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/generatevpnprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGetVpnProfilePackageUrl(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Gets pre-generated VPN profile for P2S client of the virtual network gateway in the specified resource group. The profile needs to be generated first using generateVpnProfile.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/getvpnprofilepackageurl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGetBgpPeerStatus(resourceGroupName, virtualNetworkGatewayName, peer, callback)</td>
    <td style="padding:15px">The GetBgpPeerStatus operation retrieves the status of all BGP peers.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/getBgpPeerStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysSupportedVpnDevices(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Gets a xml format representation for supported vpn devices.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/supportedvpndevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGetLearnedRoutes(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">This operation retrieves a list of routes the virtual network gateway has learned, including routes learned from BGP peers.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/getLearnedRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGetAdvertisedRoutes(resourceGroupName, virtualNetworkGatewayName, peer, callback)</td>
    <td style="padding:15px">This operation retrieves a list of routes the virtual network gateway is advertising to the specified peer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/getAdvertisedRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysSetVpnclientIpsecParameters(resourceGroupName, virtualNetworkGatewayName, vpnclientIpsecParams, callback)</td>
    <td style="padding:15px">The Set VpnclientIpsecParameters operation sets the vpnclient ipsec policy for P2S client of virtual network gateway in the specified resource group through Network resource provider.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/setvpnclientipsecparameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGetVpnclientIpsecParameters(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">The Get VpnclientIpsecParameters operation retrieves information about the vpnclient ipsec policy for P2S client of virtual network gateway in the specified resource group through Network resource provider.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/getvpnclientipsecparameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysVpnDeviceConfigurationScript(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">Gets a xml format representation for vpn device configuration script.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}/vpndeviceconfigurationscript?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysStartPacketCapture(resourceGroupName, virtualNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Starts packet capture on virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/startPacketCapture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysStopPacketCapture(resourceGroupName, virtualNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Stops packet capture on virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/stopPacketCapture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysGetVpnclientConnectionHealth(resourceGroupName, virtualNetworkGatewayName, callback)</td>
    <td style="padding:15px">Get VPN client connection health detail per P2S client connection of the virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/getVpnClientConnectionHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewaysDisconnectVirtualNetworkGatewayVpnConnections(resourceGroupName, virtualNetworkGatewayName, request, callback)</td>
    <td style="padding:15px">Disconnect vpn connections of virtual network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkGateways/{pathv3}/disconnectVirtualNetworkGatewayVpnConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsCreateOrUpdate(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a virtual network gateway connection in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsGet(resourceGroupName, virtualNetworkGatewayConnectionName, callback)</td>
    <td style="padding:15px">Gets the specified virtual network gateway connection by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsDelete(resourceGroupName, virtualNetworkGatewayConnectionName, callback)</td>
    <td style="padding:15px">Deletes the specified virtual network Gateway connection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsUpdateTags(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">Updates a virtual network gateway connection tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsSetSharedKey(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">The Put VirtualNetworkGatewayConnectionSharedKey operation sets the virtual network gateway connection shared key for passed virtual network gateway connection in the specified resource group through Network resource provider.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}/sharedkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsGetSharedKey(resourceGroupName, virtualNetworkGatewayConnectionName, callback)</td>
    <td style="padding:15px">The Get VirtualNetworkGatewayConnectionSharedKey operation retrieves information about the specified virtual network gateway connection shared key through Network resource provider.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}/sharedkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsList(resourceGroupName, callback)</td>
    <td style="padding:15px">The List VirtualNetworkGatewayConnections operation retrieves all the virtual network gateways connections created.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsResetSharedKey(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">The VirtualNetworkGatewayConnectionResetSharedKey operation resets the virtual network gateway connection shared key for passed virtual network gateway connection in the specified resource group through Network resource provider.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}/sharedkey/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsStartPacketCapture(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">Starts packet capture on virtual network gateway connection in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}/startPacketCapture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkGatewayConnectionsStopPacketCapture(resourceGroupName, virtualNetworkGatewayConnectionName, parameters, callback)</td>
    <td style="padding:15px">Stops packet capture on virtual network gateway connection in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/connections/{pathv3}/stopPacketCapture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">localNetworkGatewaysCreateOrUpdate(resourceGroupName, localNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a local network gateway in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/localNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">localNetworkGatewaysGet(resourceGroupName, localNetworkGatewayName, callback)</td>
    <td style="padding:15px">Gets the specified local network gateway in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/localNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">localNetworkGatewaysDelete(resourceGroupName, localNetworkGatewayName, callback)</td>
    <td style="padding:15px">Deletes the specified local network gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/localNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">localNetworkGatewaysUpdateTags(resourceGroupName, localNetworkGatewayName, parameters, callback)</td>
    <td style="padding:15px">Updates a local network gateway tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/localNetworkGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">localNetworkGatewaysList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all the local network gateways in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/localNetworkGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteGatewaysListBySubscription(callback)</td>
    <td style="padding:15px">Lists ExpressRoute gateways under a given subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/expressRouteGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteGatewaysListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists ExpressRoute gateways in a given resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteGatewaysCreateOrUpdate(resourceGroupName, expressRouteGatewayName, putExpressRouteGatewayParameters, callback)</td>
    <td style="padding:15px">Creates or updates a ExpressRoute gateway in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteGatewaysGet(resourceGroupName, expressRouteGatewayName, callback)</td>
    <td style="padding:15px">Fetches the details of a ExpressRoute gateway in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteGatewaysDelete(resourceGroupName, expressRouteGatewayName, callback)</td>
    <td style="padding:15px">Deletes the specified ExpressRoute gateway in a resource group. An ExpressRoute gateway resource can only be deleted when there are no connection subresources.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteConnectionsCreateOrUpdate(resourceGroupName, expressRouteGatewayName, connectionName, putExpressRouteConnectionParameters, callback)</td>
    <td style="padding:15px">Creates a connection between an ExpressRoute gateway and an ExpressRoute circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}/expressRouteConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteConnectionsGet(resourceGroupName, expressRouteGatewayName, connectionName, callback)</td>
    <td style="padding:15px">Gets the specified ExpressRouteConnection.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}/expressRouteConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteConnectionsDelete(resourceGroupName, expressRouteGatewayName, connectionName, callback)</td>
    <td style="padding:15px">Deletes a connection to a ExpressRoute circuit.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}/expressRouteConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteConnectionsList(resourceGroupName, expressRouteGatewayName, callback)</td>
    <td style="padding:15px">Lists ExpressRouteConnections.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/expressRouteGateways/{pathv3}/expressRouteConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipGroupsGet(resourceGroupName, ipGroupsName, expand, callback)</td>
    <td style="padding:15px">Gets the specified ipGroups.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ipGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipGroupsCreateOrUpdate(resourceGroupName, ipGroupsName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates an ipGroups in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ipGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipGroupsUpdateGroups(resourceGroupName, ipGroupsName, parameters, callback)</td>
    <td style="padding:15px">Updates tags of an IpGroups resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ipGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipGroupsDelete(resourceGroupName, ipGroupsName, callback)</td>
    <td style="padding:15px">Deletes the specified ipGroups.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ipGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipGroupsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all IpGroups in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ipGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipGroupsList(callback)</td>
    <td style="padding:15px">Gets all IpGroups in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ipGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webApplicationFirewallPoliciesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all of the protection policies within a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webApplicationFirewallPoliciesListAll(callback)</td>
    <td style="padding:15px">Gets all the WAF policies in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webApplicationFirewallPoliciesGet(resourceGroupName, policyName, callback)</td>
    <td style="padding:15px">Retrieve protection policy with specified name within a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webApplicationFirewallPoliciesCreateOrUpdate(resourceGroupName, policyName, parameters, callback)</td>
    <td style="padding:15px">Creates or update policy with specified rule set name within a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webApplicationFirewallPoliciesDelete(resourceGroupName, policyName, callback)</td>
    <td style="padding:15px">Deletes Policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkSecurityGroupsDelete(resourceGroupName, networkSecurityGroupName, callback)</td>
    <td style="padding:15px">Deletes the specified network security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkSecurityGroupsGet(resourceGroupName, networkSecurityGroupName, expand, callback)</td>
    <td style="padding:15px">Gets the specified network security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkSecurityGroupsCreateOrUpdate(resourceGroupName, networkSecurityGroupName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a network security group in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkSecurityGroupsUpdateTags(resourceGroupName, networkSecurityGroupName, parameters, callback)</td>
    <td style="padding:15px">Updates a network security group tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkSecurityGroupsListAll(callback)</td>
    <td style="padding:15px">Gets all network security groups in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/networkSecurityGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkSecurityGroupsList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all network security groups in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityRulesDelete(resourceGroupName, networkSecurityGroupName, securityRuleName, callback)</td>
    <td style="padding:15px">Deletes the specified network security rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}/securityRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityRulesGet(resourceGroupName, networkSecurityGroupName, securityRuleName, callback)</td>
    <td style="padding:15px">Get the specified network security rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}/securityRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityRulesCreateOrUpdate(resourceGroupName, networkSecurityGroupName, securityRuleName, securityRuleParameters, callback)</td>
    <td style="padding:15px">Creates or updates a security rule in the specified network security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}/securityRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityRulesList(resourceGroupName, networkSecurityGroupName, callback)</td>
    <td style="padding:15px">Gets all security rules in a network security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}/securityRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">defaultSecurityRulesList(resourceGroupName, networkSecurityGroupName, callback)</td>
    <td style="padding:15px">Gets all default security rules in a network security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}/defaultSecurityRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">defaultSecurityRulesGet(resourceGroupName, networkSecurityGroupName, defaultSecurityRuleName, callback)</td>
    <td style="padding:15px">Get the specified default network security rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkSecurityGroups/{pathv3}/defaultSecurityRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkProfilesDelete(resourceGroupName, networkProfileName, callback)</td>
    <td style="padding:15px">Deletes the specified network profile.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkProfiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkProfilesGet(resourceGroupName, networkProfileName, expand, callback)</td>
    <td style="padding:15px">Gets the specified network profile in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkProfiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkProfilesCreateOrUpdate(resourceGroupName, networkProfileName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a network profile.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkProfiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkProfilesUpdateTags(resourceGroupName, networkProfileName, parameters, callback)</td>
    <td style="padding:15px">Updates network profile tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkProfiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkProfilesListAll(callback)</td>
    <td style="padding:15px">Gets all the network profiles in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/networkProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkProfilesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all network profiles in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallFqdnTagsListAll(callback)</td>
    <td style="padding:15px">Gets all the Azure Firewall FQDN Tags in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/azureFirewallFqdnTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">natGatewaysDelete(resourceGroupName, natGatewayName, callback)</td>
    <td style="padding:15px">Deletes the specified nat gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/natGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">natGatewaysGet(resourceGroupName, natGatewayName, expand, callback)</td>
    <td style="padding:15px">Gets the specified nat gateway in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/natGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">natGatewaysCreateOrUpdate(resourceGroupName, natGatewayName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a nat gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/natGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">natGatewaysUpdateTags(resourceGroupName, natGatewayName, parameters, callback)</td>
    <td style="padding:15px">Updates nat gateway tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/natGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">natGatewaysListAll(callback)</td>
    <td style="padding:15px">Gets all the Nat Gateways in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/natGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">natGatewaysList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all nat gateways in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/natGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksDelete(resourceGroupName, virtualNetworkName, callback)</td>
    <td style="padding:15px">Deletes the specified virtual network.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksGet(resourceGroupName, virtualNetworkName, expand, callback)</td>
    <td style="padding:15px">Gets the specified virtual network by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksCreateOrUpdate(resourceGroupName, virtualNetworkName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a virtual network in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployment(resourceGroupName, deploymentName, parameters, callback)</td>
    <td style="padding:15px">Creates deployment in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Resources/deployments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksUpdateTags(resourceGroupName, virtualNetworkName, parameters, callback)</td>
    <td style="padding:15px">Updates a virtual network tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksListAll(callback)</td>
    <td style="padding:15px">Gets all virtual networks in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/virtualNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworksList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all virtual networks in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetsDelete(resourceGroupName, virtualNetworkName, subnetName, callback)</td>
    <td style="padding:15px">Deletes the specified subnet.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetsGet(resourceGroupName, virtualNetworkName, subnetName, expand, callback)</td>
    <td style="padding:15px">Gets the specified subnet by virtual network and resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetsCreateOrUpdate(resourceGroupName, virtualNetworkName, subnetName, subnetParameters, callback)</td>
    <td style="padding:15px">Creates or updates a subnet in the specified virtual network.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetsList(resourceGroupName, virtualNetworkName, callback)</td>
    <td style="padding:15px">Gets all subnets in a virtual network.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkPeeringsDelete(resourceGroupName, virtualNetworkName, virtualNetworkPeeringName, callback)</td>
    <td style="padding:15px">Deletes the specified virtual network peering.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/virtualNetworkPeerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkPeeringsGet(resourceGroupName, virtualNetworkName, virtualNetworkPeeringName, callback)</td>
    <td style="padding:15px">Gets the specified virtual network peering.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/virtualNetworkPeerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkPeeringsCreateOrUpdate(resourceGroupName, virtualNetworkName, virtualNetworkPeeringName, virtualNetworkPeeringParameters, callback)</td>
    <td style="padding:15px">Creates or updates a peering in the specified virtual network.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/virtualNetworkPeerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkPeeringsList(resourceGroupName, virtualNetworkName, callback)</td>
    <td style="padding:15px">Gets all virtual network peerings in a virtual network.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworks/{pathv3}/virtualNetworkPeerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPoliciesDelete(resourceGroupName, firewallPolicyName, callback)</td>
    <td style="padding:15px">Deletes the specified Firewall Policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPoliciesGet(resourceGroupName, firewallPolicyName, expand, callback)</td>
    <td style="padding:15px">Gets the specified Firewall Policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPoliciesCreateOrUpdate(resourceGroupName, firewallPolicyName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified Firewall Policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPoliciesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all Firewall Policies in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPoliciesListAll(callback)</td>
    <td style="padding:15px">Gets all the Firewall Policies in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/firewallPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPolicyRuleGroupsDelete(resourceGroupName, firewallPolicyName, ruleGroupName, callback)</td>
    <td style="padding:15px">Deletes the specified FirewallPolicyRuleGroup.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}/ruleGroups/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPolicyRuleGroupsGet(resourceGroupName, firewallPolicyName, ruleGroupName, callback)</td>
    <td style="padding:15px">Gets the specified FirewallPolicyRuleGroup.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}/ruleGroups/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPolicyRuleGroupsCreateOrUpdate(resourceGroupName, firewallPolicyName, ruleGroupName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified FirewallPolicyRuleGroup.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}/ruleGroups/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallPolicyRuleGroupsList(resourceGroupName, firewallPolicyName, callback)</td>
    <td style="padding:15px">Lists all FirewallPolicyRuleGroups in a FirewallPolicy resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/firewallPolicies/{pathv3}/ruleGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancersDelete(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Deletes the specified load balancer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancersGet(resourceGroupName, loadBalancerName, expand, callback)</td>
    <td style="padding:15px">Gets the specified load balancer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancersCreateOrUpdate(resourceGroupName, loadBalancerName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a load balancer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancersUpdateTags(resourceGroupName, loadBalancerName, parameters, callback)</td>
    <td style="padding:15px">Updates a load balancer tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancersListAll(callback)</td>
    <td style="padding:15px">Gets all the load balancers in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/loadBalancers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancersList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all the load balancers in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerBackendAddressPoolsList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets all the load balancer backed address pools.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/backendAddressPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerBackendAddressPoolsGet(resourceGroupName, loadBalancerName, backendAddressPoolName, callback)</td>
    <td style="padding:15px">Gets load balancer backend address pool.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/backendAddressPools/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerFrontendIPConfigurationsList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets all the load balancer frontend IP configurations.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/frontendIPConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerFrontendIPConfigurationsGet(resourceGroupName, loadBalancerName, frontendIPConfigurationName, callback)</td>
    <td style="padding:15px">Gets load balancer frontend IP configuration.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/frontendIPConfigurations/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboundNatRulesList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets all the inbound nat rules in a load balancer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/inboundNatRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboundNatRulesDelete(resourceGroupName, loadBalancerName, inboundNatRuleName, callback)</td>
    <td style="padding:15px">Deletes the specified load balancer inbound nat rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/inboundNatRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboundNatRulesGet(resourceGroupName, loadBalancerName, inboundNatRuleName, expand, callback)</td>
    <td style="padding:15px">Gets the specified load balancer inbound nat rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/inboundNatRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboundNatRulesCreateOrUpdate(resourceGroupName, loadBalancerName, inboundNatRuleName, inboundNatRuleParameters, callback)</td>
    <td style="padding:15px">Creates or updates a load balancer inbound nat rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/inboundNatRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerLoadBalancingRulesList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets all the load balancing rules in a load balancer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/loadBalancingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerLoadBalancingRulesGet(resourceGroupName, loadBalancerName, loadBalancingRuleName, callback)</td>
    <td style="padding:15px">Gets the specified load balancer load balancing rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/loadBalancingRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerOutboundRulesList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets all the outbound rules in a load balancer.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/outboundRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerOutboundRulesGet(resourceGroupName, loadBalancerName, outboundRuleName, callback)</td>
    <td style="padding:15px">Gets the specified load balancer outbound rule.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/outboundRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerNetworkInterfacesList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets associated load balancer network interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/networkInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerProbesList(resourceGroupName, loadBalancerName, callback)</td>
    <td style="padding:15px">Gets all the load balancer probes.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/probes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadBalancerProbesGet(resourceGroupName, loadBalancerName, probeName, callback)</td>
    <td style="padding:15px">Gets load balancer probe.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/loadBalancers/{pathv3}/probes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPPrefixesDelete(resourceGroupName, publicIpPrefixName, callback)</td>
    <td style="padding:15px">Deletes the specified public IP prefix.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPPrefixes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPPrefixesGet(resourceGroupName, publicIpPrefixName, expand, callback)</td>
    <td style="padding:15px">Gets the specified public IP prefix in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPPrefixes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPPrefixesCreateOrUpdate(resourceGroupName, publicIpPrefixName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a static or dynamic public IP prefix.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPPrefixes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPPrefixesUpdateTags(resourceGroupName, publicIpPrefixName, parameters, callback)</td>
    <td style="padding:15px">Updates public IP prefix tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPPrefixes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPPrefixesListAll(callback)</td>
    <td style="padding:15px">Gets all the public IP prefixes in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/publicIPPrefixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publicIPPrefixesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all public IP prefixes in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/publicIPPrefixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operationsList(callback)</td>
    <td style="padding:15px">Lists all of the available Network Rest API operations.</td>
    <td style="padding:15px">{base_path}/{version}/providers/Microsoft.Network/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bgpServiceCommunitiesList(callback)</td>
    <td style="padding:15px">Gets all the available bgp service communities.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/bgpServiceCommunities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysDelete(resourceGroupName, applicationGatewayName, callback)</td>
    <td style="padding:15px">Deletes the specified application gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysGet(resourceGroupName, applicationGatewayName, callback)</td>
    <td style="padding:15px">Gets the specified application gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysCreateOrUpdate(resourceGroupName, applicationGatewayName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified application gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysUpdateTags(resourceGroupName, applicationGatewayName, parameters, callback)</td>
    <td style="padding:15px">Updates the specified application gateway tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysList(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all application gateways in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAll(callback)</td>
    <td style="padding:15px">Gets all the application gateways in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysStart(resourceGroupName, applicationGatewayName, callback)</td>
    <td style="padding:15px">Starts the specified application gateway.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysStop(resourceGroupName, applicationGatewayName, callback)</td>
    <td style="padding:15px">Stops the specified application gateway in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysBackendHealth(resourceGroupName, applicationGatewayName, expand, callback)</td>
    <td style="padding:15px">Gets the backend health of the specified application gateway in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}/backendhealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysBackendHealthOnDemand(resourceGroupName, applicationGatewayName, probeRequest, expand, callback)</td>
    <td style="padding:15px">Gets the backend health for given combination of backend pool and http setting of the specified application gateway in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationGateways/{pathv3}/getBackendHealthOnDemand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAvailableServerVariables(callback)</td>
    <td style="padding:15px">Lists all available server variables.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableServerVariables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAvailableRequestHeaders(callback)</td>
    <td style="padding:15px">Lists all available request headers.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableRequestHeaders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAvailableResponseHeaders(callback)</td>
    <td style="padding:15px">Lists all available response headers.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableResponseHeaders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAvailableWafRuleSets(callback)</td>
    <td style="padding:15px">Lists all available web application firewall rule sets.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableWafRuleSets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAvailableSslOptions(callback)</td>
    <td style="padding:15px">Lists available Ssl options for configuring Ssl policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableSslOptions/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysListAvailableSslPredefinedPolicies(callback)</td>
    <td style="padding:15px">Lists all SSL predefined policies for configuring Ssl policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableSslOptions/default/predefinedPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGatewaysGetSslPredefinedPolicy(predefinedPolicyName, callback)</td>
    <td style="padding:15px">Gets Ssl predefined policy with the specified policy name.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationGatewayAvailableSslOptions/default/predefinedPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsLocationsList(callback)</td>
    <td style="padding:15px">Retrieves all ExpressRoutePort peering locations. Does not return available bandwidths for each location. Available bandwidths can only be obtained when retrieving a specific peering location.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ExpressRoutePortsLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsLocationsGet(locationName, callback)</td>
    <td style="padding:15px">Retrieves a single ExpressRoutePort peering location, including the list of available bandwidths available at said peering location.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ExpressRoutePortsLocations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsDelete(resourceGroupName, expressRoutePortName, callback)</td>
    <td style="padding:15px">Deletes the specified ExpressRoutePort resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsGet(resourceGroupName, expressRoutePortName, callback)</td>
    <td style="padding:15px">Retrieves the requested ExpressRoutePort resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsCreateOrUpdate(resourceGroupName, expressRoutePortName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified ExpressRoutePort resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsUpdateTags(resourceGroupName, expressRoutePortName, parameters, callback)</td>
    <td style="padding:15px">Update ExpressRoutePort tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">List all the ExpressRoutePort resources in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRoutePortsList(callback)</td>
    <td style="padding:15px">List all the ExpressRoutePort resources in the specified subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ExpressRoutePorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteLinksGet(resourceGroupName, expressRoutePortName, linkName, callback)</td>
    <td style="padding:15px">Retrieves the specified ExpressRouteLink resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts/{pathv3}/links/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expressRouteLinksList(resourceGroupName, expressRoutePortName, callback)</td>
    <td style="padding:15px">Retrieve the ExpressRouteLink sub-resources of the specified ExpressRoutePort resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/ExpressRoutePorts/{pathv3}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateEndpointsDelete(resourceGroupName, privateEndpointName, callback)</td>
    <td style="padding:15px">Deletes the specified private endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateEndpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateEndpointsGet(resourceGroupName, privateEndpointName, expand, callback)</td>
    <td style="padding:15px">Gets the specified private endpoint by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateEndpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateEndpointsCreateOrUpdate(resourceGroupName, privateEndpointName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates an private endpoint in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateEndpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateEndpointsList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all private endpoints in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateEndpointsListBySubscription(callback)</td>
    <td style="padding:15px">Gets all private endpoints in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/privateEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationSecurityGroupsDelete(resourceGroupName, applicationSecurityGroupName, callback)</td>
    <td style="padding:15px">Deletes the specified application security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationSecurityGroupsGet(resourceGroupName, applicationSecurityGroupName, callback)</td>
    <td style="padding:15px">Gets information about the specified application security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationSecurityGroupsCreateOrUpdate(resourceGroupName, applicationSecurityGroupName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates an application security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationSecurityGroupsUpdateTags(resourceGroupName, applicationSecurityGroupName, parameters, callback)</td>
    <td style="padding:15px">Updates an application security group's tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationSecurityGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationSecurityGroupsListAll(callback)</td>
    <td style="padding:15px">Gets all application security groups in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/applicationSecurityGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationSecurityGroupsList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all the application security groups in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/applicationSecurityGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPoliciesDelete(resourceGroupName, serviceEndpointPolicyName, callback)</td>
    <td style="padding:15px">Deletes the specified service endpoint policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPoliciesGet(resourceGroupName, serviceEndpointPolicyName, expand, callback)</td>
    <td style="padding:15px">Gets the specified service Endpoint Policies in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPoliciesCreateOrUpdate(resourceGroupName, serviceEndpointPolicyName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a service Endpoint Policies.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPoliciesUpdateTags(resourceGroupName, serviceEndpointPolicyName, parameters, callback)</td>
    <td style="padding:15px">Updates tags of a service endpoint policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPoliciesList(callback)</td>
    <td style="padding:15px">Gets all the service endpoint policies in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/ServiceEndpointPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPoliciesListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all service endpoint Policies in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPolicyDefinitionsDelete(resourceGroupName, serviceEndpointPolicyName, serviceEndpointPolicyDefinitionName, callback)</td>
    <td style="padding:15px">Deletes the specified ServiceEndpoint policy definitions.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}/serviceEndpointPolicyDefinitions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPolicyDefinitionsGet(resourceGroupName, serviceEndpointPolicyName, serviceEndpointPolicyDefinitionName, callback)</td>
    <td style="padding:15px">Get the specified service endpoint policy definitions from service endpoint policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}/serviceEndpointPolicyDefinitions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPolicyDefinitionsCreateOrUpdate(resourceGroupName, serviceEndpointPolicyName, serviceEndpointPolicyDefinitionName, serviceEndpointPolicyDefinitions, callback)</td>
    <td style="padding:15px">Creates or updates a service endpoint policy definition in the specified service endpoint policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}/serviceEndpointPolicyDefinitions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceEndpointPolicyDefinitionsListByResourceGroup(resourceGroupName, serviceEndpointPolicyName, callback)</td>
    <td style="padding:15px">Gets all service endpoint policy definitions in a service end point policy.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/serviceEndpointPolicies/{pathv3}/serviceEndpointPolicyDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRoutersDelete(resourceGroupName, virtualRouterName, callback)</td>
    <td style="padding:15px">Deletes the specified Virtual Router.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRoutersGet(resourceGroupName, virtualRouterName, expand, callback)</td>
    <td style="padding:15px">Gets the specified Virtual Router.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRoutersCreateOrUpdate(resourceGroupName, virtualRouterName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified Virtual Router.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRoutersListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all Virtual Routers in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRoutersList(callback)</td>
    <td style="padding:15px">Gets all the Virtual Routers in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/virtualRouters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRouterPeeringsDelete(resourceGroupName, virtualRouterName, peeringName, callback)</td>
    <td style="padding:15px">Deletes the specified peering from a Virtual Router.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRouterPeeringsGet(resourceGroupName, virtualRouterName, peeringName, callback)</td>
    <td style="padding:15px">Gets the specified Virtual Router Peering.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRouterPeeringsCreateOrUpdate(resourceGroupName, virtualRouterName, peeringName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified Virtual Router Peering.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}/peerings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualRouterPeeringsList(resourceGroupName, virtualRouterName, callback)</td>
    <td style="padding:15px">Lists all Virtual Router Peerings in a Virtual Router resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualRouters/{pathv3}/peerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkTapsDelete(resourceGroupName, tapName, callback)</td>
    <td style="padding:15px">Deletes the specified virtual network tap.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkTaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkTapsGet(resourceGroupName, tapName, callback)</td>
    <td style="padding:15px">Gets information about the specified virtual network tap.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkTaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkTapsCreateOrUpdate(resourceGroupName, tapName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a Virtual Network Tap.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkTaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkTapsUpdateTags(resourceGroupName, tapName, tapParameters, callback)</td>
    <td style="padding:15px">Updates an VirtualNetworkTap tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkTaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkTapsListAll(callback)</td>
    <td style="padding:15px">Gets all the VirtualNetworkTaps in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/virtualNetworkTaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualNetworkTapsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all the VirtualNetworkTaps in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/virtualNetworkTaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallsDelete(resourceGroupName, azureFirewallName, callback)</td>
    <td style="padding:15px">Deletes the specified Azure Firewall.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/azureFirewalls/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallsGet(resourceGroupName, azureFirewallName, callback)</td>
    <td style="padding:15px">Gets the specified Azure Firewall.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/azureFirewalls/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallsCreateOrUpdate(resourceGroupName, azureFirewallName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified Azure Firewall.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/azureFirewalls/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallsUpdateTags(resourceGroupName, azureFirewallName, parameters, callback)</td>
    <td style="padding:15px">Updates tags of an Azure Firewall resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/azureFirewalls/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallsList(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all Azure Firewalls in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/azureFirewalls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">azureFirewallsListAll(callback)</td>
    <td style="padding:15px">Gets all the Azure Firewalls in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/azureFirewalls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersCreateOrUpdate(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a network watcher in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGet(resourceGroupName, networkWatcherName, callback)</td>
    <td style="padding:15px">Gets the specified network watcher by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersDelete(resourceGroupName, networkWatcherName, callback)</td>
    <td style="padding:15px">Deletes the specified network watcher resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersUpdateTags(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Updates a network watcher tags.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all network watchers by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersListAll(callback)</td>
    <td style="padding:15px">Gets all network watchers by subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/networkWatchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetTopology(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Gets the current network topology by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersVerifyIPFlow(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Verify IP flow from the specified VM to a location given the currently configured NSG rules.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/ipFlowVerify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetNextHop(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Gets the next hop from the specified VM.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/nextHop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetVMSecurityRules(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Gets the configured and effective security group rules on the specified VM.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/securityGroupView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetTroubleshooting(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Initiate troubleshooting on a specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/troubleshoot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetTroubleshootingResult(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Get the last completed troubleshooting result on a specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/queryTroubleshootResult?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersSetFlowLogConfiguration(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Configures flow log and traffic analytics (optional) on a specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/configureFlowLog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetFlowLogStatus(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Queries status of flow log and traffic analytics (optional) on a specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/queryFlowLogStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersCheckConnectivity(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Verifies the possibility of establishing a direct TCP connection from a virtual machine to a given endpoint including another VM or an arbitrary remote server.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectivityCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetAzureReachabilityReport(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">NOTE: This feature is currently in preview and still being tested for stability. Gets the relative latency score for internet service providers from a specified location to Azure regions.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/azureReachabilityReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersListAvailableProviders(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">NOTE: This feature is currently in preview and still being tested for stability. Lists all available internet service providers for a specified Azure region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/availableProvidersList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkWatchersGetNetworkConfigurationDiagnostic(resourceGroupName, networkWatcherName, parameters, callback)</td>
    <td style="padding:15px">Gets Network Configuration Diagnostic data to help customers understand and debug network behavior. It provides detailed information on what security rules were applied to a specified traffic flow and the result of evaluating these rules. Customers must provide details of a flow like source, destination, protocol, etc. The API returns whether traffic was allowed or denied, the rules evaluated for the specified flow and the evaluation results.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/networkConfigurationDiagnostic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packetCapturesCreate(resourceGroupName, networkWatcherName, packetCaptureName, parameters, callback)</td>
    <td style="padding:15px">Create and start a packet capture on the specified VM.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/packetCaptures/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packetCapturesGet(resourceGroupName, networkWatcherName, packetCaptureName, callback)</td>
    <td style="padding:15px">Gets a packet capture session by name.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/packetCaptures/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packetCapturesDelete(resourceGroupName, networkWatcherName, packetCaptureName, callback)</td>
    <td style="padding:15px">Deletes the specified packet capture session.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/packetCaptures/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packetCapturesStop(resourceGroupName, networkWatcherName, packetCaptureName, callback)</td>
    <td style="padding:15px">Stops a specified packet capture session.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/packetCaptures/{pathv4}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packetCapturesGetStatus(resourceGroupName, networkWatcherName, packetCaptureName, callback)</td>
    <td style="padding:15px">Query the status of a running packet capture session.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/packetCaptures/{pathv4}/queryStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packetCapturesList(resourceGroupName, networkWatcherName, callback)</td>
    <td style="padding:15px">Lists all packet capture sessions within the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/packetCaptures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsCreateOrUpdate(resourceGroupName, networkWatcherName, connectionMonitorName, parameters, callback)</td>
    <td style="padding:15px">Create or update a connection monitor.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsGet(resourceGroupName, networkWatcherName, connectionMonitorName, callback)</td>
    <td style="padding:15px">Gets a connection monitor by name.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsDelete(resourceGroupName, networkWatcherName, connectionMonitorName, callback)</td>
    <td style="padding:15px">Deletes the specified connection monitor.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsUpdateTags(resourceGroupName, networkWatcherName, connectionMonitorName, parameters, callback)</td>
    <td style="padding:15px">Update tags of the specified connection monitor.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsStop(resourceGroupName, networkWatcherName, connectionMonitorName, callback)</td>
    <td style="padding:15px">Stops the specified connection monitor.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsStart(resourceGroupName, networkWatcherName, connectionMonitorName, callback)</td>
    <td style="padding:15px">Starts the specified connection monitor.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsQuery(resourceGroupName, networkWatcherName, connectionMonitorName, callback)</td>
    <td style="padding:15px">Query a snapshot of the most recent connection states.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors/{pathv4}/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionMonitorsList(resourceGroupName, networkWatcherName, callback)</td>
    <td style="padding:15px">Lists all connection monitors for the specified Network Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/connectionMonitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowLogsCreateOrUpdate(resourceGroupName, networkWatcherName, flowLogName, parameters, callback)</td>
    <td style="padding:15px">Create or update a flow log for the specified network security group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/flowLogs/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowLogsGet(resourceGroupName, networkWatcherName, flowLogName, callback)</td>
    <td style="padding:15px">Gets a flow log resource by name.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/flowLogs/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowLogsDelete(resourceGroupName, networkWatcherName, flowLogName, callback)</td>
    <td style="padding:15px">Deletes the specified flow log resource.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/flowLogs/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowLogsList(resourceGroupName, networkWatcherName, callback)</td>
    <td style="padding:15px">Lists all flow log resources for the specified Network Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/networkWatchers/{pathv3}/flowLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesDelete(resourceGroupName, serviceName, callback)</td>
    <td style="padding:15px">Deletes the specified private link service.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesGet(resourceGroupName, serviceName, expand, callback)</td>
    <td style="padding:15px">Gets the specified private link service by resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesList(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all private link services in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesListBySubscription(callback)</td>
    <td style="padding:15px">Gets all private link service in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/privateLinkServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesGetPrivateEndpointConnection(resourceGroupName, serviceName, peConnectionName, expand, callback)</td>
    <td style="padding:15px">Get the specific private end point connection by specific private link service in the resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}/privateEndpointConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesUpdatePrivateEndpointConnection(resourceGroupName, serviceName, peConnectionName, parameters, callback)</td>
    <td style="padding:15px">Approve or reject private end point connection for a private link service in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}/privateEndpointConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesDeletePrivateEndpointConnection(resourceGroupName, serviceName, peConnectionName, callback)</td>
    <td style="padding:15px">Delete private end point connection for a private link service in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}/privateEndpointConnections/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesListPrivateEndpointConnections(resourceGroupName, serviceName, callback)</td>
    <td style="padding:15px">Gets all private end point connections for a specific private link service.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}/privateEndpointConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesCheckPrivateLinkServiceVisibility(location, parameters, callback)</td>
    <td style="padding:15px">Checks whether the subscription is visible to private link service.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/checkPrivateLinkServiceVisibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesCheckPrivateLinkServiceVisibilityByResourceGroup(location, resourceGroupName, parameters, callback)</td>
    <td style="padding:15px">Checks whether the subscription is visible to private link service in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/locations/{pathv3}/checkPrivateLinkServiceVisibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesListAutoApprovedPrivateLinkServices(location, callback)</td>
    <td style="padding:15px">Returns all of the private link service ids that can be linked to a Private Endpoint with auto approved in this subscription in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/locations/{pathv2}/autoApprovedPrivateLinkServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesListAutoApprovedPrivateLinkServicesByResourceGroup(location, resourceGroupName, callback)</td>
    <td style="padding:15px">Returns all of the private link service ids that can be linked to a Private Endpoint with auto approved in this subscription in this region.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/locations/{pathv3}/autoApprovedPrivateLinkServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">privateLinkServicesCreateOrUpdate(resourceGroupName, serviceName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates an private link service in the specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/privateLinkServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bastionHostsDelete(resourceGroupName, bastionHostName, callback)</td>
    <td style="padding:15px">Deletes the specified Bastion Host.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/bastionHosts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bastionHostsGet(resourceGroupName, bastionHostName, callback)</td>
    <td style="padding:15px">Gets the specified Bastion Host.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/bastionHosts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bastionHostsCreateOrUpdate(resourceGroupName, bastionHostName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates the specified Bastion Host.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/bastionHosts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bastionHostsList(callback)</td>
    <td style="padding:15px">Lists all Bastion Hosts in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/bastionHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bastionHostsListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists all Bastion Hosts in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/bastionHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFiltersDelete(resourceGroupName, routeFilterName, callback)</td>
    <td style="padding:15px">Deletes the specified route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFiltersGet(resourceGroupName, routeFilterName, expand, callback)</td>
    <td style="padding:15px">Gets the specified route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFiltersCreateOrUpdate(resourceGroupName, routeFilterName, routeFilterParameters, callback)</td>
    <td style="padding:15px">Creates or updates a route filter in a specified resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFiltersUpdateTags(resourceGroupName, routeFilterName, parameters, callback)</td>
    <td style="padding:15px">Updates tags of a route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFiltersListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Gets all route filters in a resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFiltersList(callback)</td>
    <td style="padding:15px">Gets all route filters in a subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Network/routeFilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFilterRulesDelete(resourceGroupName, routeFilterName, ruleName, callback)</td>
    <td style="padding:15px">Deletes the specified rule from a route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}/routeFilterRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFilterRulesGet(resourceGroupName, routeFilterName, ruleName, callback)</td>
    <td style="padding:15px">Gets the specified rule from a route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}/routeFilterRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFilterRulesCreateOrUpdate(resourceGroupName, routeFilterName, ruleName, routeFilterRuleParameters, callback)</td>
    <td style="padding:15px">Creates or updates a route in the specified route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}/routeFilterRules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routeFilterRulesListByRouteFilter(resourceGroupName, routeFilterName, callback)</td>
    <td style="padding:15px">Gets all RouteFilterRules in a route filter.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Network/routeFilters/{pathv3}/routeFilterRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesPowerOff(resourceGroupName, vmName, skipShutdown, callback)</td>
    <td style="padding:15px">Operation to power off (stop) a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}/powerOff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesRestart(resourceGroupName, vmName, callback)</td>
    <td style="padding:15px">Operation to restart a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesStart(resourceGroupName, vmName, callback)</td>
    <td style="padding:15px">The operation to start a virtual machine</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesRunCommand(resourceGroupName, vmName, requestBody, callback)</td>
    <td style="padding:15px">Run command on the VM.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}/runCommand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesCreateOrUpdate(resourceGroupName, vmName, parameters, callback)</td>
    <td style="padding:15px">The operation to create or update a virtual machine</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesGet(resourceGroupName, vmName, expand, callback)</td>
    <td style="padding:15px">Retrieves information about the model view or the instance view of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesList(resourceGroupName, expand, filter, callback)</td>
    <td style="padding:15px">Lists all of the virtual machines in the specified resource group</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesListAll(expand, filter, statusOnly, callback)</td>
    <td style="padding:15px">Lists all of the virtual machines in the specified subscription</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.Compute/virtualMachines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualMachinesDelete(resourceGroupName, vmName, forceDeletion, callback)</td>
    <td style="padding:15px">The operation to delete a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.Compute/virtualMachines/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
