# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Azure System. The API that was used to build the adapter for Azure is usually available in the report directory of this adapter. The adapter utilizes the Azure API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Azure adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Azure. With this adapter you have the ability to perform operations such as:

- Create, Modify, and Delete VNF/VNET/VMs.

- Reserve resources and configure permissions.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
