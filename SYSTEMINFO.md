# Adapter for Microsoft Azure

Vendor: Microsoft
Homepage: https://www.microsoft.com/en-us/

Product: Azure
Product Page: https://azure.microsoft.com/en-us/

## Introduction
We classify Microsoft Azure into the Cloud domain as Microsoft Azure provides a wide range of cloud services and the capability to interact with and manage Azure services and resources. 

## Why Integrate
The Azure adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Azure. With this adapter you have the ability to perform operations such as:

- Create, Modify, and Delete VNF/VNET/VMs.

- Reserve resources and configure permissions.

## Additional Product Documentation
The [API documents for Microsoft Azure](https://learn.microsoft.com/en-us/rest/api/azure/)
